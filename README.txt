=== CannaPet Customization ===
Contributors: angelleye
Donate link: http://www.angelleye.com/
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 5.2.2
Stable tag: 1.2.10
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Customizes checkout flow for guest checkouts related to AutoShip products and the PayPal for WooCommerce or Network Merchants Inc payment plugin gateways.

== Description ==

Customizes checkout flow for guest checkouts related to AutoShip products and the PayPal for WooCommerce plugin gateways.

= 1.2.10 =
* [CAN-37] - Allows automated tokenization for all orders instead of only Autoship orders.

= 1.2.9 =
* Add Authorize.net THREE gateway
* [CP-38] - Resolves expiration date formatting problems.
* [ca81255] - Added card type function + added add payment method code + changed the wrong displayed label

= 1.2.8 =
* [CP-34] - Adjustments to Authorize.net error responses.

= 1.2.7 =
* [CP-29] - Corrects format of expiration date when adding cards through My Account page.

= 1.2.6 =
* [CP-25] - Adds logs to PetConscious payment gateway to help with troubleshooting.

= 1.2.5 =
* [CP-7] - Remove CC entry fields when saved payment method is selected
* [CP-10] - Token Transactions
* [CP-11] - Remove public facing notices that mention Mojopay
* [CP-12] - Formatting of data entry field(s)

= 1.2.4 =
* Tweak - Order status for failed MojoPay orders set to Pending instead of On Hold. [#54]
* Tweak - Specific error handler to ignore errors for response code 802. [#52][#56]
* Fix - Resolves an issue with a masked card number getting sent in the MojoPay request. [#55]

= 1.2.3 =
* Tweak - Adjusts result handler to work on response codes instead of status codes. [#43]
* Tweak - Disable autocomplete on MojoPay fields. [#44]
* Tweak - Adjusts log files to mask sensitive data. [#49]
* Fix - Resolves "expected as string" errors. [#45][#48]
* Fix - Resolves issue with expired card by ensuring we always pass expiration date, even with reference transactions. [#50][#51]

= 1.2.2 =
* Feature - Adds MojoPay customization for auto account creation and Autoship products/orders. [#34][#40]
* Feature - Adds support for Woo token payments into MojoPay integration. [#39]
* Feature - Adds support for refunding MojoPay orders through WooCommerce. [#38]
* Tweak - Adjustments to PetConscious checkout system to eliminate iFrame. [#36]
* Fix - Adds form validation for maximum value lengths based on MojoPay limits. [#41]
* Fix - Truncates and cleans data values of special characters to avoid MojoPay failures. [#42]

= 1.2.1 =
* Feature - Adds refund capability for 3rd party iFrame checkout. [#30]
* Feature - Adds auto-account creation to 3rd party iFrame checkout. [#31]
* Feature - Adds AutoShip compatibility to 3rd party iFrame checkout. [#32]
* Tweak - Adjusts PayFlow description display in iFrame checkout. [#29]
* Tweak - Adjusts jQuery. [#33]

= 1.2.0 =
* Feature - Adds 3rd party iFrame hosted PayPal Express Checkout and PayPal Pro.

= 1.1.5 =
* Fix - Resolves a vulnerability with account login of an existing account during checkout. [#21]
* Fix - Resolves an issue with Month/Year formatting. [#22]
* Fix - Resolves a PHP fatal error when trying to run Autoship manually. [#24]
* Fix - Resolves E00003 error caused because CVV/CVC fields are required, and when empty this error was generated. [#26]
* Fix - Resolves conflicts with guest checkouts where email is already registered. [#27]

= 1.1.4 =
* Tweak - Further improvements to Authorize.net error logging and functionality. [#18]
* Tweak - Pull error display functions from Auth.net CIM plugin. [#19]
* Tweak - Merge client added adjustments to CSS styling. [#20]

= 1.1.3 =
* Tweak - Improves Authorize.net functionality. [#17]

= 1.1.2 =
* Feature - Adds Authorize.net compatibility with auto-account creation. [#16]

= 1.1.1 =
* Feature - Modifies AutoShip export functionality to include the gateway ID. [#15]

= 1.1.0 =
* Feature - Adds auto-account creation compatibility with Network Merchants Inc payment gateway. [#12]
* Tweak - Updates logic so that auto-account creation does not occur until after order payment is completed successfully. [#13]

= 1.0.0 =
* Enables guest checkout experience for AutoShip products.
* Automatically creates account for guest checkouts.
* Customizes New Account email sent to guest checkout users to give them a quick method of completing their account setup with the site.