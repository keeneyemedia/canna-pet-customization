(function ($) {
    $(function () {
        $(document.body).on('updated_checkout', function () {
            setTimeout(function () {
                $('.wc-credit-card-form-card-number').val('');
                $('.wc-credit-card-form-card-expiry').val('');
                $('.wc-credit-card-form-card-cvc').val('');
            }, 200);
        });
        setTimeout(function () {
            if($('.wc-credit-card-form-card-number').length > 0) {
                $('.wc-credit-card-form-card-number').val('');
                $('.wc-credit-card-form-card-expiry').val('');
                $('.wc-credit-card-form-card-cvc').val('');
            }
        }, 200);
    });
}(jQuery));