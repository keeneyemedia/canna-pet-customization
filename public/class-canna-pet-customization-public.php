<?php

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Canna_Pet_Customization
 * @subpackage Canna_Pet_Customization/public
 * @author     Angell EYE <service@angelleye.com>
 */
class Canna_Pet_Customization_Public {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;
    public $customer_id;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
        $this->customer_id;
        $this->enable_automated_account_creation_for_guest_checkouts = $this->angelleye_wc_autoship_cart_has_autoship_item();
    }

    public function angelleye_wc_autoship_cart_has_autoship_item() {
        if (!function_exists('WC')) {
            return false;
        }
        $cart = WC()->cart;
        if (empty($cart)) {
            return false;
        }

        /*
         * https://bitbucket.org/angelleye/cannapet-customization/issues/8/auto-account-and-token-on-all-orders
         * Auto-Account and Token on ALL orders.
         */

        /* $has_autoship_items = false;
          foreach ($cart->get_cart() as $item) {
          if (isset($item['wc_autoship_frequency'])) {
          $has_autoship_items = true;
          break;
          }
          } */
        return $has_autoship_items = true;
    }

    public function enable_automated_account_creation_for_guest_checkouts($order_id) {
        if (!$this->angelleye_wc_autoship_cart_has_autoship_item()) {
            return;
        }
        try {
            if (is_user_logged_in()) {
                return false;
            }
            if (wc_notice_count('error') > 0) {
                return false;
            }
            $this->customer_id = $this->angelleye_automated_account_creation_for_guest_checkouts($order_id);
        } catch (Exception $e) {
            if (!empty($e)) {
                wc_add_notice($e->getMessage(), 'error');
            }
        }
    }

    public function angelleye_automated_account_creation_for_guest_checkouts($order_id) {
        if ($this->angelleye_get_name_from_order($order_id)) {
            $email = $this->angelleye_get_email_from_order($order_id);
            if (email_exists($email)) {
                $customer_id = email_exists($email);
            } else {
                $username = sanitize_user(current(explode('@', $email)), true);
                $append = 1;
                $o_username = $username;
                while (username_exists($username)) {
                    $username = $o_username . $append;
                    $append++;
                }
                $password = wp_generate_password();
                WC()->session->set('before_wc_create_new_customer', true);
                $new_customer = wc_create_new_customer($email, $username, $password);
                if (is_wp_error($new_customer)) {
                    throw new Exception($new_customer->get_error_message());
                } else {
                    $customer_id = absint($new_customer);
                    do_action('woocommerce_guest_customer_new_account_notification', $customer_id);
                }
            }
            WC()->session->set('automated_account_creation_for_guest_checkouts', true);
            wp_set_current_user( $customer_id );
            WC()->session->set('reload_checkout', true);
            WC()->cart->calculate_totals();
            $name = $this->angelleye_get_name_from_order($order_id);
            if ($name['first_name'] && apply_filters('woocommerce_checkout_update_customer_data', true, WC()->customer)) {
                $userdata = array(
                    'ID' => $customer_id,
                    'first_name' => $name['first_name'],
                    'last_name' => $name['last_name'],
                    'display_name' => $name['first_name']
                );
                update_post_meta($order_id, '_customer_user', $customer_id);
                wp_update_user(apply_filters('woocommerce_checkout_customer_userdata', $userdata, WC()->customer));
                $order = wc_get_order($order_id);
                $is_address_updated = get_user_meta($customer_id, 'is_address_updated', 'yes');
                if ($is_address_updated != 'yes') {
                    $billing_address = $order->get_address($type = 'billing');
                    foreach ($billing_address as $key => $value) {
                        update_user_meta($customer_id, 'billing_' . $key, $value);
                    }
                    $shipping_address = $order->get_address($type = 'shipping');
                    foreach ($shipping_address as $key => $value) {
                        update_user_meta($customer_id, 'shipping_' . $key, $value);
                    }
                    update_user_meta($customer_id, 'is_address_updated', 'yes');
                }
                wc_clear_notices();
                return $customer_id;
            }
        }
    }

    public function angelleye_wc_autoship_get_option_enable_guest_checkout($enable_guest_checkout) {
        if ($this->angelleye_wc_autoship_cart_has_autoship_item()) {
            global $wpdb;
            $row = $wpdb->get_row($wpdb->prepare("SELECT option_value FROM $wpdb->options WHERE option_name = %s LIMIT 1", 'woocommerce_enable_guest_checkout'));
            if (empty($row->option_value) || $row->option_value == 'yes') {
                return $woocommerce_enable_guest_checkout = 'yes';
            }
        }
        return $enable_guest_checkout;
    }

    public function payment_fields_saved_payment_methods($gateway) {
        global $wp;        
        if ($gateway->supports('tokenization') && $this->cart_has_autoship_item()) {
            ?>
            <script type="text/javascript">
                jQuery(document.body).on('updated_checkout wc-credit-card-form-init', function () {
                    jQuery("#wc-<?php echo $gateway->id; ?>-new-payment-method").prop("checked", true);
                    jQuery("#wc-<?php echo $gateway->id; ?>-new-payment-method").parents('p').hide();
                });
            </script>		
            <style type="text/css">
                .form-row.woocommerce-SavedPaymentMethods-saveNew {
                    display: none !important;
                }
            </style>
            <?php
        }                                    
        if($gateway->supports('tokenization') && isset($wp->query_vars['order-pay'])){
            ?>
             
            <script type="text/javascript">
                jQuery(document).on('click','.payment_method_<?php echo $gateway->id; ?> .woocommerce-SavedPaymentMethods-token', function () {                    
                    jQuery('#wc-<?php echo $gateway->id; ?>-cc-form').hide();
                });
                jQuery(document).on('click','.payment_method_<?php echo $gateway->id; ?> .woocommerce-SavedPaymentMethods-new', function () {
                    jQuery('#wc-<?php echo $gateway->id; ?>-cc-form').show();
                });
                jQuery(document).ready(function(){
                    if(jQuery('#payment_method_<?php echo $gateway->id; ?>').is(':checked')) {
                        jQuery("#wc-<?php echo $gateway->id; ?>-payment-token-new").prop("checked", true);
                    }
                    
                    jQuery(document).on('click','#payment_method_<?php echo $gateway->id; ?>',function(){
                        jQuery("#wc-<?php echo $gateway->id; ?>-payment-token-new").prop("checked", true);
                        jQuery('#wc-<?php echo $gateway->id; ?>-cc-form').show();
                    });
                });
            </script>
        <?php        
        }
        
    }

    public function angelleye_automated_account_logout_for_guest_checkout() {
        if (isset(WC()->session->automated_account_creation_for_guest_checkouts)) {
            unset(WC()->session->automated_account_creation_for_guest_checkouts);
            wp_logout();
        }
    }

    public function angelleye_change_page_title($title) {
        if (!empty($_GET['show-reset-form']) && $_GET['show-reset-form'] == true && $title == 'Lost Password') {
            return apply_filters('canna_pet_customization_account_page_name', __('Set Password and Create Account.', 'canna-pet-customization'));
        } else {
            return $title;
        }
    }

    public function remove_woocommerce_created_customer($email_actions) {
        if (isset(WC()->session->before_wc_create_new_customer)) {
            unset(WC()->session->before_wc_create_new_customer);
            unset($email_actions['woocommerce_created_customer']);
        }
        return $email_actions;
    }

    public function canna_pet_customization_woocommerce_locate_template($template, $template_name, $template_path) {
        global $woocommerce;
        $_template = $template;
        if (!$template_path) {
            $template_path = $woocommerce->template_url;
        }
        $plugin_path = untrailingslashit(plugin_dir_path(__FILE__)) . '/templates/';
        $template = locate_template(
                array(
                    $template_path . $template_name,
                    $template_name
                )
        );
        if (!$template && file_exists($plugin_path . $template_name)) {
            $template = $plugin_path . $template_name;
        }
        if (!$template) {
            $template = $_template;
        }
        return $template;
    }

    public function canna_pet_customization_woocommerce_hide_save_to_account() {
        wp_enqueue_style('canna-pet-customization-css', CANNA_PET_CUSTOMIZATION_ASSET_URL . 'public/assets/css/canna-pet-customization.css', array(), $this->version, 'all');        
    }

    public function angelleye_automated_account_creation_for_guest($order_id) {
        if ($this->angelleye_get_email_from_order($order_id)) {
            $this->enable_automated_account_creation_for_guest_checkouts($order_id);
        }
    }

    public function angelleye_get_email_from_order($order_id) {
        $shipping_email = get_post_meta($order_id, '_shipping_email', true);
        if (!empty($shipping_email)) {
            return $shipping_email;
        }
        $billing_email = get_post_meta($order_id, '_billing_email', true);
        if (!empty($billing_email)) {
            return $billing_email;
        }
        $customer_email = get_post_meta($order_id, 'customer_email', true);
        if (!empty($customer_email)) {
            return $customer_email;
        }
        $paypal_email = get_post_meta($order_id, 'paypal_email', true);
        if (!empty($paypal_email)) {
            return $paypal_email;
        }
        return false;
    }

    public function angelleye_get_name_from_order($order_id) {
        $name = array();
        $shipping_first_name = get_post_meta($order_id, '_shipping_first_name', true);
        $shipping_last_name = get_post_meta($order_id, '_shipping_last_name', true);
        if (!empty($shipping_first_name) && !empty($shipping_last_name)) {
            $name['first_name'] = $shipping_first_name;
            $name['last_name'] = $shipping_last_name;
            return $name;
        }
        $billing_first_name = get_post_meta($order_id, '_billing_first_name', true);
        $billing_last_name = get_post_meta($order_id, '_billing_last_name', true);
        if (!empty($billing_first_name) && !empty($billing_last_name)) {
            $name['first_name'] = $billing_first_name;
            $name['last_name'] = $billing_last_name;
            return $name;
        }
        return false;
    }

    public function own_woocommerce_checkout_order_processed($order_id, $posted_data, $order = array()) {
        
        if (isset(WC()->cart) && sizeof(WC()->cart->get_cart()) > 0) {
            if (WC()->cart->needs_payment()) {                 
                if ($this->cart_has_autoship_item() == true && is_user_logged_in() == false) {
                    $payment_method = get_post_meta($order_id, '_payment_method', true);
                    if (!empty($payment_method)) {
                        $key = 'wc-' . $payment_method . '-new-payment-method';
                        $_POST[$key] = true;
                    }
                } elseif ($this->cart_has_autoship_item() == true && is_user_logged_in() == true) {
                    $payment_method = get_post_meta($order_id, '_payment_method', true);
                    $key_redio = 'wc-' . $payment_method . '-payment-token';
                    if (!empty($_POST[$key_redio]) && $_POST[$key_redio] == 'new') {
                        $key = 'wc-' . $payment_method . '-new-payment-method';
                        $_POST[$key] = true;
                    } else {
                        $key = 'wc-' . $payment_method . '-new-payment-method';
                        unset($_POST[$key]);
                    }
                }                
                $payment_methods_array = array('wc_autoship_authorize_net','wc_autoship_authorize_net_two','wc_autoship_authorize_net_three','petconscious_checkout','petconscious_paypal_payflow', 'cannapets_mojopay');               
                if( !empty($payment_method) && in_array($payment_method, $payment_methods_array)) {
                    $this->enable_automated_account_creation_for_guest_checkouts($order_id);
                }
            }
        }
    }

    public function cart_has_autoship_item() {
        if (!function_exists('WC')) {
            return false;
        }
        $cart = WC()->cart;
        if (empty($cart)) {
            return false;
        }
//        $has_autoship_items = false;
//        foreach ($cart->get_cart() as $item) {
//            if (isset($item['wc_autoship_frequency'])) {
//                $has_autoship_items = true;
//                break;
//            }
//        }
        return $has_autoship_items = true;
    }
    
    public function wc_get_account_saved_payment_methods_list_item_cc($item, $payment_token ) {
        if ( 'cc' !== strtolower( $payment_token->get_type() ) ) {
		return $item;
	}

	$card_type               = $payment_token->get_card_type();
	$item['method']['last4'] = $payment_token->get_last4();
	$item['method']['brand'] = ( ! empty( $card_type ) ? ucfirst( $card_type ) : esc_html__( 'Credit card', 'woocommerce' ) );
	$item['expires']         = $payment_token->get_expiry_month() . '/' . $payment_token->get_expiry_year();

	return $item;
    }
    
    public function action_woocommerce_receipt_petconscious_checkout($order_id) {
        $order = new WC_Order( $order_id );
        $url = site_url()."/checkout/order-pay/".$order_id."/?pay_for_order=true&key=".$order->order_key;
        wp_redirect($url);
        exit;
    }   

    public function add_notice_for_dec_on_checkout_page(){        
        if(WC()->session->__isset('DEC_ERRORS')){
            $error = WC()->session->get( 'DEC_ERRORS' );
            wc_print_notice( $error, 'error' );               
            WC()->session->set( 'DEC_ERRORS', null );
        }
    }
    
    public function angelleye_payment_scripts() {
        wp_enqueue_script( 'angelleye-cannapet-customization', CANNA_PET_CUSTOMIZATION_ASSET_URL . 'public/assets/js/cannapet-customization.js', array( 'wc-credit-card-form' ), WC_VERSION, true );
    }
}
