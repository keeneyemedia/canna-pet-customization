<?php

global $woocommerce;

if(isset($_GET['order_number'])){
    if($_GET['pg'] == 'ecp'){
        //get order from
        $order = wc_get_order( $_GET['order_number'] );

        $txn_id = $_GET['txn_id'];
        update_post_meta($order->get_order_number(), '_transaction_id', $txn_id);

        // Set order status
        $status = 'wc-processing';
        $order->update_status( $status, __( 'Checkout with PayPal Express Checkout Payment. ', 'canna-pet-customization' ) );

        $order->add_order_note(sprintf(__('TransactionID for the  (Order: %1$s) and is %2$s', 'canna-pet-customization'), $order->get_order_number(),$txn_id));
        // Reduce stock levels
        $order->reduce_order_stock();

        // Empty cart.
        $items = $woocommerce->cart->get_cart();
        if(!empty($items)){
            $woocommerce->cart->empty_cart();
        }

        $url = site_url().'/checkout/order-received/'.$order->get_order_number().'/?key='.$order->get_order_key();
        wp_redirect($url);
        exit;
    }
    
    if($_GET['pg'] == 'pppro'){
       
        $order = wc_get_order( $_GET['order_number'] );
         $status = 'wc-processing';
        $order->update_status( $status, __( 'Checkout with PayPal Payments (PayFlow).', 'canna-pet-customization' ) );
        
        if(isset($_GET['txn_id']) && !empty($_GET['txn_id'])){
            $order->add_order_note(sprintf(__('PayPal Payments (PayFlow) completed (PNREF: %s) (PPREF: %s)', 'paypal-for-woocommerce'), $_GET['pnref'], $_GET['txn_id']));
            $txn_id = $_GET['txn_id'];
            update_post_meta($order->get_order_number(), '_transaction_id', $txn_id);
        }
        else{
            $order->add_order_note(sprintf(__('PayPal Payments (PayFlow) completed (PNREF: %s)', 'paypal-for-woocommerce'), $_GET['pnref']));
        }
        $order->reduce_order_stock();
        $items = $woocommerce->cart->get_cart();
        if(!empty($items)){
            $woocommerce->cart->empty_cart();
        }
        $url = site_url().'/checkout/order-received/'.$order->get_order_number().'/?key='.$order->get_order_key();
        wp_redirect($url);
        exit;
    }
    
}