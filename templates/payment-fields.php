<div id="wc-autoship-authorize-net-fields" class="wc-autoship-payment-fields">
	<span id="wc-autoship-authorize-net-payment-errors" class="payment-errors"></span>

	<ul class="woocommerce-SavedPaymentMethods wc-saved-payment-methods">
		<?php foreach ( $payment_tokens as $token ): ?>
			<li class="woocommerce-SavedPaymentMethods-token">
				<input id="wc-wc_autoship_authorize_net-payment-token-<?php echo esc_attr( $token->get_id() ); ?>" type="radio" name="wc-wc_autoship_authorize_net-payment-token" value="<?php echo esc_attr( $token->get_id() ); ?>" style="width:auto;" class="woocommerce-SavedPaymentMethods-tokenInput" />
				<label for="wc-wc_autoship_authorize_net-payment-token-<?php echo esc_attr( $token->get_id() ); ?>"><?php echo esc_html( $this->get_display_name($token) ); ?></label>
			</li>
		<?php endforeach; ?>
		<li class="woocommerce-SavedPaymentMethods-token">
			<input id="wc-wc_autoship_authorize_net-payment-token-new" type="radio" name="wc-wc_autoship_authorize_net-payment-token" value="new" style="width:auto;" class="woocommerce-SavedPaymentMethods-tokenInput" checked="checked" />
			<label for="wc-wc_autoship_authorize_net-payment-token-new"><?php echo __( 'Add a new payment method', 'wc-autoship-authorize-net-payments' ); ?></label>
		</li>
	</ul>

	<div class="wc-autoship-payment-field wc-autoship-authorize-net-number-field">
		<label for="wc-autoship-authorize-net-number">
			<span>Card Number</span>
			<input type="text" id="wc-autoship-authorize-net-number" size="20" name="wc_autoship_authorize_net_number" placeholder="•••• •••• •••• ••••" class="input-text wc-credit-card-form-card-number" />
			<span id="wc-autoship-authorize-net-number-errors" class="payment-errors"></span>
		</label>
	</div>

	<div class="wc-autoship-payment-field wc-autoship-authorize-net-exp-field">
		<label for="wc-autoship-authorize-net-exp">
			<span>Expiration (MM/YYYY)</span>
			<input type="text" id="wc-autoship-authorize-net-exp" size="7" name="wc_autoship_authorize_net_exp" placeholder="MM/YYYY" class="input-text wc-credit-card-form-card-expiry" />
			<span id="wc-autoship-authorize-net-exp-errors" class="payment-errors"></span>
		</label>
	</div>
	
	<?php if ( 'yes' == $require_cvv ): ?>
		<div class="wc-autoship-payment-field wc-autoship-authorize-net-card-code-field">
			<label for="wc-autoship-authorize-net-card-code">
				<span>CVV/CVC</span>
				<input type="text" id="wc-autoship-authorize-net-card-code" size="4" name="wc_autoship_authorize_net_card_code" placeholder="CVC" class="input-text wc-credit-card-form-card-cvc" />
				<span id="wc-autoship-authorize-net-card-code-errors" class="payment-errors"></span>
			</label>
		</div>
	<?php endif; ?>

	<div style="display: none">
		<input type="hidden" name="wc-wc_autoship_authorize_net-new-payment-method" value="yes" />
	</div>

</div>