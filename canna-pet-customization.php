<?php

/**
 * @link              http://www.angelleye.com/
 * @since             1.0.0
 * @package           Canna_Pet_Customization
 *
 * @wordpress-plugin
 * Plugin Name:       Canna-Pet Customization
 * Plugin URI:        http://www.angelleye.com/
 * Description:       Customizes checkout flow for guest checkouts related to AutoShip products and the PayPal for WooCommerce plugin gateways.
 * Version:           1.2.10
 * Author:            Angell EYE
 * Author URI:        http://www.angelleye.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       canna-pet-customization
 * Domain Path:       /languages
 */
// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}
if (!defined('CANNA_PET_CUSTOMIZATION_ASSET_URL')) {
    define('CANNA_PET_CUSTOMIZATION_ASSET_URL', plugin_dir_url(__FILE__));
}
if (!defined('CANNA_PET_CUSTOMIZATION_PLUGIN_DIR')) {
    define('CANNA_PET_CUSTOMIZATION_PLUGIN_DIR', dirname(__FILE__));
}
if (!defined('CANNA_PET_CUSTOMIZATION_VERSION')) {
    define('CANNA_PET_CUSTOMIZATION_VERSION', '1.2.10');
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-canna-pet-customization-activator.php
 */
function activate_canna_pet_customization() {
    require_once plugin_dir_path(__FILE__) . 'includes/class-canna-pet-customization-activator.php';
    Canna_Pet_Customization_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-canna-pet-customization-deactivator.php
 */
function deactivate_canna_pet_customization() {
    require_once plugin_dir_path(__FILE__) . 'includes/class-canna-pet-customization-deactivator.php';
    Canna_Pet_Customization_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_canna_pet_customization');
register_deactivation_hook(__FILE__, 'deactivate_canna_pet_customization');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-canna-pet-customization.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_canna_pet_customization() {

    $plugin = new Canna_Pet_Customization();
    $plugin->run();
}

add_action('plugins_loaded', 'run_canna_pet_customization', 999);