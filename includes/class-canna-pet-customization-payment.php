<?php

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Canna_Pet_Customization
 * @subpackage Canna_Pet_Customization/includes
 * @author     Angell EYE <service@angelleye.com>
 */


class Canna_Pet_Customization_Payment extends woocommerce_nmipay {

    /**
     * @since    1.0.0
     */
    public function __construct() {
        parent::__construct();
    }

    function process_payment($order_id) {
        global $woocommerce;

        $order = new WC_Order($order_id);

        $credit_card = preg_replace('/(?<=\d)\s+(?=\d)/', '', trim($_POST['nmipay-card-number']));
        $ccexp_expiry = str_replace(' ', '', $_POST['nmipay-card-expiry']);
        $month = substr($ccexp_expiry, 0, 2);
        $year = substr($ccexp_expiry, 3, 4);
        $provider = $this->provider;
        $cardtype = $this->getCardType($credit_card);

        $nmi_adr = $this->$provider . '?';

        if ($this->debug == 'yes') {
            $this->log->add('nmipay', $order_id . ' - CC Processing Started');
        }

        /*
        // 09-18-19: Remove all references to Tokens
        
        $tokens = WC_Payment_Tokens::get_customer_tokens(get_current_user_id(), 'nmipay');
        if (isset($_POST['wc-nmipay-new-payment-method']) && count($tokens) == 0) {

            $nmipay_args['customer_vault'] = 'add_customer';
            $nmipay_args['ccnumber'] = $credit_card;
            $nmipay_args['cvv'] = $_POST["nmipay-card-cvc"];
            $nmipay_args['ccexp'] = $month . '/' . $year;

            $last_four_digits = substr($nmipay_args['ccnumber'], -4);

            if ($this->debug == 'yes') {
                $this->log->add('nmipay', $order_id . ' - Token Count: ' . count($tokens) . ' Token Param: ' . $_POST['wc-nmipay-new-payment-method']);
            }
        } else if (isset($_POST['wc-nmipay-new-payment-method']) && count($tokens) > 0) {

            $token = WC_Payment_Tokens::get(get_user_meta(get_current_user_id(), 'nmi_cc_token_id', true));

            $nmipay_args['customer_vault'] = 'update_customer';
            $nmipay_args['customer_vault_id'] = $token->get_token();
            $nmipay_args['ccnumber'] = $credit_card;
            $nmipay_args['cvv'] = $_POST["nmipay-card-cvc"];
            $nmipay_args['ccexp'] = $month . '/' . $year;

            $last_four_digits = substr($nmipay_args['ccnumber'], -4);

            if ($this->debug == 'yes') {
                $this->log->add('nmipay', $order_id . ' - Custom Saved Token : ' . $token->get_id() . ' Token Param: ' . $_POST['wc-nmipay-new-payment-method']);
            }
        } else if (isset($_POST['wc-nmipay-payment-token']) && 'new' !== $_POST['wc-nmipay-payment-token']) {

            $token_id = wc_clean($_POST['wc-nmipay-payment-token']);
            $token = WC_Payment_Tokens::get($token_id);
            $nmipay_args['customer_vault'] = 'update_customer';
            $nmipay_args['customer_vault_id'] = $token->get_token();

            if ($this->debug == 'yes') {
                $this->log->add('nmipay', $order_id . ' - Token : ' . $token_id . ' Token Param: ' . $_POST['wc-nmipay-new-payment-method']);
            }
        } else {
        */
            $nmipay_args['ccnumber'] = $credit_card;
            $nmipay_args['cvv'] = $_POST["nmipay-card-cvc"];
            $nmipay_args['ccexp'] = $month . '/' . $year;

            $last_four_digits = substr($credit_card, -4);

            if ($this->debug == 'yes') {
                $this->log->add('nmipay', $order_id . ' - New Card with no Tokenisation ');
            }
        //}

        if ($this->receipt) {
            $nmipay_args['customer_receipt'] = $this->receipt;
        }

        // Processing subscription
        if (function_exists('wcs_order_contains_subscription') || function_exists('wcs_is_subscription')) {

            if (wcs_order_contains_subscription($order_id) || wcs_is_subscription($order_id)) {

                $nmipay_args['type'] = $this->transtype;
                $nmipay_args['payment'] = 'creditcard';
                $nmipay_args['ipaddress'] = $_SERVER['REMOTE_ADDR'];
                $nmipay_args['username'] = $this->username;
                $nmipay_args['password'] = $this->password;
                $nmipay_args['currency'] = get_woocommerce_currency();

                $nmipay_args['orderid'] = $order_id . '-' . time();

                $nmipay_args = $this->set_billing_and_shipping_address($nmipay_args, $order);

                $nmipay_args['email'] = $order->get_billing_email();

                $nmipay_args['invoice'] = $order->order_key;

                $AmountInput = number_format($order->order_total, 2, '.', '');

                $nmipay_args['amount'] = $AmountInput;

                if (in_array($order->billing_country, array('US', 'CA'))) {
                    $order->billing_phone = str_replace(array('( ', '-', ' ', ' )', '.'), '', $order->billing_phone);
                    $nmipay_args['phone'] = $order->billing_phone;
                } else {
                    $nmipay_args['phone'] = $order->billing_phone;
                }
                //var_dump($order->get_total_tax());die;
                // Tax
                $nmipay_args['tax'] = $order->get_total_tax();

                // Cart Contents
                $item_loop = 0;
                if (sizeof($order->get_items()) > 0) {
                    foreach ($order->get_items() as $item) {
                        if ($item['qty']) {

                            $item_loop++;

                            $product = $order->get_product_from_item($item);

                            $item_name = $item['name'];

                            $item_meta = new WC_Order_Item_Meta($item['item_meta']);
                            if ($meta = $item_meta->display(true, true))
                                $item_name .= ' ( ' . $meta . ' )';

                            $nmipay_args['item_description_' . $item_loop] = $item_name;
                            $nmipay_args['item_quantity_' . $item_loop] = $item['qty'];
                            $nmipay_args['item_unit_cost_' . $item_loop] = $order->get_item_subtotal($item, false);

                            //if ( $product->get_sku() )
                            //	$nmipay_args[ 'product_sku_' . $item_loop ] = $product->get_sku();
                        }
                    }
                }

                // Discount
                if ($order->get_total_discount() > 0) {
                    $nmipay_args['discount_amount'] = number_format($order->get_total_discount(), 2, '.', '');
                }

                // Shipping Cost item - nmipay only allows shipping per item, we want to send shipping for the order
                if ($order->get_total_shipping() > 0) {
                    $nmipay_args['shipping'] = number_format($order->get_total_shipping(), 2, '.', '');
                }


                $subscriptions = wcs_get_subscriptions_for_order($order);

                $subscription = array_pop($subscriptions);

                if (!empty($subscription)) {

                    $order_items = $order->get_items();

                    $unconverted_periods = array(
                        'billing_period' => $subscription->billing_period,
                        'trial_period' => $subscription->trial_period,
                    );

                    $converted_periods = array();

                    // Convert period strings into PayPay's format
                    foreach ($unconverted_periods as $key => $period) {
                        switch (strtolower($period)) {
                            case 'day':
                                $converted_periods[$key] = 'day';
                                break;
                            case 'week':
                                $converted_periods[$key] = 'week';
                                break;
                            case 'year':
                                $converted_periods[$key] = 'year';
                                break;
                            case 'month':
                            default:
                                $converted_periods[$key] = 'month';
                                break;
                        }
                    }

                    $sign_up_fee = $subscription->get_sign_up_fee();
                    $price_per_period = $subscription->get_total();
                    $subscription_interval = $subscription->billing_interval;
                    $start_timestamp = $subscription->get_time('start');
                    $trial_end_timestamp = $subscription->get_time('trial_end');
                    $next_payment_timestamp = $subscription->get_time('next_payment');

                    $is_synced_subscription = WC_Subscriptions_Synchroniser::subscription_contains_synced_product($subscription->id);

                    if ($is_synced_subscription) {
                        $length_from_timestamp = $next_payment_timestamp;
                    } elseif ($trial_end_timestamp > 0) {
                        $length_from_timestamp = $trial_end_timestamp;
                    } else {
                        $length_from_timestamp = $start_timestamp;
                    }

                    $subscription_length = wcs_estimate_periods_between($length_from_timestamp, $subscription->get_time('end'), $subscription->billing_period);

                    $subscription_installments = $subscription_length / $subscription_interval;

                    $initial_payment = ( $is_payment_change ) ? 0 : $order->get_total();

                    if ($initial_payment == '0.00') {
                        $initial_payment = '0.01';
                    }

                    if ($subscription_trial_length > 0) {

                        $trial_until = wcs_calculate_paypal_trial_periods_until($next_payment_timestamp);

                        $subscription_trial_length = $trial_until['first_trial_length'];
                        $converted_periods['trial_period'] = $trial_until['first_trial_period'];

                        $dateformat = "Ymd";
                        $todayDate = date($dateformat);
                        $startdate = date($dateformat, strtotime(date($dateformat, strtotime($todayDate)) . " +" . $subscription_trial_length . ' ' . $converted_periods['trial_period']));

                        $nmipay_args['plan_payments'] = $subscription_installments;

                        $nmipay_args['amount'] = $initial_payment;

                        $nmipay_args['plan_amount'] = $price_per_period;

                        if ($converted_periods['billing_period'] == 'day') {
                            $nmipay_args['day_frequency'] = $subscription_interval;
                        } else if ($converted_periods['billing_period'] == 'week') {
                            $nmipay_args['day_frequency'] = $subscription_interval * 7;
                        } else if ($converted_periods['billing_period'] == 'year') {
                            $nmipay_args['month_frequency'] = $subscription_interval * 12;
                            $timestamp = strtotime($startdate);
                            $day = date('d', $timestamp);
                            $nmipay_args['day_of_month'] = $day;
                        } else {
                            $nmipay_args['month_frequency'] = $subscription_interval;
                            $timestamp = strtotime($startdate);
                            $day = date('d', $timestamp);
                            $nmipay_args['day_of_month'] = $day;
                        }
                    } else {
                        $dateformat = "Ymd";
                        $startdate = date($dateformat);

                        $nmipay_args['plan_payments'] = $subscription_installments;

                        $nmipay_args['amount'] = $initial_payment;

                        $nmipay_args['plan_amount'] = $price_per_period;

                        if ($converted_periods['billing_period'] == 'day') {
                            $nmipay_args['day_frequency'] = $subscription_interval;
                            $startdate = date($dateformat, strtotime(date($dateformat, strtotime($startdate)) . ' +1 day'));
                        } else if ($converted_periods['billing_period'] == 'week') {
                            $nmipay_args['day_frequency'] = $subscription_interval * 7;
                            $startdate = date($dateformat, strtotime(date($dateformat, strtotime($startdate)) . ' +1 week'));
                        } else if ($converted_periods['billing_period'] == 'year') {
                            $nmipay_args['month_frequency'] = $subscription_interval * 12;
                            $startdate = date($dateformat, strtotime(date($dateformat, strtotime($startdate)) . ' +1 year'));
                            $timestamp = strtotime($startdate);
                            $day = date('d', $timestamp);
                            $nmipay_args['day_of_month'] = $day;
                        } else {
                            $nmipay_args['month_frequency'] = $subscription_interval;
                            $timestamp = strtotime($startdate);
                            $day = date('d', $timestamp);
                            $nmipay_args['day_of_month'] = $day;
                            $startdate = date($dateformat, strtotime(date($dateformat, strtotime($startdate)) . ' +1 month'));
                        }
                    }

                    $nmipay_args['start_date'] = $startdate;

                    $nmipay_args['recurring'] = 'add_subscription';

                    $nmipay_args['billing_method'] = 'recurring';
                }
            } else {
                $nmipay_args['type'] = $this->transtype;
                $nmipay_args['payment'] = 'creditcard';
                $nmipay_args['ipaddress'] = $_SERVER['REMOTE_ADDR'];
                $nmipay_args['username'] = $this->username;
                $nmipay_args['password'] = $this->password;
                $nmipay_args['currency'] = get_woocommerce_currency();

                $nmipay_args['orderid'] = $order_id . '-' . time();

                $nmipay_args = $this->set_billing_and_shipping_address($nmipay_args, $order);

                $nmipay_args['email'] = $order->get_billing_email();

                $nmipay_args['invoice'] = $order->order_key;

                $AmountInput = number_format($order->order_total, 2, '.', '');

                $nmipay_args['amount'] = $AmountInput;

                if (in_array($order->billing_country, array('US', 'CA'))) {
                    $order->billing_phone = str_replace(array('( ', '-', ' ', ' )', '.'), '', $order->billing_phone);
                    $nmipay_args['phone'] = $order->billing_phone;
                } else {
                    $nmipay_args['phone'] = $order->billing_phone;
                }
                //var_dump($order->get_total_tax());die;
                // Tax
                $nmipay_args['tax'] = $order->get_total_tax();

                // Cart Contents
                $item_loop = 0;
                if (sizeof($order->get_items()) > 0) {
                    foreach ($order->get_items() as $item) {
                        if ($item['qty']) {

                            $item_loop++;

                            $product = $order->get_product_from_item($item);

                            $item_name = $item['name'];

                            $item_meta = new WC_Order_Item_Meta($item['item_meta']);
                            if ($meta = $item_meta->display(true, true))
                                $item_name .= ' ( ' . $meta . ' )';

                            $nmipay_args['item_description_' . $item_loop] = $item_name;
                            $nmipay_args['item_quantity_' . $item_loop] = $item['qty'];
                            $nmipay_args['item_unit_cost_' . $item_loop] = $order->get_item_subtotal($item, false);

                            //if ( $product->get_sku() )
                            //	$nmipay_args[ 'product_sku_' . $item_loop ] = $product->get_sku();
                        }
                    }
                }

                // Discount
                if ($order->get_total_discount() > 0) {
                    $nmipay_args['discount_amount'] = number_format($order->get_total_discount(), 2, '.', '');
                }

                // Shipping Cost item - nmipay only allows shipping per item, we want to send shipping for the order
                if ($order->get_total_shipping() > 0) {
                    $nmipay_args['shipping'] = number_format($order->get_total_shipping(), 2, '.', '');
                }
            }

            // Processing standard
        } else {
            $nmipay_args['type'] = $this->transtype;
            $nmipay_args['payment'] = 'creditcard';
            $nmipay_args['ipaddress'] = $_SERVER['REMOTE_ADDR'];
            $nmipay_args['username'] = $this->username;
            $nmipay_args['password'] = $this->password;
            $nmipay_args['currency'] = get_woocommerce_currency();

            $nmipay_args['orderid'] = $order_id . '-' . time();

            $nmipay_args = $this->set_billing_and_shipping_address($nmipay_args, $order);

            $nmipay_args['email'] = $order->get_billing_email();

            $nmipay_args['invoice'] = $order->order_key;

            $AmountInput = number_format($order->order_total, 2, '.', '');

            $nmipay_args['amount'] = $AmountInput;

            if (in_array($order->billing_country, array('US', 'CA'))) {
                $order->billing_phone = str_replace(array('( ', '-', ' ', ' )', '.'), '', $order->billing_phone);
                $nmipay_args['phone'] = $order->billing_phone;
            } else {
                $nmipay_args['phone'] = $order->billing_phone;
            }
            //var_dump($order->get_total_tax());die;
            // Tax
            $nmipay_args['tax'] = $order->get_total_tax();

            // Cart Contents
            $item_loop = 0;
            if (sizeof($order->get_items()) > 0) {
                foreach ($order->get_items() as $item) {
                    if ($item['qty']) {

                        $item_loop++;

                        $product = $order->get_product_from_item($item);

                        $item_name = $item['name'];

                        $item_meta = new WC_Order_Item_Meta($item['item_meta']);
                        if ($meta = $item_meta->display(true, true))
                            $item_name .= ' ( ' . $meta . ' )';

                        $nmipay_args['item_description_' . $item_loop] = $item_name;
                        $nmipay_args['item_quantity_' . $item_loop] = $item['qty'];
                        $nmipay_args['item_unit_cost_' . $item_loop] = $order->get_item_subtotal($item, false);

                        //if ( $product->get_sku() )
                        //	$nmipay_args[ 'product_sku_' . $item_loop ] = $product->get_sku();
                    }
                }
            }

            // Discount
            if ($order->get_total_discount() > 0) {
                $nmipay_args['discount_amount'] = number_format($order->get_total_discount(), 2, '.', '');
            }

            // Shipping Cost item - nmipay only allows shipping per item, we want to send shipping for the order
            if ($order->get_total_shipping() > 0) {
                $nmipay_args['shipping'] = number_format($order->get_total_shipping(), 2, '.', '');
            }
        }

        $name_value_pairs = array();
        foreach ($nmipay_args as $key => $value) {
            $name_value_pairs[] = $key . '=' . urlencode($value);
        }
        $gateway_values = implode('&', $name_value_pairs);

        $response = wp_remote_post($nmi_adr . $gateway_values, array('sslverify' => false, 'timeout' => 60));

        if ($this->debug == 'yes') {
            $nmipay_args['ccnumber'] = "XXXX-XXXX-XXXX-XXXX";
            $nmipay_args['cvv'] = "XXX";
            $this->log->add('nmipay', $order_id . ' - NMI CC Order Request: ' . print_r($nmipay_args, true));
        }

        if (!is_wp_error($response) && $response['response']['code'] >= 200 && $response['response']['code'] < 300) {
            parse_str($response['body'], $response);

            if ($this->debug == 'yes') {
                $this->log->add('nmipay', $order_id . ' - NMI CC Order Response: ' . print_r($response, true));
            }

            if ($response['response'] == '1') {
                // Payment completed
                do_action('before_save_payment_token', $order_id);
                $order->add_order_note(sprintf(__('The NMI Payment transaction is successful. The Transaction Id is %s.', 'woo-nmi-patsatech'), $response["transactionid"]));
                if (isset($response['subscription_id'])) {
                    update_post_meta($order_id, 'NMI Subscriber ID', $response['subscription_id']);
                    WC_Subscriptions_Manager::activate_subscriptions_for_order($order);
                }

                update_post_meta($order_id, 'NMI Transaction ID', $response['transactionid']);

                if (isset($response['customer_vault_id']) && isset($_POST['wc-nmipay-new-payment-method']) && count($tokens) == 0) {
                    // Build the token
                    $token = new WC_Payment_Token_CC();
                    $token->set_token($response['customer_vault_id']); // Token comes from payment processor
                    $token->set_gateway_id('nmipay');
                    $token->set_last4($last_four_digits);
                    $token->set_expiry_year('20' . substr($ccexp_expiry, 3, 7));
                    $token->set_expiry_month($month);
                    $token->set_card_type($cardtype);
                    $token->set_user_id(get_current_user_id());
                    // Save the new token to the database
                    $save_result = $token->save();
                    if ($save_result) {
                        $order->add_payment_token($token);
                    }

                    update_user_meta(get_current_user_id(), 'nmi_cc_token_id', $token->get_id());

                    if ($this->debug == 'yes') {
                        $this->log->add('nmipay', $order_id . ' - New Token Saved : ' . $token->get_id());
                    }
                } else if (isset($response['customer_vault_id']) && isset($_POST['wc-nmipay-new-payment-method']) && count($tokens) > 0) {

                    $token_id = get_user_meta(get_current_user_id(), 'nmi_cc_token_id', true);
                    $token = WC_Payment_Tokens::get($token_id);
                    $token->set_token($response['customer_vault_id']); // Token comes from payment processor
                    $token->set_gateway_id('nmipay');
                    $token->set_last4($last_four_digits);
                    $token->set_expiry_year('20' . substr($ccexp_expiry, 3, 7));
                    $token->set_expiry_month($month);
                    $token->set_card_type($cardtype);
                    $token->set_user_id(get_current_user_id());
                    // Save the new token to the database
                    $save_result = $token->save();
                    if ($save_result) {
                        $order->add_payment_token($token);
                    }

                    update_user_meta(get_current_user_id(), 'nmi_cc_token_id', $token->get_id());

                    if ($this->debug == 'yes') {
                        $this->log->add('nmipay', $order_id . ' - Old Token Deleted : ' . $token_id . ' - New Token Saved : ' . $token->get_id());
                    }
                }
                $order->payment_complete($response["transactionid"]);
                return array(
                    'result' => 'success',
                    'redirect' => $this->get_return_url($order)
                );
            } else {

                if (strpos($response['responsetext'], 'Invalid Customer Vault Id') !== false) {// Build the token
                    $token_id = get_user_meta(get_current_user_id(), 'nmi_cc_token_id', true);
                    if (!empty($token_id)) {
                        $token = new WC_Payment_Tokens();
                        $token->delete($token_id);
                    }
                    if ($this->debug == 'yes') {
                        $this->log->add('nmipay', $order_id . ' - Token Deleted : ' . $token_id . ' - because of error : ' . $response['responsetext']);
                    }
                    return array(
                        'result' => 'success',
                        'redirect' => $order->get_checkout_payment_url()
                    );
                } else {
                    $order->add_order_note(sprintf(__('Transaction Failed. %s-%s', 'woo-nmi-patsatech'), $response['response_code'], $response['responsetext']));
                    wc_add_notice(sprintf(__('Transaction Failed. %s-%s', 'woo-nmi-patsatech'), $response['response_code'], $response['responsetext']), $notice_type = 'error');
                    if ($this->debug == 'yes') {
                        $this->log->add('nmipay', $order_id . ' - ' . sprintf(__('Transaction Failed. %s-%s', 'woo-nmi-patsatech'), $response['response_code'], $response['responsetext']));
                    }
                }
            }
        } else {
            $order->add_order_note(sprintf(__('Gateway Error. Please Notify the Store Owner about this error. %s', 'woo-nmi-patsatech'), print_r($response, TRUE)));
            wc_add_notice(__('Gateway Error. Please Notify the Store Owner about this error. %s', 'woo-nmi-patsatech'), $notice_type = 'error');
            if ($this->debug == 'yes') {
                $this->log->add('nmipay', $order_id . ' - ' . sprintf(__('Gateway Error. Please Notify the Store Owner about this error. %s', 'woo-nmi-patsatech'), print_r($response, TRUE)));
            }
        }
    }

    private function force_ssl($url) {

        if ('yes' == get_option('woocommerce_force_ssl_checkout')) {
            $url = str_replace('http:', 'https:', $url);
        }

        return $url;
    }

    private function is_empty_credit_card($credit_card) {

        if (empty($credit_card)) {
            return false;
        }

        return true;
    }

    private function is_valid_credit_card($credit_card) {

        $credit_card = preg_replace('/(?<=\d)\s+(?=\d)/', '', trim($credit_card));

        $number = preg_replace('/[^0-9]+/', '', $credit_card);
        $strlen = strlen($number);
        $sum = 0;
        if ($strlen < 13) {
            return false;
        }
        for ($i = 0; $i < $strlen; $i++) {
            $digit = substr($number, $strlen - $i - 1, 1);

            if ($i % 2 == 1) {

                $sub_total = $digit * 2;

                if ($sub_total > 9) {
                    $sub_total = 1 + ( $sub_total - 10 );
                }
            } else {
                $sub_total = $digit;
            }
            $sum += $sub_total;
        }

        if ($sum > 0 AND $sum % 10 == 0) {
            return true;
        }

        return false;
    }

    private function is_empty_expire_date($ccexp_expiry) {

        $ccexp_expiry = str_replace(array(' / ', '/', ' '), '', $ccexp_expiry);

        if (is_numeric($ccexp_expiry) && ( strlen($ccexp_expiry) == 4 )) {
            return true;
        }

        return false;
    }

    /*
     * Check expiry date is valid
     */

    private function is_valid_expire_date($ccexp_expiry) {

        $month = $year = '';
        $month = substr($ccexp_expiry, 0, 2);
        $year = substr($ccexp_expiry, 5, 7);
        $year = '20' . $year;

        if ($month > 12) {
            return false;
        }

        if (date("Y-m-d", strtotime($year . "-" . $month . "-01")) > date("Y-m-d")) {
            return true;
        }

        return false;
    }

    /*
     * Check whether the ccv number is empty
     */

    private function is_empty_ccv_nmber($ccv_number) {

        $length = strlen($ccv_number);

        return is_numeric($ccv_number) AND $length > 2 AND $length < 5;
    }

    private function getCardType($CCNumber) {

        $creditcardTypes = array(
            array('Name' => 'AMEX', 'cardLength' => array(15), 'cardPrefix' => array('34', '37')),
            array('Name' => 'Maestro', 'cardLength' => array(12, 13, 14, 15, 16, 17, 18, 19), 'cardPrefix' => array('5018', '5020', '5038', '6304', '6759', '6761', '6763')),
            array('Name' => 'MasterCard', 'cardLength' => array(16), 'cardPrefix' => array('51', '52', '53', '54', '55')),
            array('Name' => 'VISA', 'cardLength' => array(13, 16), 'cardPrefix' => array('4')),
            array('Name' => 'Discover', 'cardLength' => array(13, 16), 'cardPrefix' => array('6011', '65')),
            array('Name' => 'JCB', 'cardLength' => array(16), 'cardPrefix' => array('3528', '3529', '353', '354', '355', '356', '357', '358')),
            array('Name' => 'Diners', 'cardLength' => array(14), 'cardPrefix' => array('300', '301', '302', '303', '304', '305', '36')),
            array('Name' => 'Diners', 'cardLength' => array(16), 'cardPrefix' => array('54', '55')),
            array('Name' => 'Diners', 'cardLength' => array(14), 'cardPrefix' => array('300', '305'))
        );

        $CCNumber = trim($CCNumber);
        $type = 'VISA-SSL';
        foreach ($creditcardTypes as $card) {
            if (!in_array(strlen($CCNumber), $card['cardLength'])) {
                continue;
            }
            $prefixes = '/^(' . implode('|', $card['cardPrefix']) . ')/';
            if (preg_match($prefixes, $CCNumber) == 1) {
                $type = $card['Name'];
                break;
            }
        }
        return $type;
    }

}
