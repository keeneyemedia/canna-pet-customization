<?php

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Canna_Pet_Customization
 * @subpackage Canna_Pet_Customization/includes
 * @author     Angell EYE <service@angelleye.com>
 */
class Canna_Pet_Customization {

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Canna_Pet_Customization_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $plugin_name    The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $version    The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct() {

        $this->plugin_name = 'canna-pet-customization';
        $this->version = '1.2.10';
        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();
    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Canna_Pet_Customization_Loader. Orchestrates the hooks of the plugin.
     * - Canna_Pet_Customization_i18n. Defines internationalization functionality.
     * - Canna_Pet_Customization_Admin. Defines all hooks for the admin area.
     * - Canna_Pet_Customization_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_dependencies() {

        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-canna-pet-customization-loader.php';

        

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-canna-pet-customization-i18n.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-canna-pet-customization-admin.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-canna-pet-customization-public.php';
        
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-petconscious-checkout-payment.php';
        
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-petconscious-paypal-payflow-pro.php';
        
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-cannapets-mojopay.php';
        
        $this->loader = new Canna_Pet_Customization_Loader();
        
        add_filter('woocommerce_payment_gateways', array($this, 'own_canna_pet_woocommerce_payment_gateways'), 99);
    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Canna_Pet_Customization_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function set_locale() {

        $plugin_i18n = new Canna_Pet_Customization_i18n();

        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_admin_hooks() {

        $plugin_admin = new Canna_Pet_Customization_Admin($this->get_plugin_name(), $this->get_version());
        $this->loader->add_filter('woocommerce_email_classes', $plugin_admin, 'add_guest_customer_new_account_woocommerce_email');
        $this->loader->add_action('woocommerce_guest_customer_new_account_notification', $plugin_admin, 'send_guest_customer_new_account_woocommerce_email', 10, 1);
        $this->loader->add_action('woocommerce_guest_customer_new_account_notification', $plugin_admin, 'send_guest_customer_new_account_woocommerce_email', 10, 1);
        
        $this->loader->add_action('wp_ajax_export_autoship_schedules', $plugin_admin, 'own_wc_autoship_admin_ajax_export_autoship_schedules', 0);
                
        $this->loader->add_filter( 'woocommerce_payment_gateways', $plugin_admin,'add_your_gateway_class' );

    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_public_hooks() {

        $plugin_public = new Canna_Pet_Customization_Public($this->get_plugin_name(), $this->get_version());
        $this->loader->add_filter('pre_option_woocommerce_enable_guest_checkout', $plugin_public, 'angelleye_wc_autoship_get_option_enable_guest_checkout', 11, 1);
        $this->loader->add_action('payment_fields_saved_payment_methods', $plugin_public, 'payment_fields_saved_payment_methods', 11, 1);
        $this->loader->add_action('woocommerce_payment_complete', $plugin_public, 'angelleye_automated_account_logout_for_guest_checkout', 10);
        $this->loader->add_filter('the_title', $plugin_public, 'angelleye_change_page_title', 10, 1);
        $this->loader->add_filter('woocommerce_email_actions', $plugin_public, 'remove_woocommerce_created_customer', 10, 1);
        //$this->loader->add_filter( 'woocommerce_locate_template', $plugin_public, 'canna_pet_customization_woocommerce_locate_template', 10, 3 );
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'canna_pet_customization_woocommerce_hide_save_to_account', 10);
        //$this->loader->add_action('woocommerce_payment_successful_result', $plugin_public, 'angelleye_automated_account_creation_for_guest', 10, 2);
        //$this->loader->add_action('enable_automated_account_creation_for_guest_checkouts_paypal_express', $plugin_public, 'angelleye_automated_account_creation_for_guest_checkouts_for_express_checkout', 10, 1);
        $this->loader->add_action('before_save_payment_token', $plugin_public, 'angelleye_automated_account_creation_for_guest', 10, 1);
        $this->loader->add_action('woocommerce_checkout_order_processed', $plugin_public, 'own_woocommerce_checkout_order_processed', 10, 3);
        
        $this->loader->add_filter( 'woocommerce_payment_methods_list_item', $plugin_public, 'wc_get_account_saved_payment_methods_list_item_cc', 99, 2 );                                        
        
        $this->loader->add_action( 'woocommerce_before_checkout_form',$plugin_public, 'add_notice_for_dec_on_checkout_page');
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public , 'angelleye_payment_scripts' );
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run() {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     1.0.0
     * @return    string    The name of the plugin.
     */
    public function get_plugin_name() {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    Canna_Pet_Customization_Loader    Orchestrates the hooks of the plugin.
     */
    public function get_loader() {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */
    public function get_version() {
        return $this->version;
    }
    
    public function own_canna_pet_woocommerce_payment_gateways($methods) {
        if(class_exists('WC_Payment_Gateway_CC')) {
            if(class_exists('woocommerce_nmipay')) {
                require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-canna-pet-customization-payment.php';
                $object = new Canna_Pet_Customization_Payment();
            }
            if(class_exists('WC_Autoship_Authorize_Net_Gateway')) {
                require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-canna-pet-customization-authorize-net-payment.php';
                require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-canna-pet-customization-authorize-net-payment_two.php';
                require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-canna-pet-customization-authorize-net-payment_three.php';
                $object = new Canna_Pet_Customization_Authorize_Net_Payment();
                $object_2 = new Canna_Pet_Customization_Authorize_Net_Payment_Two();
                $object_3 = new Canna_Pet_Customization_Authorize_Net_Payment_Three();
            } 
        }
        if (class_exists('woocommerce_nmipay')) {
            $methods[] = 'Canna_Pet_Customization_Payment';
        }
        if(class_exists('WC_Autoship_Authorize_Net_Gateway')) {
            unset($methods['WC_Autoship_Authorize_Net_Gateway']);
            $methods[] = 'Canna_Pet_Customization_Authorize_Net_Payment';
            $methods[] = 'Canna_Pet_Customization_Authorize_Net_Payment_Two';
            $methods[] = 'Canna_Pet_Customization_Authorize_Net_Payment_Three';
        }
        return $methods;
    }

}
