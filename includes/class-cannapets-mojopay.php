<?php

class WC_Gateway_Cannapets_MojoPay extends WC_Payment_Gateway_CC {

    public $domain;

    /**
     * Constructor for the gateway.
     */
    public function __construct() {

        $this->id = 'cannapets_mojopay';
        $this->icon = apply_filters('woocommerce_petconscious_checkout_gateway_icon', '');
        $this->has_fields = false;
        $this->method_title = __('MojoPay', 'cannapet-customization');
        $this->method_description = __('Allows payments with Mojo Pay gateway.', 'cannapet-customization');

        // Load the settings.
        $this->init_form_fields();
        $this->init_settings();

        // Define user set variables
        $this->title = $this->get_option('title');
        $this->description = $this->get_option('description');
        $this->cardtypes = $this->get_option('cardtypes');
        $this->testmode = $this->get_option('testmode');
        $this->instructions = $this->get_option('instructions');
        $this->uapi_api_token = $this->get_option('uapi_api_token');

        $this->supports = array(
            'products',
            'refunds',
            'tokenization'
        );
        // Actions
        add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
        add_action('woocommerce_thankyou_' . $this->id, array($this, 'thankyou_page'));

        // Customer Emails
        add_action('woocommerce_email_before_order_table', array($this, 'email_instructions'), 10, 3);
        add_action('woocommerce_credit_card_form_fields', array($this, 'cannapets_mojopay_form_fields'), 10, 2);
    }

    /**
     * Initialise Gateway Settings Form Fields.
     */
    public function init_form_fields() {

        $this->form_fields = array(
            'enabled' => array(
                'title' => __('Enable/Disable', 'cannapet-customization'),
                'type' => 'checkbox',
                'label' => __('Enable MojoPay', 'cannapet-customization'),
                'default' => 'yes'
            ),
            'title' => array(
                'title' => __('Title', 'cannapet-customization'),
                'type' => 'text',
                'description' => __('This controls the title which the user sees during checkout.', 'cannapet-customization'),
                'default' => __('MojoPay', 'cannapet-customization'),
                'desc_tip' => true,
            ),
            'testmode' => array(
                'title' => __('MojoPay Sandbox', 'cannapet-customization'),
                'type' => 'checkbox',
                'label' => __('Enable MojoPay Sandbox', 'cannapet-customization'),
                'default' => 'yes',
                'description' => sprintf(__('MojoPay sandbox can be used to test payments.', 'cannapet-customization')),
            ),
            'uapi_api_token' => array(
                'title' => __('MojoPay Secure Api Token', 'cannapet-customization'),
                'type' => 'text',
                'description' => __('MojoPay will supply you with this token', 'cannapet-customization'),
                'default' => '',
                'desc_tip' => true,
            ),
            'description' => array(
                'title' => __('Description', 'cannapet-customization'),
                'type' => 'textarea',
                'description' => __('Payment method description that the customer will see on your checkout.', 'cannapet-customization'),
                'default' => __('Payment Information', 'cannapet-customization'),
                'desc_tip' => true,
            ),
            'instructions' => array(
                'title' => __('Instructions', 'cannapet-customization'),
                'type' => 'textarea',
                'description' => __('Instructions that will be added to the thank you page and emails.', 'cannapet-customization'),
                'desc_tip' => true,
            )
        );
    }

    public function add_payment_method() {

        if ($_POST['payment_method'] == $this->id) {

            if (isset($_POST[$this->id . '-card-number'])) {
                $cc_ccnumber = $_POST[$this->id . '-card-number'];
                $cc_ccnumber = str_replace(array(' ', '-'), '', $cc_ccnumber);
                $card_expiry = isset($_POST[$this->id . '-card-expiry']) ? wc_clean($_POST[$this->id . '-card-expiry']) : '';
                $cc_cvv = isset($_POST[$this->id . '-card-cvc']) ? wc_clean($_POST[$this->id . '-card-cvc']) : '';

                $expiry_araay = explode('/', str_replace(' ', '', $card_expiry));

                $cc_ccnumber = str_replace(array(' ', '-'), '', $cc_ccnumber);
                $cc_expiremonth = $expiry_araay[0];
                $cc_expireyear = $expiry_araay[1];

                if (strlen($cc_expireyear) == 4) {
                    $cc_expireyear = $cc_expireyear - 2000;
                }

                if (empty($cc_ccnumber) || !ctype_digit($cc_ccnumber)) {
                    wc_add_notice(__('Card number is invalid', 'cannapet-customization'), "error");
                    return;
                }

                $save_card = array(
                    'transaction' => array(
                        'process' => 'online',
                        'type' => 'tokenize'
                    ),
                    'requestData' => array(
                        'cardNumber' => $cc_ccnumber,
                        'cardExpiration' => $cc_expiremonth . $cc_expireyear,
                        'cardCVV' => $cc_cvv
                    )
                );
                $xml_save_card = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><UAPIRequest></UAPIRequest>");
                self::array_to_xml($save_card, $xml_save_card);
                $token_xml = $xml_save_card->asXML();        
                $this->add_log(sprintf(__('Canna-Pet Customization Version: %s', 'paypal-for-woocommerce'), CANNA_PET_CUSTOMIZATION_VERSION));
                $this->add_log(sprintf(__('WooCommerce Version: %s', 'paypal-for-woocommerce'), WC_VERSION));
                $this->add_log('Test Mode: ' . $this->testmode);
                $token_Resp = $this->_sendRequest($token_xml);                
                $resp = $this->_handleResponse($token_Resp); 
                $this->add_log('MojoPay Add Card Request: ' . print_r($this->_masked_request($token_xml), true));
                $this->add_log('MojoPay Response:  '. print_r($resp, true));
                if (0 == $resp['statusCode'] && ('000' === $resp['processorResponseCode'] || '801' === $resp['processorResponseCode'] || '802' === $resp['processorResponseCode'])) {
                    $cardType = $this->get_cc_type_mojopay($resp['cardType']);
                    $current_user = wp_get_current_user();
                    $customer_id = $current_user->ID;
                    $token = new WC_Payment_Token_CC();
                    $token->set_token($resp['token']);
                    $token->set_gateway_id($this->id);
                    $token->set_card_type($cardType);
                    $token->set_last4(substr($cc_ccnumber, -4));
                    $token->set_expiry_month($cc_expiremonth);
                    if (strlen($cc_expireyear) == 2) {
                        $cc_expireyear = $cc_expireyear + 2000;
                    }
                    $token->set_expiry_year($cc_expireyear);
                    $token->set_user_id($customer_id);
                    $save_result = $token->save();
                    if (!$save_result) {
                        wc_add_notice(__('Error saving WC payment token!', 'cannapet-customization'), 'error');
                        return;
                    }
                    return array(
                        'result' => 'success',
                        'redirect' => wc_get_endpoint_url('payment-methods')
                    );
                }
                else if(0 == $resp['statusCode'] && '000' != $resp['processorResponseCode']){
                    $processorResponse = $this->get_processor_response_message($resp['processorResponseCode']);                                        
                    wc_add_notice(__('Processor Error : ','canna-pet-customization').$processorResponse,'error');
                    return;
                }
                else if (2 == $resp['statusCode']) {
                    // Decline                
                    wc_add_notice(__('Payment failed or Payment declined.', 'cannapet-customization'), 'error');
                    return;
                } else if (3 == $resp['statusCode']) {
                    wc_add_notice(__('Sorry, there was an error: ' . $resp['code'] . ' ' . $resp['statusMessage'], 'cannapet-customization'), 'error');
                    return;
                } else {
                    wc_add_notice(__('Error: ' . $resp['statusMessage'], 'cannapet-customization'), 'error');
                    return;
                }
            } else {
                wc_add_notice(__('Invalid card number.', 'cannapet-customization'), 'error');
                return;
            }
        } else {
            wc_add_notice(__('Invalid Payment method.', 'cannapet-customization'), 'error');
            return;
        }
    }

    public function get_cc_type_mojopay($cardType) {
        if (isset($cardType) && $cardType == 'VI') {
            return __('Visa', 'cannapet-customization');
        } else if (isset($cardType) && $cardType == 'MC') {
            return __('MasterCard', 'cannapet-customization');
        } else if (isset($cardType) && $cardType == 'DI') {
            return __('Discover', 'cannapet-customization');
        } else if (isset($cardType) && $cardType == 'AX') {
            return __('American Express', 'cannapet-customization');
        } else {
            return __('Credit Card', 'cannapet-customization');
        }
    }

    public function get_icon() {
        $image_path = plugins_url('public/assets/images/payflow-cards.png', plugin_basename(dirname(__FILE__)));
        $icon = "<img src=\"$image_path\" alt='" . __('Pay with MojoPay', 'cannapet-customization') . "'/>";
        return apply_filters('angelleye_ec_checkout_icon', $icon, $this->id);
    }

    /**
     * Output for the order received page.
     */
    public function thankyou_page() {
        if ($this->instructions)
            echo wpautop(wptexturize($this->instructions));
    }

    /**
     * Add content to the WC emails.
     *
     * @access public
     * @param WC_Order $order
     * @param bool $sent_to_admin
     * @param bool $plain_text 
     */
    public function email_instructions($order, $sent_to_admin, $plain_text = false) {
        if ($this->instructions && !$sent_to_admin && 'cannapets_mojopay' === $order->get_payment_method()) {
            echo wpautop(wptexturize($this->instructions)) . PHP_EOL;
        }
    }

    public function payment_fields() {
        if ($description = $this->get_description()) {
            echo wpautop(wptexturize($description));
        }
        if ($this->supports('tokenization') && is_checkout()) {
            $this->tokenization_script();
            $this->saved_payment_methods();
            $this->form();
            $this->save_payment_method_checkbox();
        } else {
            $this->form();
        }
        do_action('payment_fields_saved_payment_methods', $this);
    }

    public function field_name($name) {
        return ' name="' . esc_attr($this->id . '-' . $name) . '" ';
    }

    public function cannapets_mojopay_form_fields($default_fields, $current_gateway_id) {
        if ($current_gateway_id == $this->id) {
            $fields = array(
                'card-number-field' => '<p class="form-row form-row-wide">
                                <label for="' . esc_attr($this->id) . '-card-number">' . apply_filters('cc_form_label_card_number', __('Card number', 'paypal-for-woocommerce'), $this->id) . ' <span class="required">*</span></label>
                                <input id="' . esc_attr($this->id) . '-card-number" class="input-text wc-credit-card-form-card-number" inputmode="numeric" autocomplete="off" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" placeholder="&bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull;" ' . $this->field_name('card-number') . ' />
                            </p>',
                'card-expiry-field' => '<p class="form-row form-row-first">
				<label for="' . esc_attr($this->id) . '-card-expiry">' . esc_html__('Expiry (MM/YY)', 'woocommerce') . ' <span class="required">*</span></label>
				<input id="' . esc_attr($this->id) . '-card-expiry" class="input-text wc-credit-card-form-card-expiry" inputmode="numeric" autocomplete="off" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" placeholder="' . esc_attr__('MM / YY', 'woocommerce') . '" ' . $this->field_name('card-expiry') . ' />
			</p>',
                '<p class="form-row form-row-last">
                                <label for="' . esc_attr($this->id) . '-card-cvc">' . apply_filters('cc_form_label_card_code', __('Card Security Code', 'paypal-for-woocommerce'), $this->id) . ' <span class="required">*</span></label>
                                <input id="' . esc_attr($this->id) . '-card-cvc" class="input-text wc-credit-card-form-card-cvc" inputmode="numeric" autocomplete="off" autocorrect="no" autocapitalize="no" spellcheck="no" type="password" maxlength="4" placeholder="' . esc_attr__('CVC', 'paypal-for-woocommerce') . '" ' . $this->field_name('card-cvc') . ' style="width:100px" />
                            </p>'
            );
            return $fields;
        } else {
            return $default_fields;
        }
    }
    
    public function add_log($message, $level = 'info') {        
        if (version_compare(WC_VERSION, '3.0', '<')) {
            if (empty($this->log)) {
                $this->log = new WC_Logger();
            }
            $this->log->add('cannapets_mojopay', $message);
        } else {
            if (empty($this->log)) {
                $this->log = wc_get_logger();
            }
            $this->log->log($level, $message, array('source' => 'cannapets_mojopay'));
        }
    }
    
    /**
     * Returns string trimmed to max length (if it exceeds it)
     *
     * @return string
     * @access protected
     */
    function _getfield($string, $length)
    {
            if( !empty($string) ) {
                return substr($string, 0, $length);
            } else {
                return $string;
            }
    }
    
    /**
     * Process the payment and return the result.
     *
     * @param int $order_id
     * @return array
     */
    public function process_payment($order_id) {

        $order = wc_get_order($order_id);    
        if(isset($return['success']) && $return['success'] == false){
            wc_add_notice(__($return['message'], 'cannapet-customization'), "error");
                return false;
        }        
        if (empty($_POST['wc-cannapets_mojopay-payment-token']) || $_POST['wc-cannapets_mojopay-payment-token'] == 'new') {

            $cc_ccnumber = isset($_POST['cannapets_mojopay-card-number']) ? wc_clean($_POST['cannapets_mojopay-card-number']) : '';
            $card_expiry = isset($_POST['cannapets_mojopay-card-expiry']) ? wc_clean($_POST['cannapets_mojopay-card-expiry']) : '';
            $cc_cvv = isset($_POST['cannapets_mojopay-card-cvc']) ? wc_clean($_POST['cannapets_mojopay-card-cvc']) : '';

            $expiry_araay = explode('/', str_replace(' ', '', $card_expiry));

            $cc_ccnumber = str_replace(array(' ', '-'), '', $cc_ccnumber);
            $cc_expiremonth = $expiry_araay[0];
            $cc_expireyear = $expiry_araay[1];

            if (isset($_POST['wc-cannapets_mojopay-new-payment-method']) && $_POST['wc-cannapets_mojopay-new-payment-method'] == 'true') {
                $save_to_account = true;
            } else {
                $save_to_account = false;
            }

            if (strlen($cc_expireyear) == 4) {
                $cc_expireyear = $cc_expireyear - 2000;
            }

            // Check card security code
            if (!ctype_digit($cc_cvv)) {
                wc_add_notice(__('Card security code is invalid (only digits are allowed)', 'cannapet-customization'), "error");
                return false;
            }

            // Check card expiration data

            if (!ctype_digit($cc_expiremonth) || !ctype_digit($cc_expireyear) || $cc_expiremonth > 12 || $cc_expiremonth < 1 || $cc_expireyear < date('y') || $cc_expireyear > date('y') + 20) {
                wc_add_notice(__('Card expiration date is invalid', 'cannapet-customization'), "error");
                return false;
            }

            // Check card number

            if (empty($cc_ccnumber) || !ctype_digit($cc_ccnumber)) {
                wc_add_notice(__('Card number is invalid', 'cannapet-customization'), "error");
                return false;
            }                        
            $reqData = $this->_generateRequest($order, $cc_ccnumber, $cc_expiremonth, $cc_expireyear, $cc_cvv);            
            $this->add_log(sprintf(__('Canna-Pet Customization Version: %s', 'paypal-for-woocommerce'), CANNA_PET_CUSTOMIZATION_VERSION));
            $this->add_log(sprintf(__('WooCommerce Version: %s', 'paypal-for-woocommerce'), WC_VERSION));
            $this->add_log('Test Mode: ' . $this->testmode);
            $this->add_log('MojoPay Request Order ID: '. $order_id);  
            $reqResp = $this->_sendRequest($reqData);
            if (!empty($reqResp['CURL_ERROR'])) {
                return false;
            }
            $this->add_log('MojoPay Request: ' . print_r($this->_masked_request($reqData), true)); 
            $this->add_log('MojoPay Response Order ID: '. $order_id);
            $this->add_log('MojoPay Response:  '. print_r($reqResp, true));
            $resp = $this->_handleResponse($reqResp);     
            if (0 == $resp['statusCode'] && ('000' === $resp['processorResponseCode'] || '801' === $resp['processorResponseCode'] || '802' === $resp['processorResponseCode'])) {
                $cardType = $this->get_cc_type_mojopay($resp['cardType']);
                $customer_id = $order->get_user_id();
                $token = new WC_Payment_Token_CC();
                $token->set_token($resp['token']);
                $token->set_gateway_id($this->id);
                $token->set_card_type($cardType);
                $token->set_last4(substr($cc_ccnumber, -4));
                $token->set_expiry_month($cc_expiremonth);
                if (strlen($cc_expireyear) == 2) {
                    $cc_expireyear = $cc_expireyear + 2000;
                }
                $token->set_expiry_year($cc_expireyear);
                $token->set_user_id($customer_id);
                $save_result = $token->save();
                if ($save_result) {
                    $order->add_payment_token($token);
                }
                $order->add_order_note(__('Payment Transaction ID: ', 'canna-pet-customization') . $resp['transactionId']);
                $order->payment_complete($resp['transactionId']);

                update_post_meta($order_id, 'authCode', $resp['authCode']);
                update_post_meta($order_id, 'statusMessage', $resp['statusMessage']);
                update_post_meta($order_id, 'action_code', $resp['action_code']);
                update_post_meta($order_id, 'avsResponse', $resp['avsResponse']);
                update_post_meta($order_id, 'cvvResponse', $resp['cvvResponse']);

                return array(
                    'result' => 'success',
                    'redirect' => $this->get_return_url($order),
                );
            }
            
            else if(0 == $resp['statusCode'] && '000' != $resp['processorResponseCode']){                
                $processorResponse = $this->get_processor_response_message($resp['processorResponseCode']);
                if(!empty($processorResponse)){
                    $order->add_order_note(__('Processor Response : ','canna-pet-customization').$processorResponse);
                }
                update_post_meta($order_id, 'authCode', $resp['authCode']);
                update_post_meta($order_id, 'statusMessage', $resp['statusMessage']);
                update_post_meta($order_id, 'action_code', $resp['action_code']);
                update_post_meta($order_id, 'avsResponse', $resp['avsResponse']);
                update_post_meta($order_id, 'cvvResponse', $resp['cvvResponse']);    
                wc_add_notice("Processor Response : " . $processorResponse, 'error' );
                return array(
                        'result'   => 'fail',
                        'redirect' => '',
                );
            }
            //Response , Response Code, Response Text
            else if (2 == $resp['statusCode']) {
                // Decline                
                wc_add_notice(__('Payment failed or Payment declined.'), 'error');
                return;
            } else if (3 == $resp['statusCode']) {
                wc_add_notice(__('Sorry, there was an error: ') . $resp['code'] . ' ' . $resp['statusMessage'], 'error');
                return;
            } else {
                // No response or unexpected response                           
                wc_add_notice(__('Error: ' . $resp['statusMessage']), 'error');
                return;
            }
        } else {
            /* Do referance transaction here */
            $payment_token_id = $_POST['wc-cannapets_mojopay-payment-token'];
            $payment_token = WC_Payment_Tokens::get($payment_token_id);
            $cardToken = $payment_token->get_token();
            $billingAddress = $order->get_billing_address_1();
            $billingAddress = str_replace('+', ' ', $billingAddress); 
            $billingAddress = urlencode($billingAddress);
            $billingAddress = $this->_getField($billingAddress, 35);
            
            $billingPhone = $this->_getField($order->get_billing_phone(), 20);
            $billingPhone = str_replace(array(' ','-','(',')','+','/'), '', $billingPhone);
            $billingPhone = preg_replace('/[^0-9]/', '', $billingPhone); 
            
            $billingCity = $this->_getField($order->get_billing_city(), 19);
            $billingCity = str_replace(array(' ','-','(',')','+','/'), '', $billingCity);
            $billingCity = preg_replace('/[^A-Za-z]/', '', $billingCity);
               
            $expiry_month = $payment_token->get_expiry_month();
            
            $expiry_year = $payment_token->get_expiry_year();
            
            if(strlen($expiry_year) == 4){
                $expiry_year = $expiry_year - 2000;
            }
                        
            $sale_with_token_array = array(
                'transaction' => array(
                    'process' => 'online',
                    'type' => 'sale'
                ),
                'requestData' => array(
                    'orderId' => $order->get_id(),
                    'orderAmount' => $order->get_total(),
                    'billing' => array(
                        'billingFirstName' => htmlentities($this->_getField($order->get_billing_first_name(), 25)),
                        'billingLastName' => $this->_getField($order->get_billing_last_name(), 25),
                        'billingAddressLine1' => trim($billingAddress),
                        'billingCity' => $billingCity,
                        'billingState' => $this->_getField($order->get_billing_state(), 2),
                        'billingZipCode' => $this->_getField($order->get_billing_postcode(), 13),
                        'billingCountry' => $this->_getField($order->get_billing_country(), 3),
                        'billingEmail' => $this->_getField($order->get_billing_email(), 100),
                        'billingPhone' => $billingPhone
                    ),
                    'cardToken' => $cardToken,
                    'expDate' => $expiry_month.$expiry_year,
                    /*  'cardValidationNum' => '', need to ask about this field */
                    'recurringTransaction' => 'false',
                    'currencyCode' => '840'
                )
            );
            $xml_sale_with_token = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><UAPIRequest></UAPIRequest>");
            self::array_to_xml($sale_with_token_array, $xml_sale_with_token);
            $sale_with_token_xml = $xml_sale_with_token->asXML();
            $this->add_log(sprintf(__('Canna-Pet Customization Version: %s', 'paypal-for-woocommerce'), CANNA_PET_CUSTOMIZATION_VERSION));
            $this->add_log(sprintf(__('WooCommerce Version: %s', 'paypal-for-woocommerce'), WC_VERSION));
            $this->add_log('Test Mode: ' . $this->testmode);
            $this->add_log('MojoPay Request Order ID: '. $order_id);  
            $this->add_log('MojoPay Request: ' . $sale_with_token_xml);  
            $sale_with_token_Resp = $this->_sendRequest($sale_with_token_xml);
            if (!empty($sale_with_token_Resp['CURL_ERROR'])) {
                return false;
            }
            $this->add_log('MojoPay Response Order ID: '. $order_id);
            $this->add_log('MojoPay Response:  '. print_r($sale_with_token_Resp, true));
            $resp = $this->_handleResponse($sale_with_token_Resp);
            if (0 == $resp['statusCode'] && ('000' === $resp['processorResponseCode'] || '801' === $resp['processorResponseCode'] || '802' === $resp['processorResponseCode'])) {
                $order->add_order_note(__('MojoPay Commerce payment Transaction ID: ', 'canna-pet-customization') . $resp['transactionId']);
                $order->payment_complete($resp['transactionId']);

                update_post_meta($order_id, 'authCode', $resp['authCode']);
                update_post_meta($order_id, 'statusMessage', $resp['statusMessage']);
                update_post_meta($order_id, 'avsResponse', $resp['avsResponse']);
                update_post_meta($order_id, 'cvvResponse', $resp['cvvResponse']);

                return array(
                    'result' => 'success',
                    'redirect' => $this->get_return_url($order),
                );
            }
            else if(0 == $resp['statusCode'] && '000' != $resp['processorResponseCode']){                
                $processorResponse = $this->get_processor_response_message($resp['processorResponseCode']);
                if(!empty($processorResponse)){
                    $order->add_order_note(__('Processor Response : ','canna-pet-customization').$processorResponse);
                }
                update_post_meta($order_id, 'authCode', $resp['authCode']);
                update_post_meta($order_id, 'statusMessage', $resp['statusMessage']);                
                update_post_meta($order_id, 'avsResponse', $resp['avsResponse']);
                update_post_meta($order_id, 'cvvResponse', $resp['cvvResponse']);         
                $order->add_order_note("Processor Response : " . $processorResponse);
                return array(
                    'result' => 'fail',
                    'redirect' => '',
                );
            }
            //Response , Response Code, Response Text
            else if (2 == $resp['statusCode']) {
                // Decline                
                wc_add_notice(__('Payment failed or Payment declined.'), 'error');
                return;
            } else if (3 == $resp['statusCode']) {
                wc_add_notice(__('Sorry, there was an error: ') . $resp['code'] . ' ' . $resp['statusMessage'], 'error');
                return;
            } else {
                // No response or unexpected response                                
                wc_add_notice(__('Error: ' . $resp['statusMessage']), 'error');
                return;
            }
        }
    }
    /**
     * _sendRequest
     * Posts the request to MojoPay & returns response using curl
     *
     * @param string $url
     * @param string $content
     *
     */
    public function _sendRequest($get_string) {
        // TODO: Add option to authorize or charge
        $base_url = $this->testmode == 'no' ?
                'https://uapi.total-apps.com/universalapi' :
                'https://uapidev.total-apps.com/universalapi';

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $base_url,
            CURLOPT_VERBOSE => 1,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSLVERSION => 6,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $get_string,
            CURLOPT_HTTPHEADER => array(
                "authorized: " . $this->uapi_api_token,
                "cache-control: no-cache",
                "content-type: text/xml"
            ),
        ));

        $response = curl_exec($curl);

        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($response === false || $httpCode != 200) {
            wc_add_notice(curl_error($curl), 'error');
            if( empty($response) ) {
                $response = array();
                $error = curl_error($curl);
                if( !empty($error) ) {
                    $response['CURL_ERROR'] = $error;
                }
            }
            return $response;
        } else {
            return $response;
        }
        curl_close($curl);
    }

    function _masked_request($request){
        $r= new SimpleXMLElement($request);
        if(isset($r->requestData->card->cardNumber)){
            $cc_newstring = substr($r->requestData->card->cardNumber, -4);
            $masked_cc_number = 'xxxx'.$cc_newstring;
            $r->requestData->card->cardNumber = $masked_cc_number;
        }
        if(isset($r->requestData->card->cardCVV)){
            $r->requestData->card->cardCVV = '***';
        }
        return $r->asXML();
    }
    
    /**
     * Generates the GET request for processing the transaction at the gateway
     *
     * @return string
     * @access protected
     */
    function _generateRequest($order, $cc_ccnumber, $cc_expiremonth, $cc_expireyear, $cc_cvv) {

        $type = 'sale'; //sale, auth, credit, validate
        $apiToken = $this->uapi_api_token;           
        $billingAddress = $order->get_billing_address_1();
        $billingAddress = str_replace('+', ' ', $billingAddress); 
        $billingAddress = urlencode($billingAddress);
        $billingAddress = $this->_getField($billingAddress, 35);
        
        $billingPhone = $order->get_billing_phone();
        $billingPhone = str_replace(array(' ','-','(',')','+','/'), '', $billingPhone);
        $billingPhone = preg_replace('/[^0-9]/', '', $billingPhone); 
                
        $City = $this->_getField($order->get_billing_city(), 19);
        $City = str_replace(array(' ','-','(',')','+','/'), '', $City);
        $City = preg_replace('/[^A-Za-z]/', '', $City);
            
        $cc_expireyear = substr($cc_expireyear, -2);        
        
        $xml = new DOMDocument("1.0", "UTF-8");
        $xml->preserveWhiteSpace = false;
        $xml->formatOutput = true;

        $xml_root = $xml->createElement("UAPIRequest");
        $xml->appendChild($xml_root);

        $transaction = $xml->createElement("transaction");
        $xml_root->appendChild($transaction);

        $process = $xml->createElement('process', 'online');
        $transaction->appendChild($process);
        $type = $xml->createElement('type', 'sale');
        $transaction->appendChild($type);

        $requestData = $xml->createElement("requestData");
        $xml_root->appendChild($requestData);

        $orderId = $xml->createElement('orderId', $order->get_id());
        $requestData->appendChild($orderId);
        $orderAmount = $xml->createElement('orderAmount', $order->get_total());
        $requestData->appendChild($orderAmount);

        $billing = $xml->createElement("billing");
        $requestData->appendChild($billing);        
        $billingFirstName = $xml->createElement("billingFirstName",htmlentities($this->_getField($order->get_billing_first_name(), 25))); 
        $billing->appendChild($billingFirstName);        
        $billingLastName = $xml->createElement("billingLastName",$this->_getField($order->get_billing_last_name(), 25)); 
        $billing->appendChild($billingLastName);
        $billingAddressLine1 = $xml->createElement("billingAddressLine1",trim($billingAddress)); 
        $billing->appendChild($billingAddressLine1);                
        $billingCity = $xml->createElement("billingCity",$City); 
        $billing->appendChild($billingCity);        
        $billingState = $xml->createElement("billingState",$this->_getField($order->get_billing_state(), 2)); 
        $billing->appendChild($billingState);
        $billingZipCode = $xml->createElement("billingZipCode",$this->_getField($order->get_billing_postcode(), 13)); 
        $billing->appendChild($billingZipCode);
        $billingCountry = $xml->createElement("billingCountry",$this->_getField($order->get_billing_country(), 3)); 
        $billing->appendChild($billingCountry);
        $billingEmail = $xml->createElement("billingEmail", $this->_getField($order->get_billing_email(), 100));
        $billing->appendChild($billingEmail);        
        $billingPhone = $xml->createElement("billingPhone",$this->_getField($billingPhone, 20)); 
        $billing->appendChild($billingPhone);        
        $card = $xml->createElement("card");
        $requestData->appendChild($card);
        $cardNumber = $xml->createElement("cardNumber", $cc_ccnumber);
        $card->appendChild($cardNumber);
        $cardExpiration = $xml->createElement("cardExpiration", $cc_expiremonth . $cc_expireyear);
        $card->appendChild($cardExpiration);
        $cardCVV = $xml->createElement("cardCVV", $cc_cvv);
        $card->appendChild($cardCVV);
        $orderAmount = $xml->createElement('currencyCode', 840);
        $requestData->appendChild($orderAmount);

        $xml = $xml->saveXML();
        return $xml;
    }

    /**
     * Proceeds the simple payment
     *
     * @param string $response
     * @return array
     *
     */
    public function _handleResponse($response) {               
        $response = str_replace(array('&lt;','&gt;'),' ', $response);
        
        $xml = simplexml_load_string($response);
        $json = json_encode($xml);
        $jsonResp = json_decode($json, true);

        $retArr = array(
            'statusCode' => isset($jsonResp['statusCode']) ? $jsonResp['statusCode'] : '',
            'transactionId' => isset($jsonResp['transactionId']) ? $jsonResp['transactionId'] : '',
            'authCode' => isset($jsonResp['authCode']) ? $jsonResp['authCode'] : '',            
            'statusMessage' => isset($jsonResp['statusMessage']) ? $jsonResp['statusMessage'] : '',
            'avsResponse' => isset($jsonResp['avsResponse']) ? $jsonResp['avsResponse'] : '',
            'cvvResponse' => isset($jsonResp['cvvResponse']) ? $jsonResp['cvvResponse'] : '',
            'raw' => isset($jsonResp) ? $jsonResp : '',
            'token' => isset($jsonResp['token']) ? $jsonResp['token'] : '',
            'tokenizeResponseMessage' => isset($jsonResp['tokenizeResponseMessage']) ? $jsonResp['tokenizeResponseMessage'] : '',
            'tokenizeResponseCode' => isset($jsonResp['tokenizeResponseCode']) ? $jsonResp['tokenizeResponseCode'] : '',
            'processorTransactionID' => isset($jsonResp['processorTransactionID']) ? $jsonResp['processorTransactionID'] : '',
            'processorResponseCode' => isset($jsonResp['processorResponseCode']) ? $jsonResp['processorResponseCode'] : '',
            'processorResponse' => isset($jsonResp['processorResponse']) ? $jsonResp['processorResponse'] : '',
            'cardType' => isset($jsonResp['type']) ? $jsonResp['type'] : '',
            'processor' => isset($jsonResp['processor']) ? $jsonResp['processor'] : ''
        );
        return $retArr;
    }

    public static function array_to_xml($array, &$xml_user_info) {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    $subnode = $xml_user_info->addChild("$key");
                    self::array_to_xml($value, $subnode);
                } else {
                    $subnode = $xml_user_info->addChild("item$key");
                    self::array_to_xml($value, $subnode);
                }
            } else {
                $xml_user_info->addChild("$key", htmlspecialchars("$value"));
            }
        }
    }

    /**
     * Process a refund if supported
     * @param  int $order_id
     * @param  float $amount
     * @param  string $reason
     * @return  bool|wp_error True or false based on success, or a WP_Error object
     */
    public function process_refund($order_id, $amount = null, $reason = '') {

        $order = wc_get_order($order_id);
        if (!$order || !$order->get_transaction_id()) {
            return false;
        }

        if ($amount <= 0) {
            return new WP_Error('mojopay-refund-error', 'Error: Please enter valid refund amount.');
        }
                        
        $credit_transaction = array(
            'transaction' => array(
                'process' => 'online',
                'type' => 'credit'
            ),
            'requestData' => array(
                'orderAmount' => $amount,
                'uapiTransactionId' => $order->get_transaction_id()
            )
        );

        $xml_refund = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><UAPIRequest></UAPIRequest>");
        self::array_to_xml($credit_transaction, $xml_refund);
        $credit_xml = $xml_refund->asXML();
        $this->add_log('MojoPay Refund Request (Order ID - '.$order_id.'): ' . print_r($credit_xml, true));
        $credit_Resp = $this->_sendRequest($credit_xml);
        if (!empty($credit_Resp['CURL_ERROR'])) {
            return false;
        }
        $this->add_log('MojoPay Refund Response (Order ID - '.$order_id.'): ' . print_r($credit_Resp, true));
        $resp = $this->_handleResponse($credit_Resp);

        if (0 == $resp['statusCode'] && '000' === $resp['processorResponseCode']) {
            update_post_meta($order_id, 'Refund Transaction ID', $resp['transactionId']);
            $order->add_order_note('Refund Transaction ID:' . $resp['transactionId']);                        
            return true;
        }
        else if(0 == $resp['statusCode'] && '000' != $resp['processorResponseCode']){
            update_post_meta($order_id, 'Refund Transaction ID', $resp['transactionId']);
            $order->add_order_note('Refund Transaction ID:' . $resp['transactionId']);
            $processorResponse = $this->get_processor_response_message($resp['processorResponseCode']);
            return new WP_Error('mojopay-refund-error', 'Processor Response : '.$processorResponse);
        }
        //Response , Response Code, Response Text
        else if (2 == $resp['statusCode']) {
            // Decline                
            return new WP_Error('mojopay-refund-error', 'Payment failed or Payment declined.');
        } else if (3 == $resp['statusCode']) {
            return new WP_Error('mojopay-refund-error', 'Sorry, there was an error: ' . $resp['code'] . ' ' . $resp['statusMessage']);
        } else {
            // No response or unexpected response                                            
            return new WP_Error('mojopay-refund-error', 'Error: ' . $resp['statusMessage']);
        }
        return false;
    }
    
    public function get_processor_response_message($responseCode) {
        $responseCodesHumanReadable = array(
            000 => __('Approved','cannapet-customization'),
            010 => __('Partially Approved','cannapet-customization'),
            100 => __('Processing Network Unavailable','cannapet-customization'),
            101 => __('Issuer Unavailable','cannapet-customization'),
            102 => __('Re-submit Transaction','cannapet-customization'),
            110 => __('Insufficient Funds','cannapet-customization'),
            111 => __('Authorization amount has already been depleted','cannapet-customization'),
            120 => __('Call Issuer','cannapet-customization'),
            121 => __('Call AMEX','cannapet-customization'),
            122 => __('Call Diners Club','cannapet-customization'),
            123 => __('Call Discover','cannapet-customization'),
            124 => __('Call JBS','cannapet-customization'),
            125 => __('Call Visa/MasterCard','cannapet-customization'),
            126 => __('Call Issuer - Update Cardholder Data','cannapet-customization'),
            127 => __('Exceeds Approval Amount Limit','cannapet-customization'),
            130 => __('Call Indicated Number','cannapet-customization'),
            140 => __('Update Cardholder Data','cannapet-customization'),
            191 => __('The merchant is not registered in the update program.','cannapet-customization'),
            192 => __('Merchant not certified/enabled for IIAS','cannapet-customization'),
            206 => __('Issuer Generated Error','cannapet-customization'),
            207 => __('Pickup card - Other than Lost/Stolen','cannapet-customization'),
            209 => __('Invalid Amount','cannapet-customization'),
            211 => __('Reversal Unsuccessful','cannapet-customization'),
            212 => __('Missing Data','cannapet-customization'),
            213 => __('Pickup Card - Lost Card','cannapet-customization'),
            214 => __('Pickup Card - Stolen Card','cannapet-customization'),
            215 => __('Restricted Card','cannapet-customization'),
            216 => __('Invalid Deactivate','cannapet-customization'),
            217 => __('Card Already Active','cannapet-customization'),
            218 => __('Card Not Active','cannapet-customization'),
            219 => __('Card Already Deactivate','cannapet-customization'),
            221 => __('Over Max Balance','cannapet-customization'),
            222 => __('Invalid Activate','cannapet-customization'),
            223 => __('No transaction Found for Reversal','cannapet-customization'),
            226 => __('Incorrect CVV','cannapet-customization'),
            229 => __('Illegal Transaction','cannapet-customization'),
            251 => __('Duplicate Transaction','cannapet-customization'),
            252 => __('System Error','cannapet-customization'),
            253 => __('Deconverted BIN','cannapet-customization'),
            254 => __('Merchant Depleted','cannapet-customization'),
            255 => __('Gift Card Escheated','cannapet-customization'),
            256 => __('Invalid Reversal Type for Credit Card Transaction','cannapet-customization'),
            257 => __('System Error (message format error)','cannapet-customization'),
            258 => __('System Error (cannot process)','cannapet-customization'),
            301 => __('Invalid Account Number','cannapet-customization'),
            302 => __('Account Number Does Not Match Payment Type','cannapet-customization'),
            303 => __('Pick Up Card','cannapet-customization'),
            304 => __('Lost/Stolen Card','cannapet-customization'),
            305 => __('Expired Card','cannapet-customization'),
            306 => __('Authorization has expired; no need to reverse','cannapet-customization'),
            307 => __('Restricted Card','cannapet-customization'),
            308 => __('Restricted Card - Chargeback','cannapet-customization'),
            309 => __('Restricted Card - Prepaid Card Filtering Service','cannapet-customization'),
            310 => __('Invalid track data','cannapet-customization'),
            311 => __('Deposit is already referenced by a chargeback','cannapet-customization'),
            312 => __('Restricted Card - International Card Filtering Service','cannapet-customization'),
            313 => __('International filtering for issuing card country <country> (where <country> is the 3-character country code)','cannapet-customization'),
            315 => __('Restricted Card - Auth Fraud Velocity Filtering Service','cannapet-customization'),
            316 => __('Automatic Refund Already Issued','cannapet-customization'),
            318 => __('Restricted Card - Auth Fraud Advice Filtering Service','cannapet-customization'),
            319 => __('Restricted Card - Fraud AVS Filtering Service','cannapet-customization'),
            320 => __('Invalid Expiration Date','cannapet-customization'),
            321 => __('Invalid Merchant','cannapet-customization'),
            322 => __('Invalid Transaction','cannapet-customization'),
            323 => __('No such issuer','cannapet-customization'),
            324 => __('Invalid Pin','cannapet-customization'),
            325 => __('Transaction not allowed at terminal','cannapet-customization'),
            326 => __('Exceeds number of PIN entries','cannapet-customization'),
            327 => __('Cardholder transaction not permitted','cannapet-customization'),
            328 => __('Cardholder requested that recurring or installment payment be stopped','cannapet-customization'),
            330 => __('Invalid Payment Type','cannapet-customization'),
            331 => __('Invalid POS Capability for Cardholder Authorized Terminal Transaction','cannapet-customization'),
            332 => __('Invalid POS Cardholder ID for Cardholder Authorized Terminal Transaction','cannapet-customization'),
            335 => __('This method of payment does not support authorization reversals','cannapet-customization'),
            336 => __('Reversal amount does not match Authorization amount.','cannapet-customization'),
            340 => __('Invalid Amount','cannapet-customization'),
            341 => __('Invalid Healthcare Amounts','cannapet-customization'),
            346 => __('Invalid billing descriptor prefix','cannapet-customization'),
            347 => __('Invalid billing descriptor','cannapet-customization'),
            348 => __('Invalid Report Group','cannapet-customization'),
            349 => __('Do Not Honor','cannapet-customization'),
            350 => __('Generic Decline','cannapet-customization'),
            351 => __('Decline - Request Positive ID','cannapet-customization'),
            352 => __('Decline CVV2/CID Fail','cannapet-customization'),
            354 => __('3-D Secure transaction not supported by merchant','cannapet-customization'),
            356 => __('Invalid purchase level III, the transaction contained bad or missing data','cannapet-customization'),
            357 => __('Missing healthcareIIAS tag for an FSA transaction','cannapet-customization'),
            358 => __('Restricted by Litle due to security code mismatch.','cannapet-customization'),
            360 => __('No transaction found with specified litleTxnId','cannapet-customization'),
            361 => __('Authorization no longer available','cannapet-customization'),
            362 => __('Transaction Not Voided - Already Settled','cannapet-customization'),
            363 => __('Auto-void on refund','cannapet-customization'),
            364 => __('Invalid Account Number - original or NOC up','cannapet-customization'),
            365 => __('Total credit amount exceeds capture amount','cannapet-customization'),
            366 => __('Exceed the threshold for sending redeposits','cannapet-customization'),
            367 => __('Deposit has not been returned for insufficient/non-sufficient funds','cannapet-customization'),
            368 => __('Invalid check number','cannapet-customization'),
            369 => __('Redeposit against invalid transaction type','cannapet-customization'),
            370 => __('Internal System Error - Call Litle','cannapet-customization'),
            372 => __('Soft Decline - Auto Recycling In Progress','cannapet-customization'),
            373 => __('Hard Decline - Auto Recycling Complete','cannapet-customization'),
            375 => __('Merchant is not enabled for surcharging','cannapet-customization'),
            376 => __('This method of payment does not support surcharging','cannapet-customization'),
            377 => __('Surcharge is not valid for debit or prepaid cards','cannapet-customization'),
            378 => __('Surcharge cannot exceed 4% of the sale amount','cannapet-customization'),
            380 => __('Secondary amount cannot exceed the sale amount','cannapet-customization'),
            381 => __('This method of payment does not support secondary amount','cannapet-customization'),
            382 => __('Secondary amount cannot be less than zero','cannapet-customization'),
            383 => __('Partial transaction is not supported when including a secondary amount','cannapet-customization'),
            384 => __('Secondary amount required on partial refund when used on deposit','cannapet-customization'),
            385 => __('Secondary amount not allowed on refund if not included on deposit','cannapet-customization'),
            401 => __('Invalid E-mail','cannapet-customization'),
            469 => __('Invalid Recurring Request - See Recurring Response for Details','cannapet-customization'),
            470 => __('Approved - Recurring Subscription Created','cannapet-customization'),
            471 => __('Parent Transaction Declined - Recurring Subscription Not Created','cannapet-customization'),
            472 => __('Invalid Plan Code','cannapet-customization'),
            473 => __('Scheduled Recurring Payment Processed','cannapet-customization'),
            475 => __('Invalid Subscription Id','cannapet-customization'),
            476 => __('Add On Code Already Exists','cannapet-customization'),
            477 => __('Duplicate Add On Codes in Requests','cannapet-customization'),
            478 => __('No Matching Add On Code for the Subscription','cannapet-customization'),
            480 => __('No Matching Discount Code for the Subscription','cannapet-customization'),
            481 => __('Duplicate Discount Codes in Request','cannapet-customization'),
            482 => __('Invalid Start Date','cannapet-customization'),
            483 => __('Merchant Not Registered for Recurring Engine','cannapet-customization'),
            500 => __('The account number was changed','cannapet-customization'),
            501 => __('The account was closed','cannapet-customization'),
            502 => __('The expiration date was changed','cannapet-customization'),
            503 => __('The issuing bank does not participate in the update program','cannapet-customization'),
            504 => __('Contact the cardholder for updated information','cannapet-customization'),
            505 => __('No match found','cannapet-customization'),
            506 => __('No changes found','cannapet-customization'),
            521 => __('Soft Decline - Card reader decryption service is not available','cannapet-customization'),
            523 => __('Soft Decline - Decryption failed','cannapet-customization'),
            524 => __('Hard Decline - Input data is invalid.','cannapet-customization'),
            530 => __('Apple Pay Key Mismatch','cannapet-customization'),
            531 => __('Apple Pay Decryption Failed','cannapet-customization'),
            550 => __('Restricted Device or IP - ThreatMetrix Fraud Score Below Threshold','cannapet-customization'),
            601 => __('Soft Decline - Primary Funding Source Failed','cannapet-customization'),
            602 => __('Soft Decline - Buyer has alternate funding source','cannapet-customization'),
            610 => __('Hard Decline - Invalid Billing Agreement Id','cannapet-customization'),
            611 => __('Hard Decline - Primary Funding Source Failed','cannapet-customization'),
            612 => __('Hard Decline - Issue with Paypal Account','cannapet-customization'),
            613 => __('Hard Decline - PayPal authorization ID missing','cannapet-customization'),
            614 => __('Hard Decline - confirmed email address is not available','cannapet-customization'),
            615 => __('Hard Decline - PayPal buyer account denied','cannapet-customization'),
            616 => __('Hard Decline - PayPal buyer account restricted','cannapet-customization'),
            617 => __('Hard Decline - PayPal order hasbeen voided, expired, or completed','cannapet-customization'),
            618 => __('Hard Decline - issue with PayPal refund','cannapet-customization'),
            619 => __('Hard Decline - PayPal credentials issue','cannapet-customization'),
            620 => __('Hard Decline - PayPal authorization voided or expired','cannapet-customization'),
            621 => __('Hard Decline - required PayPal parameter missing','cannapet-customization'),
            622 => __('Hard Decline - PayPal transaction ID or auth ID is invalid','cannapet-customization'),
            623 => __('Hard Decline - Exceeded maximum number of PayPal authorization attempts','cannapet-customization'),
            624 => __('Hard Decline - Transaction amount exceeds merchant’s PayPal account limit.','cannapet-customization'),
            625 => __('Hard Decline - PayPal funding sources unavailable.','cannapet-customization'),
            626 => __('Hard Decline - issue with PayPal primary funding source.','cannapet-customization'),
            627 => __('Hard Decline - PayPal profile does not allow this transaction type.','cannapet-customization'),
            628 => __('Internal System Error with PayPal - Contact Litle','cannapet-customization'),
            629 => __('Hard Decline - Contact PayPal consumer for another payment method','cannapet-customization'),
            637 => __('Invalid terminal Id','cannapet-customization'),
            701 => __('Under 18 years old','cannapet-customization'),
            702 => __('Bill to outside USA','cannapet-customization'),
            703 => __('Bill to address is not equal to ship to address','cannapet-customization'),
            704 => __('Declined, foreign currency, must be USD','cannapet-customization'),
            705 => __('On negative file','cannapet-customization'),
            706 => __('Blocked agreement','cannapet-customization'),
            707 => __('Insufficient buying power','cannapet-customization'),
            708 => __('Invalid Data','cannapet-customization'),
            709 => __('Invalid Data - data elements missing','cannapet-customization'),
            710 => __('Invalid Data - data format error','cannapet-customization'),
            711 => __('Invalid Data - Invalid T&C version','cannapet-customization'),
            712 => __('Duplicate transaction','cannapet-customization'),
            713 => __('Verify billing address','cannapet-customization'),
            714 => __('Inactive Account','cannapet-customization'),
            716 => __('Invalid Auth','cannapet-customization'),
            717 => __('Authorization already exists for the order','cannapet-customization'),
            801 => __('Account number was successfully registered','cannapet-customization'),
            802 => __('Account number was previously registered','cannapet-customization'),
            805 => __('Card Validation Number Updated','cannapet-customization'),
            820 => __('Credit card number was invalid','cannapet-customization'),
            821 => __('Merchant is not authorized for tokens','cannapet-customization'),
            822 => __('Token was not found','cannapet-customization'),
            823 => __('Token Invalid','cannapet-customization'),
            835 => __('Capture amount can not be more than authorized amount','cannapet-customization'),
            850 => __('Tax Billing only allowed for MCC 9311','cannapet-customization'),
            851 => __('MCC 9311 requires taxType element','cannapet-customization'),
            852 => __('Debt Repayment only allowed for VI transactions on MCCs 6012 and 6051','cannapet-customization'),
            877 => __('Invalid Pay Page Registration Id','cannapet-customization'),
            878 => __('Expired Pay Page Registration Id','cannapet-customization'),
            879 => __('Merchant is not authorized for Pay Page','cannapet-customization'),
            898 => __('Generic token registration error','cannapet-customization'),
            899 => __('Generic token use error','cannapet-customization'),
            900 => __('Invalid Bank Routing Number','cannapet-customization'),
            950 => __('Decline - Negative Information on File','cannapet-customization'),
            951 => __('Absolute Decline','cannapet-customization'),
            952 => __('The Merchant Profile does not allow the requested operation','cannapet-customization'),
            953 => __('The account cannot accept ACH transactions','cannapet-customization'),
            954 => __('The account cannot accept ACH transactions or site drafts','cannapet-customization'),
            955 => __('Amount greater than limit specified in the Merchant Profile','cannapet-customization'),
            956 => __('Merchant is not authorized to perform eCheck Verification','cannapet-customization'),
            957 => __('First Name and Last Name required for eCheck Verifications transactions','cannapet-customization'),
            958 => __('Company Name required for corporate account for eCheck Verifications','cannapet-customization'),
            959 => __('Phone number required for eCheck Verifications','cannapet-customization'),
            961 => __('Card Brand token not supported','cannapet-customization'),
            962 => __('Private Label Card not supported','cannapet-customization')
        );
        $returnMessage = isset($responseCodesHumanReadable[$responseCode]) ? $responseCodesHumanReadable[$responseCode] : '';
        return $returnMessage;
    }

}
