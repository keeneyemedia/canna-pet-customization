<?php

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Canna_Pet_Customization
 * @subpackage Canna_Pet_Customization/includes
 * @author     Angell EYE <service@angelleye.com>
 */
class Canna_Pet_Customization_Activator {

    /**
     * @since    1.0.0
     */
    public static function activate() {
        /* set_transient added for endpoints. */
        set_transient( 'cp_flush', 1, 60 );
    }

}
