<?php

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Canna_Pet_Customization
 * @subpackage Canna_Pet_Customization/includes
 * @author     Angell EYE <service@angelleye.com>
 */
class Canna_Pet_Customization_i18n {

    /**
     * Load the plugin text domain for translation.
     *
     * @since    1.0.0
     */
    public function load_plugin_textdomain() {

        load_plugin_textdomain(
                'canna-pet-customization', false, dirname(dirname(plugin_basename(__FILE__))) . '/languages/'
        );
    }

}
