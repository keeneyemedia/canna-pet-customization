<?php
class WC_Gateway_Petconscious_Payflow_Pro extends WC_Payment_Gateway_CC {

    public $domain;

    /**
     * Constructor for the gateway.
     */
        public function __construct() {

        $this->domain = 'petconscious_paypal_payflow_pro';

        $this->id                 = 'petconscious_paypal_payflow';
        $this->icon               = apply_filters('woocommerce_petconscious_checkout_gateway_icon', '');
        $this->has_fields         = true;
        $this->method_title       = __('PetConscious PayPal Pro (PayFlow)', $this->domain);
        $this->method_description = __('Allows payments with PayPal Payments (PayFlow) gateway.', $this->domain);

        // Load the settings.
        $this->init_form_fields();
        $this->init_settings();

        // Define user set variables
        $this->title        = $this->get_option('title');
        $this->description  = $this->get_option('description');
        $this->instructions = $this->get_option('instructions', $this->description);
        $this->order_status = $this->get_option('order_status', 'completed');

        $this->supports = array(
            'products',
            'refunds',
            'tokenization',
        );
        // Actions
        add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
        add_action('woocommerce_thankyou_' . $this->id, array($this, 'thankyou_page'));
        add_action('woocommerce_credit_card_form_fields', array($this, 'petconscious_paypal_payflow_form_fields'), 10, 2);

        // Customer Emails
        add_action('woocommerce_email_before_order_table', array($this, 'email_instructions'), 10, 3);
    }
    /**
     * Initialise Gateway Settings Form Fields.
     */
        public function init_form_fields() {

        $this->form_fields = array(
            'enabled' => array(
                'title'   => __('Enable/Disable', $this->domain),
                'type'    => 'checkbox',
                'label'   => __('Enable PayPal Payments (PayFlow)', $this->domain),
                'default' => 'yes'
            ),
            'title' => array(
                'title'       => __('Title', $this->domain),
                'type'        => 'text',
                'description' => __('This controls the title which the user sees during checkout.', $this->domain),
                'default'     => __('PayPal Pro Payflow Edition', $this->domain),
                'desc_tip'    => true,
            ),
            'order_status' => array(
                'title'       => __('Order Status', $this->domain),
                'type'        => 'select',
                'class'       => 'wc-enhanced-select',
                'description' => __('Choose whether status you wish after checkout.', $this->domain),
                'default'     => 'wc-completed',
                'desc_tip'    => true,
                'options'     => wc_get_order_statuses()
            ),
            'description' => array(
                'title'       => __('Description', $this->domain),
                'type'        => 'textarea',
                'description' => __('Payment method description that the customer will see on your checkout.', $this->domain),
                'default'     => __('Payment Information', $this->domain),
                'desc_tip'    => true,
            ),
            'instructions' => array(
                'title'       => __('Instructions', $this->domain),
                'type'        => 'textarea',
                'description' => __('Instructions that will be added to the thank you page and emails.', $this->domain),
                'default'     => '',
                'desc_tip'    => true,
            ),
            'iframe_payflow' => array(
                'title'       => __('Third Party Hosted URL', $this->domain),
                'type'        => 'text',
                'description' => __('Add Third Party Hosted URL for PayPal Pro (Payflow)', $this->domain),
                'default'     => '',
                'desc_tip'    => true,
            )
        );
    }

        public function add_log($message, $level = 'info') {
        if (version_compare(WC_VERSION, '3.0', '<')) {
            if (empty($this->log)) {
                $this->log = new WC_Logger();
            }
            $this->log->add('petconscious_paypal_payflow_pro', $message);
        } else {
            if (empty($this->log)) {
                $this->log = wc_get_logger();
            }
            $this->log->log($level, $message, array('source' => 'petconscious_paypal_payflow_pro'));
        }
    }

    public function add_payment_method()
    {

        $card_number = isset($_POST['petconscious_paypal_payflow-card-number']) ? wc_clean($_POST['petconscious_paypal_payflow-card-number']) : '';
        $expiry_month = isset($_POST['pc_paypal_pro_payflow_card_expiration_month']) ? wc_clean($_POST['pc_paypal_pro_payflow_card_expiration_month']) : '';
        $expiry_year = isset($_POST['pc_paypal_pro_payflow_card_expiration_year']) ? wc_clean($_POST['pc_paypal_pro_payflow_card_expiration_year']) : '';
        $card_cvc = isset($_POST['petconscious_paypal_payflow-card-cvc']) ? wc_clean($_POST['petconscious_paypal_payflow-card-cvc']) : '';

        //$expiry_araay = explode('/', str_replace(' ','',$card_expiry));

        $card_number = str_replace(array(' ', '-'), '', $card_number);
        //$expiry_month = $expiry_araay[0];
        //$expiry_year = $expiry_araay[1];

        /*if(strlen($expiry_year) == 4){
                $expiry_year = (string) $expiry_year - 2000;
            }*/

        // Check card security code

        if (!ctype_digit($card_cvc)) {
            wc_add_notice(__('Card security code is invalid (only digits are allowed)', $this->domain), "error");
            return false;
        }

        // Check card expiration data

        if (!ctype_digit($expiry_month) || !ctype_digit($expiry_year) || $expiry_month > 12 || $expiry_month < 1 || $expiry_year < date('y') || $expiry_year > date('y') + 20) {
            wc_add_notice(__('Card expiration date is invalid', $this->domain), "error");
            return false;
        }

        // Check card number

        if (empty($card_number) || !ctype_digit($card_number)) {
            wc_add_notice(__('Card number is invalid', $this->domain), "error");
            return false;
        }

        $customer_id = get_current_user_id();
        $billtofirstname = (get_user_meta($customer_id, 'billing_first_name', true)) ? get_user_meta($customer_id, 'billing_first_name', true) : get_user_meta($customer_id, 'shipping_first_name', true);
        $billtolastname = (get_user_meta($customer_id, 'billing_last_name', true)) ? get_user_meta($customer_id, 'billing_last_name', true) : get_user_meta($customer_id, 'shipping_last_name', true);
        $billtostate = (get_user_meta($customer_id, 'billing_state', true)) ? get_user_meta($customer_id, 'billing_state', true) : get_user_meta($customer_id, 'shipping_state', true);
        $billtocountry = (get_user_meta($customer_id, 'billing_country', true)) ? get_user_meta($customer_id, 'billing_country', true) : get_user_meta($customer_id, 'shipping_country', true);
        $billtozip = (get_user_meta($customer_id, 'billing_postcode', true)) ? get_user_meta($customer_id, 'billing_postcode', true) : get_user_meta($customer_id, 'shipping_postcode', true);

        $PayPalRequestData = array(
            'tender' => 'C',
            'trxtype' => 'A',
            'acct' => $card_number,
            'expdate' => $expiry_month . $expiry_year,
            'amt' => '0.00',
            'currency' => get_woocommerce_currency(),
            'cvv2' => $card_cvc,
            'billtofirstname' => $billtofirstname,
            'billtomiddlename' => '',
            'billtolastname' => $billtolastname,
            'billtostreet' => '',
            'billtocity' => '',
            'billtostate' => $billtostate,
            'billtozip' => $billtozip,
            'billtocountry' => $billtocountry,
        );
        $payflow_third_party_url = $this->get_option('iframe_payflow');

            $response = wp_remote_post($payflow_third_party_url.'add_card.php', array(
                'method' => 'POST',
                'timeout' => 45,
                'redirection' => 5,
                'httpversion' => '1.0',
                'blocking' => true,
                'headers' => array(),
                'body' => $PayPalRequestData,
                'cookies' => array()
            )
        );

        if (is_wp_error($response)) {
            wc_add_notice(
                __('Error: ', $this->domain) . $response->get_error_message(),
                'error'
            );
            return;
            }
            else{
            $response_body = wp_remote_retrieve_body($response);
            $response_object = json_decode($response_body);
            $this->add_log(sprintf(__('Canna-Pet Customization Version: %s', $this->domain), CANNA_PET_CUSTOMIZATION_VERSION));
            $this->add_log(sprintf(__('WooCommerce Version: %s', $this->domain), WC_VERSION));
            $this->add_log('Petconsious Payflow Request Add Payment Method');
            $this->add_log('PayFlow Request: ' . print_r($PayPalRequestData, true));
            $this->add_log('PayFlow Response: ' . print_r($response_object, true));
            if ($response_object->success == 'true') {
                $pnref = $response_object->data->saved_card_pnref;
                // Create token instance (WC_Token_CC), save with customer id using method of token
                $token = new WC_Payment_Token_CC();
                $token->set_user_id($customer_id);
                $token->set_token($pnref);
                $token->set_gateway_id('petconscious_paypal_payflow');
                $token->set_card_type($this->cp_card_type_from_account_number($card_number));
                $token->set_last4(substr($card_number, -4));
                $token->set_expiry_month($expiry_month);
                if (strlen($expiry_year) == 2) {
                    $expiry_year = $expiry_year + 2000;
                }
                $token->set_expiry_year($expiry_year);
                if ($token->validate()) {
                    $save_result = $token->save();
                    if ($save_result) {
                        return array(
                            'result' => 'success',
                            'redirect' => wc_get_account_endpoint_url('payment-methods')
                        );
                    }
                } else {
                    throw new Exception(__('Invalid or missing payment token fields.', 'paypal-for-woocommerce'));
                }
                }
                else if($response_object->success == 'fraud_flag'){
                wc_add_notice(__('The payment was flagged by a fraud filter, please check your PayPal Manager account to review and accept or deny the payment.', 'paypal-for-woocommerce'), 'error');
                wp_redirect(wc_get_account_endpoint_url('payment-methods'));
                exit();
                }
                else{                    
                wc_add_notice($response_object->data->respmsg, 'error');
                wp_redirect(wc_get_account_endpoint_url('payment-methods'));
                exit();
            }
        }
    }

        public function get_icon() {
            $image_path = plugins_url('public/assets/images/credit-card-logos.png', plugin_basename(dirname(__FILE__)));
             $icon = "<img src=\"$image_path\" alt='" . __('Pay for order', $this->domain) . "'/>";
        return apply_filters('angelleye_ec_checkout_icon', $icon, $this->id);
    }

    /**
     * Output for the order received page.
     */
        public function thankyou_page() {
        if ($this->instructions)
            echo wpautop(wptexturize($this->instructions));
    }

    /**
     * Add content to the WC emails.
     *
     * @access public
     * @param WC_Order $order
     * @param bool $sent_to_admin
     * @param bool $plain_text 
     */
        public function email_instructions($order, $sent_to_admin, $plain_text = false) {
        if ($this->instructions && !$sent_to_admin && 'petconscious_paypal_payflow' === $order->get_payment_method()) {
            echo wpautop(wptexturize($this->instructions)) . PHP_EOL;
        }
    }

        public function payment_fields(){
        if ($description = $this->get_description()) {
            echo wpautop(wptexturize($description));
        }
        if ($this->supports('tokenization') && is_checkout()) {
            $this->tokenization_script();
            $this->saved_payment_methods();
            $this->form();
            }
            else{
            $this->form();
        }
        do_action('payment_fields_saved_payment_methods', $this);
    }

        public function field_name($name) {
        return ' name="' . esc_attr($this->id . '-' . $name) . '" ';
    }

    public function petconscious_paypal_payflow_form_fields($default_fields, $current_gateway_id)
    {
        if ($current_gateway_id == $this->id) {
            $fields = array(
                'card-number-field' => '<p class="form-row form-row-wide">
                                <label for="' . esc_attr($this->id) . '-card-number">' . apply_filters('cc_form_label_card_number', __('Card number', 'paypal-for-woocommerce'), $this->id) . ' <span class="required">*</span></label>
                                <input id="' . esc_attr($this->id) . '-card-number" class="input-text wc-credit-card-form-card-number form-control" inputmode="numeric" autocomplete="cc-number" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" placeholder="&bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull;" ' . $this->field_name('card-number') . ' />
                            </p>',
                'card-expiry-field' => $this->paypal_pro_payflow_credit_card_form_expiration_date_selectbox(),
                '<p class="form-row form-row-last">
                                <label for="' . esc_attr($this->id) . '-card-cvc">' . apply_filters('cc_form_label_card_code', __('Card Security Code', 'paypal-for-woocommerce'), $this->id) . ' <span class="required">*</span></label>
                                <input id="' . esc_attr($this->id) . '-card-cvc" class="input-text wc-credit-card-form-card-cvc form-control" inputmode="numeric" autocomplete="off" autocorrect="no" autocapitalize="no" spellcheck="no" type="password" maxlength="4" placeholder="' . esc_attr__('CVC', 'paypal-for-woocommerce') . '" ' . $this->field_name('card-cvc') . ' style="width:100px" />
                            </p>'
            );
            return $fields;
        } else {
            return $default_fields;
        }
    }

    public function paypal_pro_payflow_credit_card_form_expiration_date_selectbox()
    {
        $form_html = "";
        $form_html .= '<p class="form-row form-row-first">';
        $form_html .= '<label for="cc-expire-month">' . __("Expiration Date") . '<span class="required">*</span></label>';
        $form_html .= '<select name="pc_paypal_pro_payflow_card_expiration_month" id="cc-expire-month" class="woocommerce-select woocommerce-cc-month form-control" style="float: left;margin-right: 10px;border-color: #d3ced3;border-top-color: #c7c0c7;">';
        $form_html .= '<option value="">' . __('Month', 'paypal-for-woocommerce') . '</option>';
        $months = array();
        for ($i = 1; $i <= 12; $i++) :
            $timestamp = mktime(0, 0, 0, $i, 1);
            $months[date('n', $timestamp)] = date_i18n(_x('F', 'Month Names', 'paypal-for-woocommerce'), $timestamp);
        endfor;

        foreach ($months as $num => $name) {
            $month_value = ($num < 10) ? '0' . $num : $num;
            $form_html .= '<option value=' . $month_value . '>' . $name . '</option>';
        }
        $form_html .= '</select>';
        $form_html .= '<select name="pc_paypal_pro_payflow_card_expiration_year" id="cc-expire-year" class="woocommerce-select woocommerce-cc-year form-control" style="border-color: #d3ced3;border-top-color: #c7c0c7;">';
        $form_html .= '<option value="">' . __('Year', 'paypal-for-woocommerce') . '</option>';
        for ($i = date('y'); $i <= date('y') + 15; $i++) {
            $form_html .= '<option value=' . $i . '>20' . $i . '</option>';
        }
        $form_html .= '</select>';
        $form_html .= '</p>';
        return $form_html;
    }

    /**
     * Process the payment and return the result.
     *
     * @param int $order_id
     * @return array
     */
    public function process_payment($order_id)
    {

        $order = wc_get_order($order_id);
        if (empty($_POST['wc-petconscious_paypal_payflow-payment-token']) || $_POST['wc-petconscious_paypal_payflow-payment-token'] == 'new') {

            $card_number = isset($_POST['petconscious_paypal_payflow-card-number']) ? wc_clean($_POST['petconscious_paypal_payflow-card-number']) : '';
            $expiry_month = isset($_POST['pc_paypal_pro_payflow_card_expiration_month']) ? wc_clean($_POST['pc_paypal_pro_payflow_card_expiration_month']) : '';
            $expiry_year = isset($_POST['pc_paypal_pro_payflow_card_expiration_year']) ? wc_clean($_POST['pc_paypal_pro_payflow_card_expiration_year']) : '';
            $card_cvc = isset($_POST['petconscious_paypal_payflow-card-cvc']) ? wc_clean($_POST['petconscious_paypal_payflow-card-cvc']) : '';

            //$expiry_araay = explode('/', str_replace(' ','',$card_expiry));

            $card_number = str_replace(array(' ', '-'), '', $card_number);
            //$expiry_month = $expiry_araay[0];
            //$expiry_year = $expiry_araay[1];

            // if(strlen($expiry_year) == 4){
            //     $expiry_year = $expiry_year - 2000;
            //     $expiry_year = (string) $expiry_year;
            // }

            // Check card security code

            if (!ctype_digit($card_cvc)) {
                wc_add_notice(__('Card security code is invalid (only digits are allowed)', $this->domain), "error");
                return false;
            }

            // Check card expiration data

            if (!ctype_digit($expiry_month) || !ctype_digit($expiry_year) || $expiry_month > 12 || $expiry_month < 1 || $expiry_year < date('y') || $expiry_year > date('y') + 20) {
                wc_add_notice(__('Card expiration date is invalid', $this->domain), "error");
                return false;
            }


            // Check card number

            if (empty($card_number) || !ctype_digit($card_number)) {
                wc_add_notice(__('Card number is invalid', $this->domain), "error");
                return false;
            }


            $payflow_third_party_url = $this->get_option('iframe_payflow');

            $array_with_parameters = array(
                'order_number' => $order_id,
                'amount' => $order->get_total(),
                'card_number' => $card_number,
                'card_expiry' => $expiry_month . $expiry_year,
                'card_cvc' => $card_cvc
            );
            if (self::cart_has_autoship_item()) {
                $array_with_parameters['auto_ship_items'] = 'yes';
            }

                $response = wp_remote_post($payflow_third_party_url, array(
                    'method' => 'POST',
                    'timeout' => 45,
                    'redirection' => 5,
                    'httpversion' => '1.0',
                    'blocking' => true,
                    'headers' => array(),
                    'body' => $array_with_parameters,
                    'cookies' => array()
                )
            );
            if (is_wp_error($response)) {
                wc_add_notice(
                    __('Error: ', $this->domain) . $response->get_error_message(),
                    'error'
                );
                return;
                }
                else{
                $response_body = wp_remote_retrieve_body($response);
                $response_object = json_decode($response_body);
                $this->add_log(sprintf(__('Canna-Pet Customization Version: %s', $this->domain), CANNA_PET_CUSTOMIZATION_VERSION));
                $this->add_log(sprintf(__('WooCommerce Version: %s', $this->domain), WC_VERSION));
                $this->add_log('Petconsious Payflow Request Order ID: ' . $order_id);
                $this->add_log('PayFlow Request: ' . print_r($response_object->data->paypal_request_data, true));
                $this->add_log('PayFlow Response: ' . print_r($response_object->data->paypal_response_data, true));
                if ($response_object->success == 'true') {

                    if (isset($response_object->data->txn_id) && !empty($response_object->data->txn_id)) {
                        $order->add_order_note(sprintf(__('PayPal Payments (PayFlow) completed (PNREF: %s) (PPREF: %s)', $this->domain), $response_object->data->pnref, $response_object->data->txn_id));
                        $txn_id = $response_object->data->txn_id;
                        update_post_meta($order->get_order_number(), '_first_transaction_id', $txn_id);
                    } else {
                        $order->add_order_note(sprintf(__('PayPal Payments (PayFlow) completed (PNREF: %s)', $this->domain), $response_object->data->pnref));
                    }

                    //if (isset($response_object->data->auto_ship_items) && !empty($response_object->data->auto_ship_items) && $response_object->data->auto_ship_items == 'yes') {
                        // Store billing agreement data
                        $customer_id = $order->get_user_id();
                        $pnref = $response_object->data->pnref;
                        // Create token instance (WC_Token_CC), save with customer id using method of token
                        $token = new WC_Payment_Token_CC();
                        $token->set_user_id($customer_id);
                        $token->set_token($pnref);
                        $token->set_gateway_id('petconscious_paypal_payflow');
                        $token->set_card_type($this->cp_card_type_from_account_number($card_number));
                        $token->set_last4(substr($card_number, -4));
                        $token->set_expiry_month($expiry_month);
                        if (strlen($expiry_year) == 2) {
                            $expiry_year = $expiry_year + 2000;
                        }
                        $token->set_expiry_year($expiry_year);
                        $save_result = $token->save();
                        if ($save_result) {
                            $order->add_payment_token($token);
                        }
                    //}
                    $order->payment_complete($response_object->data->pnref);
                    if (isset($response_object->data->fraud_flag) && !empty($response_object->data->fraud_flag) && $response_object->data->fraud_flag == 'true') {
                        $order->add_order_note("The payment was flagged by a fraud filter, please check your PayPal Manager account to review and accept or deny the payment.");
                        $status = 'wc-on-hold';
                        $order->update_status($status, __('Checkout with PayPal Payments (PayFlow).', 'canna-pet-customization'));
                    }
                    return array(
                        'result'    => 'success',
                        'redirect'  => $this->get_return_url($order)
                    );
                    }
                    else{                        
                    if (isset($response_object->data->respmsg)) {
                        wc_add_notice(
                            __('Error: ', $this->domain) . $response_object->data->resultcode . ' ' . $response_object->data->respmsg,
                            'error'
                        );
                        return;
                        }
                        else{
                        wc_add_notice(
                            __('Error: Something went wrong during checkout process.', $this->domain),
                            'error'
                        );
                    }
                    return;
                }
            }
            }
            else{
            // Do referance transaction.
            $payment_token_id = $_POST['wc-petconscious_paypal_payflow-payment-token'];
            $payment_token = WC_Payment_Tokens::get($payment_token_id);
            $origin_id = $payment_token->get_token();

            $array_with_parameters = array(
                'origin_id' => $origin_id,
                'amt' => $order->get_total(),
                'inv_num' => $order_id
            );

            $url = $this->get_option('iframe_payflow');
            $ref_txn_url = $url . '/DoReferenceTransaction.php';

                $response = wp_remote_post($ref_txn_url, array(
                    'method' => 'POST',
                    'timeout' => 45,
                    'redirection' => 5,
                    'httpversion' => '1.0',
                    'blocking' => true,
                    'headers' => array(),
                    'body' => $array_with_parameters,
                    'cookies' => array()
                )
            );
            if (is_wp_error($response)) {
                wc_add_notice(
                    __('Error: ', 'wc-autoship') . $response->get_error_message(),
                    'error'
                );
                return;
                }
                else{
                $response_body = wp_remote_retrieve_body($response);
                $response_object = json_decode($response_body);

                $this->add_log(sprintf(__('Canna-Pet Customization Version: %s', $this->domain), CANNA_PET_CUSTOMIZATION_VERSION));
                $this->add_log(sprintf(__('WooCommerce Version: %s', $this->domain), WC_VERSION));
                $this->add_log('Petconsious Payflow Request Order ID: ' . $order_id);
                $this->add_log('PayFlow Request: ' . print_r($response_object->data->paypal_request_data, true));
                $this->add_log('PayFlow Response: ' . print_r($response_object->data->paypal_response_data, true));
                if ($response_object->success == 'true') {
                    $order->payment_complete($response_object->data->pnref);
                    $order->add_payment_token($payment_token);
                    $order->add_order_note(sprintf(__('TransactionID for the  (Order: %1$s) and is %2$s', 'canna-pet-customization'), $order->get_order_number(), $response_object->data->pnref));
                    return array(
                        'result' => 'success',
                        'redirect' => $this->get_return_url($order)
                    );
                    }
                    else if($response_object->success == 'fraud_flag'){
                    update_post_meta($order->get_order_number(), '_transaction_id', $response_object->data->pnref);
                    $order->add_order_note("The payment was flagged by a fraud filter, please check your PayPal Manager account to review and accept or deny the payment.");
                    $status = 'wc-on-hold';
                    $order->update_status($status, __('Checkout with PayPal Payments (PayFlow).', 'canna-pet-customization'));
                    wc_reduce_stock_levels($order->get_id());
                    $items = $woocommerce->cart->get_cart();
                    if (!empty($items)) {
                        $woocommerce->cart->empty_cart();
                    }
                    return array(
                        'result' => 'success',
                        'redirect' => $this->get_return_url($order)
                    );
                    }
                    else{                    
                    wc_add_notice(
                        __('Error: ', 'wc-autoship') . $response_object->respmsg,
                        'error'
                    );
                    return;
                }
            }
        }
    }

        public function cart_has_autoship_item(){
        if (!function_exists('WC')) {
            return false;
        }
        $cart = WC()->cart;
        if (empty($cart)) {
            return false;
        }
        $has_autoship_items = false;
        foreach ($cart->get_cart() as $item) {
            if (isset($item['wc_autoship_frequency'])) {
                $has_autoship_items = true;
                break;
            }
        }
        return $has_autoship_items;
    }

    /**
     * Process a refund if supported
     * @param  int $order_id
     * @param  float $amount
     * @param  string $reason
     * @return  bool|wp_error True or false based on success, or a WP_Error object
     */
        public function process_refund($order_id, $amount = null, $reason = '') {
        $order = wc_get_order($order_id);
        if (!$order || !$order->get_transaction_id()) {
            return false;
        }
            $array_with_parameters = array('origid' => $order->get_transaction_id(),
                                            'amount' => $amount);
        $url =  $this->get_option('iframe_payflow');
        $refund_url = $url . '/refund.php';
            $response = wp_remote_post( $refund_url, array(
                'method' => 'POST',
                'timeout' => 45,
                'redirection' => 5,
                'httpversion' => '1.0',
                'blocking' => true,
                'headers' => array(),
                'body' => $array_with_parameters,
                'cookies' => array()
            )
        );

        if (is_wp_error($response)) {
            return new WP_Error('response-error', $response->get_error_message());
        } else {
            $response_body = wp_remote_retrieve_body($response);
            $response_object = json_decode($response_body);
            if ($response_object->success == 'true') {
                update_post_meta($order_id, 'Refund Transaction ID', $response_object->newPNREF);
                $order->add_order_note('Refund Transaction ID:' . $response_object->newPNREF);
                $max_remaining_refund = wc_format_decimal($order->get_total() - $order->get_total_refunded());
                if (!$max_remaining_refund > 0) {
                    $order->update_status('refunded');
                }
                return true;
                }
                else{
                $fc_refund_error = $response_object->RESPMSG;
                return new WP_Error('payflow-refund-error', $fc_refund_error);
            }
            return false;
        }
    }

        public function cp_card_type_from_account_number($account_number) {
        $types = array(
            'visa' => '/^4[0-9]{0,15}$/i',
            'mastercard' => '/^(5[0-5]|(222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720))/',
            'amex' => '/^3[47]/',
            'discover' => '/^6([045]|22)/',
            'diners' => '/^3(?:0[0-5]|[68][0-9])[0-9]{4,}$/i',
            'jcb' => '/^35/',
            'maestro' => '/^(5(018|0[23]|[68])|6(39|7))/',
            'laser' => '/^(6706|6771|6709)/',
        );
        foreach ($types as $type => $pattern) {
            if (1 === preg_match($pattern, $account_number)) {
                return $type;
            }
        }
        return 'Card';
    }


    }