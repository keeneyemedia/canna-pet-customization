<?php
class WC_Gateway_Petconscious_Checkout extends WC_Payment_Gateway_CC {

    public $domain;

    /**
     * Constructor for the gateway.
     */
    public function __construct() {

        $this->domain = 'petconscious_checkout_payment';

        $this->id = 'petconscious_checkout';
        $this->icon = apply_filters('woocommerce_petconscious_checkout_gateway_icon', '');
        $this->has_fields = false;
        $this->method_title = __('PetConscious PayPal Express Checkout', $this->domain);
        $this->method_description = __('Allows payments with PayPal Express Checkout gateway.', $this->domain);

        // Load the settings.
        $this->init_form_fields();
        $this->init_settings();

        // Define user set variables
        $this->title = $this->get_option('title');
        $this->description = $this->get_option('description');
        $this->instructions = $this->get_option('instructions', $this->description);
        $this->order_status = $this->get_option('order_status', 'completed');

        $this->supports = array(
            'products',
            'refunds',
            'tokenization'
        );

        // Actions
        add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));        
        add_action('woocommerce_thankyou_' . $this->id, array($this, 'thankyou_page'));

        // Customer Emails
        add_action('woocommerce_email_before_order_table', array($this, 'email_instructions'), 10, 3);
        
        if (!has_action('woocommerce_api_' . strtolower('WC_Gateway_Petconscious_Checkout'))) {
           add_action('woocommerce_api_' . strtolower('WC_Gateway_Petconscious_Checkout'), array($this, 'handle_wc_api'));
        }
        
    }

    
    public function handle_wc_api() {
        
        if(isset($_GET['token'])){
            $token = $_GET['token'];
            $PayerID = isset($_GET['PayerID']) ? $_GET['PayerID'] : '';
            $order = wc_get_order($_GET['order_number']);
            
            $dec_array_with_parameters = array(
                'amount' => $order->get_total(),
                'order_number' => $order->get_id(),
                'token' => $token,
                'PayerID' => $PayerID
            );
            $url = $this->get_option('iframe_checkout');
            $dec_url = $url . '/DoExpressCheckoutPayment.php';
            $response = wp_remote_post($dec_url, array(
                    'method' => 'POST',
                    'timeout' => 45,
                    'redirection' => 5,
                    'httpversion' => '1.0',
                    'blocking' => true,
                    'headers' => array(),
                    'body' => $dec_array_with_parameters,
                    'cookies' => array()
                )
            );
            if (is_wp_error($response)) {
                wc_add_notice(
                        __( 'Error: ', $this->domain ) . $response->get_error_message(),
                        'error'
                );
                return;
            }
            else{
                $response_body = wp_remote_retrieve_body($response);
                $response_object = json_decode($response_body);
                if($response_object->success == 'true'){
                    $txn_id = $response_object->TRANSACTIONID;
                    if (isset($response_object->BILLINGAGREEMENTID) && !empty($response_object->BILLINGAGREEMENTID)) {
                        // Store billing agreement data
                        $customer_id = $order->get_user_id();
                        $billing_agreement_id = $response_object->BILLINGAGREEMENTID;
                        // Create token instance (WC_Token_CC), save with customer id using method of token
                        $token = new WC_Payment_Token_CC();
                        $token->set_user_id($customer_id);
                        $token->set_token($billing_agreement_id);
                        $token->set_gateway_id('petconscious_checkout');
                        $token->set_card_type('paypal');
                        $token->set_last4(substr($billing_agreement_id, -4));
                        $token->set_expiry_month(date('m'));
                        $token->set_expiry_year(date('Y', strtotime('+6 years')));
                        $save_result = $token->save();
                        if ($save_result) {
                            $order->add_payment_token($token);
                        }
                        update_post_meta($order->id, 'BILLINGAGREEMENTID', $billing_agreement_id);                        
                    }
                    $order->payment_complete($txn_id);
                    $order->add_order_note(sprintf(__('TransactionID for the  (Order: %1$s) and is %2$s', 'canna-pet-customization'), $order->get_order_number(), $txn_id));
                    $url = $order->get_checkout_order_received_url();
                    wp_redirect($url);
                    exit;
                }
                else{
                    $error_string = $this->DisplayPayPalErrors($response_object->paypal_errors);
                    WC()->session->set('DEC_ERRORS', $error_string);
                    $checkout_url = wc_get_checkout_url();
                    wp_redirect($checkout_url);
                    exit;
                }
            }
        }
        else{
            WC()->session->set('DEC_ERRORS', 'Invalid token or token not found');
            $checkout_url = wc_get_checkout_url();
            wp_redirect($checkout_url);
            exit;
        }
    }

    /**
     * Initialise Gateway Settings Form Fields.
     */
    public function init_form_fields() {

        $this->form_fields = array(
            'enabled' => array(
                'title' => __('Enable/Disable', $this->domain),
                'type' => 'checkbox',
                'label' => __('Enable PayPal Express Checkout Payment', $this->domain),
                'default' => 'yes'
            ),
            'title' => array(
                'title' => __('Title', $this->domain),
                'type' => 'text',
                'description' => __('This controls the title which the user sees during checkout.', $this->domain),
                'default' => __('PayPal Express', $this->domain),
                'desc_tip' => true,
            ),
            'order_status' => array(
                'title' => __('Order Status', $this->domain),
                'type' => 'select',
                'class' => 'wc-enhanced-select',
                'description' => __('Choose whether status you wish after checkout.', $this->domain),
                'default' => 'wc-completed',
                'desc_tip' => true,
                'options' => wc_get_order_statuses()
            ),
            'description' => array(
                'title' => __('Description', $this->domain),
                'type' => 'textarea',
                'description' => __('Payment method description that the customer will see on your checkout.', $this->domain),
                'default' => __('Payment Information', $this->domain),
                'desc_tip' => true,
            ),
            'instructions' => array(
                'title' => __('Instructions', $this->domain),
                'type' => 'textarea',
                'description' => __('Instructions that will be added to the thank you page and emails.', $this->domain),
                'default' => '',
                'desc_tip' => true,
            ),
            'iframe_checkout' => array(
                'title' => __('iFrame Hosted URL', $this->domain),
                'type' => 'text',
                'description' => __('Add Iframe URL for PayPal Express', $this->domain),
                'default' => '',
                'desc_tip' => true,
            )
        );
    }
    
    public function add_payment_method() {
        // what to do here ?
    }

    public function get_icon() {
        $image_path = plugins_url('public/assets/images/credit-card-logos.png', plugin_basename(dirname(__FILE__)));
        $icon = "<img src=\"$image_path\" alt='" . __('Pay with PayPal', $this->domain) . "'/>";
        return apply_filters('angelleye_ec_checkout_icon', $icon, $this->id);
    }

    /**
     * Output for the order received page.
     */
    public function thankyou_page() {
        if ($this->instructions)
            echo wpautop(wptexturize($this->instructions));
    }

    /**
     * Add content to the WC emails.
     *
     * @access public
     * @param WC_Order $order
     * @param bool $sent_to_admin
     * @param bool $plain_text
     */
    public function email_instructions($order, $sent_to_admin, $plain_text = false) {
        if ($this->instructions && !$sent_to_admin && 'petconscious_checkout' === $order->get_payment_method()) {
            echo wpautop(wptexturize($this->instructions)) . PHP_EOL;
        }
    }

    public function payment_fields() {

        if ($description = $this->get_description()) {
            echo wpautop(wptexturize($description));
        }
        if ($this->supports('tokenization') && is_checkout()) {
            $this->tokenization_script();
            $this->saved_payment_methods();
        }
        do_action('payment_fields_saved_payment_methods', $this);
    }

    /**
     * Process the payment and return the result.
     *
     * @param int $order_id
     * @return array
     */
    public function process_payment($order_id) {

        $order = wc_get_order($order_id);
        if (empty($_POST['wc-petconscious_checkout-payment-token']) || $_POST['wc-petconscious_checkout-payment-token'] == 'new') {

            
            $array_with_parameters = array(
                'amount' => $order->get_total(),
                'order_number' => $order_id,
                'return_url' => add_query_arg('order_number',$order_id,WC()->api_request_url( strtolower('WC_Gateway_Petconscious_Checkout') )) ,
                'cancel_url' => wc_get_checkout_url(),
                'auto_ship_items' => 'no'
            );
            if (self::cart_has_autoship_item()) {
                $array_with_parameters['auto_ship_items'] = 'yes';
            }
            
            $url = $this->get_option('iframe_checkout');
            $sec_url = $url . '/index.php';            
            $response = wp_remote_post($sec_url, array(
                    'method' => 'POST',
                    'timeout' => 45,
                    'redirection' => 5,
                    'httpversion' => '1.0',
                    'blocking' => true,
                    'headers' => array(),
                    'body' => $array_with_parameters,
                    'cookies' => array()
                )
            );
            if (is_wp_error($response)) {
                wc_add_notice(
                        __( 'Error: ', $this->domain ) . $response->get_error_message(),
                        'error'
                );
                return;
            }
            else{
                $response_body = wp_remote_retrieve_body($response);
                $response_object = json_decode($response_body);                
                if($response_object->success == 'true'){                    
                    $redirect_url = $response_object->redirect_url;
                    return array(
                        'result' => 'success',
                        'redirect' => $redirect_url
                    );
                }
                else{
                    wc_add_notice(
                            __( $this->DisplayPayPalErrors($response_object->paypal_errors), $this->domain ),
                            'error'
                    );
                    return;
                }
            }
        }
        else {
            
            // Do referance transaction
            $payment_token_id = $_POST['wc-petconscious_checkout-payment-token'];
            $payment_token = WC_Payment_Tokens::get( $payment_token_id );
            $REFERENCEID = $payment_token->get_token();
            
            $array_with_parameters = array(
                'ref_txn_id' => $REFERENCEID,                
                'amt' => $order->get_total(),
                'inv_num' => $order_id
            );
                    
            $url = $this->get_option('iframe_checkout');
            $ref_txn_url = $url . '/DoReferenceTransaction.php';
            
            $response = wp_remote_post($ref_txn_url, array(
                    'method' => 'POST',
                    'timeout' => 45,
                    'redirection' => 5,
                    'httpversion' => '1.0',
                    'blocking' => true,
                    'headers' => array(),
                    'body' => $array_with_parameters,
                    'cookies' => array()
                )
            );
            if (is_wp_error($response)) {
                wc_add_notice(
                        __( 'Error: ', $this->domain ) . $response->get_error_message(),
                        'error'
                );
                return;
            }
            else{
                $response_body = wp_remote_retrieve_body($response);                
                $response_object = json_decode($response_body);
                if ($response_object->success == 'true') {
                    $order->payment_complete( $response_object->transaction_id );
                    $order->add_payment_token( $payment_token );     
                    $order->add_order_note(sprintf(__('TransactionID for the  (Order: %1$s) and is %2$s', 'canna-pet-customization'), $order->get_order_number(),$response_object->transaction_id));
                    return array(
                            'result' => 'success',
                            'redirect' => $this->get_return_url( $order )
                    );
                }
                else{                    
                    wc_add_notice(
                            __( 'Error: ', $this->domain ) . $response_object->RESPMSG,
                            'error'
                    );
                    return;
                }             
            }
        }
    }
    
      public function cart_has_autoship_item() {
        if (!function_exists('WC')) {
            return false;
        }
        $cart = WC()->cart;
        if (empty($cart)) {
            return false;
        }
//        $has_autoship_items = false;
//        foreach ($cart->get_cart() as $item) {
//            if (isset($item['wc_autoship_frequency'])) {
//                $has_autoship_items = true;
//                break;
//            }
//        }
        return $has_autoship_items = true;
    }
    
    function DisplayPayPalErrors($Errors) {
        $error_string = '';
        foreach ($Errors as $ErrorVar => $ErrorVal) {
            $CurrentError = $Errors[$ErrorVar];
            foreach ($CurrentError as $CurrentErrorVar => $CurrentErrorVal) {
                if ($CurrentErrorVar == 'L_ERRORCODE') {
                    $CurrentVarName = 'Error Code';
                } elseif ($CurrentErrorVar == 'L_SHORTMESSAGE') {
                    $CurrentVarName = 'Short Message';
                } elseif ($CurrentErrorVar == 'L_LONGMESSAGE') {
                    $CurrentVarName = 'Long Message';
                } elseif ($CurrentErrorVar == 'L_SEVERITYCODE') {
                    $CurrentVarName = 'Severity Code';
                }

                $error_string .= $CurrentVarName . ': ' . $CurrentErrorVal . '<br />';
            }
            $error_string.= '<br />';
        }
        return $error_string;
    }

    /**
     * Process a refund if supported
     * @param  int $order_id
     * @param  float $amount
     * @param  string $reason
     * @return  bool|wp_error True or false based on success, or a WP_Error object
     */
    public function process_refund($order_id, $amount = null, $reason = '') {
        $order = wc_get_order($order_id);
        if (!$order || !$order->get_transaction_id()) {
            return false;
        }

        $refundtype = $order->get_total() == $amount ? 'Full' : 'Partial';

        $array_with_parameters = array(
            'transactionid' => $order->get_transaction_id(),
            'refundtype' => $refundtype,
            'amt' => $amount,
            'note' => $reason
        );
        $url = $this->get_option('iframe_checkout');
        $refund_url = $url . '/refund.php';
        $response = wp_remote_post($refund_url, array(
            'method' => 'POST',
            'timeout' => 45,
            'redirection' => 5,
            'httpversion' => '1.0',
            'blocking' => true,
            'headers' => array(),
            'body' => $array_with_parameters,
            'cookies' => array()
                )
        );        
        if (is_wp_error($response)) {
            return new WP_Error('response-error', $response->get_error_message());
        } else {
            $response_body = wp_remote_retrieve_body($response);
            $response_object = json_decode($response_body);
            if ($response_object->success == 'true') {
                update_post_meta($order_id, 'Refund Transaction ID', $response_object->refund_transaction_id);
                $order->add_order_note('Refund Transaction ID:' . $response_object->refund_transaction_id);
                $max_remaining_refund = wc_format_decimal($order->get_total() - $order->get_total_refunded());
                if (!$max_remaining_refund > 0) {
                    $order->update_status('refunded');
                }
                return true;
            } else {
                $fc_refund_error = $response_object->RESPMSG;
                return new WP_Error('ec_refund-error', $fc_refund_error);
            }
            return false;
        }
    }

}
