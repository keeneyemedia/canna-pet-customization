<?php

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Canna_Pet_Customization
 * @subpackage Canna_Pet_Customization/includes
 * @author     Angell EYE <service@angelleye.com>
 */
class Canna_Pet_Customization_Deactivator {

    /**
     * @since    1.0.0
     */
    public static function deactivate() {
        
    }

}
