<?php

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Canna_Pet_Customization
 * @subpackage Canna_Pet_Customization/includes
 * @author     Angell EYE <service@angelleye.com>
 */
class Canna_Pet_Customization_Authorize_Net_Payment extends WC_Autoship_Authorize_Net_Gateway {

    /**
     * @since    1.0.0
     */
    public $log_enabled = false;
    public $order;

    public function __construct() {
        parent::__construct();
        $this->id = 'wc_autoship_authorize_net';
        $this->debug = 'yes' === $this->get_option('debug', 'no');
        $this->log_enabled = $this->debug;
        if ($this->log_enabled) {
            if (!defined('AUTHORIZENET_LOG_FILE')) {
                define("AUTHORIZENET_LOG_FILE", version_compare(WC_VERSION, '3.0', '<') ? wc_get_log_file_path('wc_autoship_authorize_net') : WC_Log_Handler_File::get_log_file_path('wc_autoship_authorize_net'));
            }
        }
        add_filter('woocommerce_credit_card_form_fields', array($this, 'angelleye_wc_autoship_authorize_net_credit_card_form_fields'), 10, 2);
    }

    public function init_form_fields() {
        parent::init_form_fields();
        $this->form_fields['debug'] = array(
            'title' => __('Debug', 'cannapet-customization'),
            'type' => 'checkbox',
            'label' => sprintf(__('Enable logging<code>%s</code>', 'cannapet-customization'), version_compare(WC_VERSION, '3.0', '<') ? wc_get_log_file_path('wc_autoship_authorize_net') : WC_Log_Handler_File::get_log_file_path('wc_autoship_authorize_net')),
            'default' => 'no'
        );

        $this->form_fields['title'] = array(
        	'title' => __( 'Title', 'wc-autoship' ),
        	'type' => 'text',
        	'description' => __(
        		'This controls the title which the user sees during checkout.', 'wc-autoship'
        	),
        	'default' => __( 'Credit Card', 'wc-autoship' ),
        	'desc_tip' => true,
        );
    }

    public function get_posted_card() {
        $card_number = isset($_POST['wc_autoship_authorize_net_number']) ? wc_clean($_POST['wc_autoship_authorize_net_number']) : '';
        $card_cvc = isset($_POST['wc_autoship_authorize_net_card_code']) ? wc_clean($_POST['wc_autoship_authorize_net_card_code']) : '';
        $card_expiry = isset($_POST['wc_autoship_authorize_net_exp']) ? wc_clean($_POST['wc_autoship_authorize_net_exp']) : '';
        $card_number = str_replace(array(' ', '-'), '', $card_number);
        $card_expiry = array_map('trim', explode('/', $card_expiry));
        $card_exp_month = str_pad($card_expiry[0], 2, "0", STR_PAD_LEFT);
        $card_exp_year = isset($card_expiry[1]) ? $card_expiry[1] : '';
        if (strlen($card_exp_year) == 2) {
            $card_exp_year += 2000;
        }
        return (object) array(
                    'number' => $card_number,
                    'type' => '',
                    'cvc' => $card_cvc,
                    'exp_month' => $card_exp_month,
                    'exp_year' => $card_exp_year,
        );
    }

    public function validate_fields() {
        try {
            if (isset($_POST['wc-wc_autoship_authorize_net-payment-token']) && 'new' !== $_POST['wc-wc_autoship_authorize_net-payment-token']) {
                $token_id = wc_clean($_POST['wc-wc_autoship_authorize_net-payment-token']);
                $token = WC_Payment_Tokens::get($token_id);
                if ($token->get_user_id() !== get_current_user_id()) {
                    throw new Exception(__('Error processing checkout. Please try again.', 'paypal-for-woocommerce'));
                } else {
                    return true;
                }
            }
            $card = $this->get_posted_card();
            if (empty($card->exp_month) || empty($card->exp_year)) {
                throw new Exception(__('Card expiration date is invalid', 'woocommerce-gateway-paypal-pro'));
            }
            if (!ctype_digit($card->exp_month) || !ctype_digit($card->exp_year) || $card->exp_month > 12 || $card->exp_month < 1 || $card->exp_year < date('y')) {
                throw new Exception(__('Card expiration date is invalid', 'woocommerce-gateway-paypal-pro'));
            }
            if (empty($card->number) || !ctype_digit($card->number)) {
                throw new Exception(__('Card number is invalid', 'woocommerce-gateway-paypal-pro'));
            }
            if('yes' == $this->_require_cvv) {
                if (empty($card->cvc) || !ctype_digit($card->cvc)) {
                    throw new Exception(__('CVV/CVC is invalid', 'woocommerce-gateway-paypal-pro'));
                }
            }

            return true;
        } catch (Exception $e) {
            wc_add_notice($e->getMessage(), 'error');
            return false;
        }
    }

    public function process_payment($order_id) {
        $card = $this->get_posted_card();
        $order = wc_get_order($order_id);
        $this->order = $order;
        $customer_id = $order->get_user_id();
        $token = null;
        $merchant_authentication = $this->_get_merchant_authentication();
        $refId = 'ref' . time();
        $order_type = new \net\authorize\api\contract\v1\OrderType();
        $order_type->setDescription('WC Order #' . $order->get_order_number());
        $order_type->setInvoiceNumber($order->get_order_number());
        $solution_type = new \net\authorize\api\contract\v1\SolutionType();
        $solution_type->setId(('yes' == $this->_sandbox_mode) ? 'AAA100302' : 'AAA172579');
        $transaction_request_type = new \net\authorize\api\contract\v1\TransactionRequestType();
        $transaction_type = ( 'yes' == $this->_authorize_only ) ? 'authOnlyTransaction' : 'authCaptureTransaction';
        $transaction_request_type->setTransactionType($transaction_type);
        $transaction_request_type->setAmount($order->get_total());
        $transaction_request_type->setSolution($solution_type);
        if (empty($customer_id)) {
            $payment_data = array(
                'number' => $card->number,
                'expiration_date' => $card->exp_year . '-' . $card->exp_month
            );
            $creditCard = new \net\authorize\api\contract\v1\CreditCardType();
            $creditCard->setCardNumber($payment_data["number"]);
            $creditCard->setExpirationDate($payment_data["expiration_date"]);
            if ('yes' == $this->_require_cvv) {
                $payment_data['card_code'] = $card->cvc;
                $creditCard->setCardCode($payment_data["card_code"]);
            }
            $paymentOne = new \net\authorize\api\contract\v1\PaymentType();
            $paymentOne->setCreditCard($creditCard);
            $transaction_request_type->setPayment($paymentOne);
            $bill_to = new \net\authorize\api\contract\v1\CustomerAddressType();
            $bill_to->setFirstName($_POST['billing_first_name']);
            $bill_to->setLastName($_POST['billing_last_name']);
            $bill_to->setCompany($_POST['billing_company']);
            $bill_to->setAddress($_POST['billing_address_1']);
            $bill_to->setCity($_POST['billing_city']);
            $bill_to->setState($_POST['billing_state']);
            $bill_to->setZip($_POST['billing_postcode']);
            $bill_to->setCountry($_POST['billing_country']);
            $transaction_request_type->setBillTo($bill_to);
        } else {
            if (empty($_POST['wc-wc_autoship_authorize_net-payment-token']) || $_POST['wc-wc_autoship_authorize_net-payment-token'] == 'new') {
                $payment_data = array(
                    'number' => $card->number,
                    'expiration_date' => $card->exp_year . '-' . $card->exp_month
                );
                if ('yes' == $this->_require_cvv) {
                    $payment_data['card_code'] = $card->cvc;
                }
                try {
                    $token = $this->add_customer_payment_method($customer_id, $payment_data);
                } catch (Exception $e) {
                    throw new Exception(__($e->getMessage(), 'wc-autoship-authorize-net-payments'));
                }
            } else {
                $token_id = $_POST['wc-wc_autoship_authorize_net-payment-token'];
                $token = WC_Payment_Tokens::get($token_id);
            }
            try {
                $customer_profile = $this->get_or_create_customer($customer_id);
            } catch (Exception $ex) {

                $order->add_order_note($ex->getMessage());
            }

            $profile_to_charge = new \net\authorize\api\contract\v1\CustomerProfilePaymentType();
            $profile_to_charge->setCustomerProfileId($customer_profile->getCustomerProfileId());
            $payment_profile = new \net\authorize\api\contract\v1\PaymentProfileType();
            $payment_profile->setPaymentProfileId($token->get_token());
            $profile_to_charge->setPaymentProfile($payment_profile);
            $transaction_request_type->setProfile($profile_to_charge);
        }
        $transaction_request_type->setOrder($order_type);
        $request = new \net\authorize\api\contract\v1\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchant_authentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transaction_request_type);
        $controller = new \net\authorize\api\controller\CreateTransactionController($request);
        $response = $controller->executeWithApiResponse($this->_get_environment());
        if ($response != null) {
            $tresponse = $response->getTransactionResponse();
            if (( $tresponse != null ) && ( $tresponse->getResponseCode() == 1 )) {
                if (!empty($token)) {
                    $order->add_payment_token($token);
                    add_post_meta($order_id, '_cc_last_four', $token->get_last4(), true);
                } elseif (!empty($_POST['wc_autoship_authorize_net_number'])) {
                    $cc_last_four = substr($_POST['wc_autoship_authorize_net_number'], -4);
                    add_post_meta($order_id, '_cc_last_four', $cc_last_four, true);
                }
                $order_not = __('Transaction ID: ' . $tresponse->getTransId()) . PHP_EOL;
                $order_not .= __('Transaction Response Code: ' . $tresponse->getResponseCode()) . PHP_EOL;
                $order_not .= __('Message Code: ' . $tresponse->getMessages()[0]->getCode()) . PHP_EOL;
                $order_not .= __('Auth Code: ' . $tresponse->getAuthCode()) . PHP_EOL;
                $order_not .= __('AVS result Code: ' . $tresponse->getAvsResultCode()) . PHP_EOL;
                $order_not .= __('CVV result Code: ' . $tresponse->getCvvResultCode()) . PHP_EOL;
                $order_not .= __('Description: ' . $tresponse->getMessages()[0]->getDescription()) . PHP_EOL;
                $order_not .= sprintf(__('Payment Status Completed via %1$s (%2$s)', 'woo-authorizenet'), $this->title, $this->id) . PHP_EOL;
                $order->add_order_note($order_not);
                $order->payment_complete($tresponse->getTransId());
                return array(
                    'result' => 'success',
                    'redirect' => $this->get_return_url($order)
                );
            } else {
                $errorMessages = $tresponse->getErrors();
                if( !empty($errorMessages) ) {
                    $log_error = $errorMessages[0]->getErrorCode() . "  " . $errorMessages[0]->getErrorText();
                    $ERRORCODE = "Error Code  : " . $errorMessages[0]->getErrorCode() . "<br>";
                    $ERRORCODE .= "Error Message : " . $errorMessages[0]->getErrorText() . "</br>";
                    $order->add_order_note($ERRORCODE);
                    if (function_exists('wc_autoship_log_action')) {
                        wc_autoship_log_action(get_current_user_id(), 'auth_net_guest_checkout_failed', $log_error, $_SERVER['REQUEST_URI'], $customer_id);
                    }
                    $user_error_mesage = $this->get_message($tresponse);
                    if(count($user_error_mesage) > 0) {
                        foreach ($user_error_mesage as $key => $error_mesage) {
                            wc_add_notice($error_mesage, 'error');
                        }
                        return false;
                    } else {
                        $error = __('Error Message: ' . $errorMessages[0]->getErrorText(), 'wc-autoship-authorize-net-payments');
                        throw new Exception($error, $errorMessages[0]->getErrorText());
                    }
                }
            }
        }
        $error = 'No response from payment gateway';
        if (function_exists('wc_autoship_log_action')) {
            wc_autoship_log_action(get_current_user_id(), 'auth_net_get_no_response_from_server', $error, $_SERVER['REQUEST_URI'], $customer_id);
        }
        throw new Exception(__($error, 'wc-autoship-authorize-net-payments'));
    }

    /**
     * Process a refund for an order
     * @see WC_Payment_Gateway::process_refund()
     */
    public function process_refund($order_id, $amount = null, $reason = '') {
        if ($amount == null) {
            return false;
        }
        $order = wc_get_order($order_id);
        $transaction_id = $order->get_transaction_id();
        $merchant_authentication = $this->_get_merchant_authentication();
        $refId = 'ref' . time();
        $creditCard = new net\authorize\api\contract\v1\CreditCardType();
        $cc_last_four = get_post_meta($order_id, '_cc_last_four', true);
        $creditCard->setCardNumber($cc_last_four);
        $creditCard->setExpirationDate("XXXX");
        $paymentOne = new net\authorize\api\contract\v1\PaymentType();
        $paymentOne->setCreditCard($creditCard);
        $transactionRequest = new net\authorize\api\contract\v1\TransactionRequestType();
        $transactionRequest->setTransactionType("refundTransaction");
        $transactionRequest->setAmount($amount);
        $transactionRequest->setRefTransId($transaction_id);
        $transactionRequest->setPayment($paymentOne);
        $request = new net\authorize\api\contract\v1\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchant_authentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequest);
        $controller = new net\authorize\api\controller\CreateTransactionController($request);
        $response = $controller->executeWithApiResponse($this->_get_environment());
        if ($response != null) {
            if ($response->getMessages()->getResultCode() == 'Ok') {
                return true;
            }
        }
        return false;
    }

    /**
     * Add a payment method for the customer
     */
    public function add_payment_method() {
        $card = $this->get_posted_card();
        $payment_data = array(
            'number' => $card->number,
            'expiration_date' => $card->exp_year . '-' . $card->exp_month
        );
        if ('yes' == $this->_require_cvv) {
            $payment_data['card_code'] = $card->cvc;
        }
        $customer_id = get_current_user_id();
        try {
            $this->add_customer_payment_method($customer_id, $payment_data);
            return array(
                'result' => 'success',
                'redirect' => wc_get_endpoint_url('payment-methods')
            );
        } catch (Exception $e) {
            wc_add_notice($e->getMessage(), 'error');
            return;
        }
    }

    public function get_or_create_customer($customer_id) {
        $authorize_net_customer_id = get_user_meta($customer_id, 'wc_autoship_authorize_net_id', true);
        $merchant_authentication = $this->_get_merchant_authentication();
        if (!empty($authorize_net_customer_id)) {
            try {
                $request = new \net\authorize\api\contract\v1\GetCustomerProfileRequest();
                $request->setMerchantAuthentication($merchant_authentication);
                $request->setCustomerProfileId($authorize_net_customer_id);
                $controller = new \net\authorize\api\controller\GetCustomerProfileController($request);
                $response = $controller->executeWithApiResponse($this->_get_environment());
                if (( $response != null ) && ( $response->getMessages()->getResultCode() == "Ok" )) {
                    return $response->getProfile();
                }
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }
        }
        $user = new WP_User($customer_id);
        $refId = 'ref' . time();
        $request = new \net\authorize\api\contract\v1\CreateCustomerProfileRequest();
        $request->setMerchantAuthentication($merchant_authentication);
        $request->setRefId($refId);
        $customerprofile = new \net\authorize\api\contract\v1\CustomerProfileType();
        $customerprofile->setDescription(__('WC Autoship Customer ' . $customer_id, 'wc-autoship-authorize-net-payments'));
        $customerprofile->setMerchantCustomerId('wc-auto-' . $customer_id);
        $customerprofile->setEmail($user->user_email);
        $request->setProfile($customerprofile);
        $controller = new \net\authorize\api\controller\CreateCustomerProfileController($request);
        $response = $controller->executeWithApiResponse($this->_get_environment());
        if ($response != null) {
            if ($response->getMessages()->getResultCode() == "Ok") {
                $customer_profile_id = $response->getCustomerProfileId();
                update_user_meta($customer_id, 'wc_autoship_authorize_net_id', $customer_profile_id);
                $request = new \net\authorize\api\contract\v1\GetCustomerProfileRequest();
                $request->setMerchantAuthentication($merchant_authentication);
                $request->setCustomerProfileId($customer_profile_id);
                $controller = new \net\authorize\api\controller\GetCustomerProfileController($request);
                $response = $controller->executeWithApiResponse($this->_get_environment());
                if (( $response != null ) && ( $response->getMessages()->getResultCode() == "Ok" )) {
                    return $response->getProfile();
                } else {
                    $errorMessages = $response->getMessages()->getMessage();
                    $error = $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText();
                    if (function_exists('wc_autoship_log_action')) {
                        wc_autoship_log_action(get_current_user_id(), 'auth_net_get_customer_profile_failed', $error, $_SERVER['REQUEST_URI'], $customer_id);
                    }
                    throw new Exception($error);
                }
            } else {
                $errorMessages = $response->getMessages()->getMessage();
                $error = $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText();
                if( $errorMessages[0]->getText() == 'An error occurred during processing. Call Merchant Service Provider.'){
                    $error = $errorMessages[0]->getCode() . "  " .__('An error occurred during processing. Please try your order again or contact us.','wc-autoship-authorize-net-payments');
                }
                if (function_exists('wc_autoship_log_action')) {
                    wc_autoship_log_action(get_current_user_id(), 'auth_net_create_customer_profile_failed', $error, $_SERVER['REQUEST_URI'], $customer_id);
                }
                throw new Exception($error);
            }
        } else {
            $error = 'Null response from Authorize.net server';
            if (function_exists('wc_autoship_log_action')) {
                wc_autoship_log_action(get_current_user_id(), 'auth_net_create_customer_profile_failed', $error, $_SERVER['REQUEST_URI'], $customer_id);
            }
            throw new Exception($error);
        }
    }

    public function add_customer_payment_method($customer_id, $payment_data) {
        $customer_profile = $this->get_or_create_customer($customer_id);
        $billing_address = get_user_meta($customer_id, 'billing_address_1', true);
        if (empty($billing_address)) {
            throw new Exception(__('Please set a billing address before adding a new payment method!', 'wc-autoship-authorize-net'), self::ERROR_DISPLAY_TO_USER);
        }
        $merchant_authentication = $this->_get_merchant_authentication();
        $refId = 'ref' . time();
        $credit_card = new \net\authorize\api\contract\v1\CreditCardType();
        $clean_cc_num = $this->_clean_cc_number($payment_data['number']);
        $credit_card->setCardNumber($clean_cc_num);
        $credit_card->setExpirationDate($payment_data['expiration_date']);
        if ('yes' == $this->_require_cvv) {
            $credit_card->setCardCode($payment_data['card_code']);
        }
        $payment_credit_card = new \net\authorize\api\contract\v1\PaymentType();
        $payment_credit_card->setCreditCard($credit_card);
        $bill_to = new \net\authorize\api\contract\v1\CustomerAddressType();
        $bill_to->setFirstName(get_user_meta($customer_id, 'billing_first_name', true));
        $bill_to->setLastName(get_user_meta($customer_id, 'billing_last_name', true));
        $bill_to->setCompany(get_user_meta($customer_id, 'billing_company', true));
        $bill_to->setAddress(get_user_meta($customer_id, 'billing_address_1', true));
        $bill_to->setCity(get_user_meta($customer_id, 'billing_city', true));
        $bill_to->setState(get_user_meta($customer_id, 'billing_state', true));
        $bill_to->setZip(get_user_meta($customer_id, 'billing_postcode', true));
        $bill_to->setCountry(get_user_meta($customer_id, 'billing_country', true));
        $payment_profile = new \net\authorize\api\contract\v1\CustomerPaymentProfileType();
        $payment_profile->setCustomerType('individual');
        $payment_profile->setBillTo($bill_to);
        $payment_profile->setPayment($payment_credit_card);
        $paymentprofiles[] = $payment_profile;
        $payment_profile_request = new \net\authorize\api\contract\v1\CreateCustomerPaymentProfileRequest();
        $payment_profile_request->setMerchantAuthentication($merchant_authentication);
        $payment_profile_request->setCustomerProfileId($customer_profile->getCustomerProfileId());
        $payment_profile_request->setPaymentProfile($payment_profile);
        $payment_profile_request->setValidationMode("liveMode");
        $controller = new \net\authorize\api\controller\CreateCustomerPaymentProfileController($payment_profile_request);
        $response = $controller->executeWithApiResponse($this->_get_environment());
        if ($response != null) {
            if ($response->getMessages()->getResultCode() == "Ok") {
                $customer_payment_profile_id = $response->getCustomerPaymentProfileId();
                $refId = 'ref' . time();
                $request = new \net\authorize\api\contract\v1\GetCustomerPaymentProfileRequest();
                $request->setMerchantAuthentication($merchant_authentication);
                $request->setRefId($refId);
                $request->setCustomerProfileId($customer_profile->getCustomerProfileId());
                $request->setCustomerPaymentProfileId($customer_payment_profile_id);
                $controller = new \net\authorize\api\controller\GetCustomerPaymentProfileController($request);
                $profile_response = $controller->executeWithApiResponse($this->_get_environment());
                if ($profile_response != null) {
                    if ($profile_response->getMessages()->getResultCode() == "Ok") {
                        $payment_profile = $profile_response->getPaymentProfile();
                        $token = new WC_Payment_Token_CC();
                        $token->set_user_id($customer_id);
                        $token->set_token($payment_profile->getCustomerPaymentProfileId());
                        $token->set_gateway_id($this->id);
                        $credit_card = $payment_profile->getPayment()->getCreditCard();
                        $token->set_card_type(__('Credit Card', 'wc-autoship-authorize-net-payments'));
                        $token->set_last4(substr($credit_card->getCardNumber(), -4));
                        $expiration = $credit_card->getExpirationDate();
                        if (!is_numeric($expiration)) {
                            $expiration = $payment_data['expiration_date'];
                        }
                        $token->set_expiry_month(substr($expiration,-2));
                        $token->set_expiry_year(substr($expiration, 0, 4));
                        $saved = $token->save();
                        if (!$saved) {
                            throw new Exception(__('Error saving WC payment token!', 'wc-autoship-authorize-net-payments'));
                        }
                        return $token;
                    } else {
                        $errorMessages = $profile_response->getMessages()->getMessage();
                        $error = $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText();
                        if( $errorMessages[0]->getText() == 'An error occurred during processing. Call Merchant Service Provider.'){
                            $error = $errorMessages[0]->getCode() . "  " .__('An error occurred during processing. Please try your order again or contact us.','wc-autoship-authorize-net-payments');
                        }
                        if (function_exists('wc_autoship_log_action')) {
                            wc_autoship_log_action(get_current_user_id(), 'auth_net_get_payment_profile_failed', $error, $_SERVER['REQUEST_URI'], $customer_id);
                        }
                        if ( is_object( $this->order ) ) {
                            $this->order->add_order_note($error);
                        }
                        throw new Exception($error);
                    }
                } else {
                    $error = 'NULL response from Authorize.net server';
                    if (function_exists('wc_autoship_log_action')) {
                        wc_autoship_log_action(get_current_user_id(), 'auth_net_get_payment_profile_failed', $error, $_SERVER['REQUEST_URI'], $customer_id);
                    }
                    if ( is_object( $this->order ) ) {
                        $this->order->add_order_note($error);
                    }
                    throw new Exception($error);
                }
            } else {
                $errorMessages = $response->getMessages()->getMessage();
                if(!empty($errorMessages) && $errorMessages[0]->getCode() == 'E00039') {
                    $tokens = WC_Payment_Tokens::get_customer_tokens( $customer_id, $this->id );
                    if( !empty($tokens)) {
                        foreach ($tokens as $key => $token) {
                            if($token->get_last4() ==  substr($payment_data['number'], -4)) {
                                return $token;
                            }
                        }
                        foreach ($tokens as $key => $token) {
                            return $token;
                        }
                    }
                }

                $error = $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText();
                if( $errorMessages[0]->getText() == 'An error occurred during processing. Call Merchant Service Provider.'){
                    $error = $errorMessages[0]->getCode() . "  " .__('An error occurred during processing. Please try your order again or contact us.','wc-autoship-authorize-net-payments');
                }
                if (function_exists('wc_autoship_log_action')) {
                    wc_autoship_log_action(get_current_user_id(), 'auth_net_create_payment_profile_failed', $error, $_SERVER['REQUEST_URI'], $customer_id);
                }
                if ( is_object( $this->order ) ) {
                    $this->order->add_order_note($error);
                }
                throw new Exception($error);
            }
        } else {
            $error = 'NULL response from Authorize.net server';
            if (function_exists('wc_autoship_log_action')) {
                wc_autoship_log_action(get_current_user_id(), 'auth_net_create_payment_profile_failed', $error, $_SERVER['REQUEST_URI'], $customer_id);
            }
            if ( is_object( $this->order ) ) {
                $this->order->add_order_note($error);
            }
            throw new Exception($error);
        }
    }

    public function delete_customer_payment_method($customer_id, $payment_method_id) {
        $authorize_net_customer_id = get_user_meta($customer_id, 'wc_autoship_authorize_net_id', true);
        $merchant_authentication = $this->_get_merchant_authentication();
        $request = new \net\authorize\api\contract\v1\DeleteCustomerPaymentProfileRequest();
        $request->setMerchantAuthentication($merchant_authentication);
        $request->setCustomerProfileId($authorize_net_customer_id);
        $request->setCustomerPaymentProfileId($payment_method_id);
        $controller = new \net\authorize\api\controller\DeleteCustomerPaymentProfileController($request);
        $response = $controller->executeWithApiResponse($this->_get_environment());
        if (null == $response) {
            $error = __('NULL response from Authorize.net server', 'wc-autoship-authorize-net');
            if (function_exists('wc_autoship_log_action')) {
                wc_autoship_log_action(get_current_user_id(), 'auth_net_delete_payment_profile_failed', $error, $_SERVER['REQUEST_URI'], $customer_id);
            }
            throw new Exception($error);
        } elseif ($response->getMessages()->getResultCode() != "Ok") {
            $errorMessages = $response->getMessages()->getMessage();
            $error = $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText();
            if( $errorMessages[0]->getText() == 'An error occurred during processing. Call Merchant Service Provider.'){
                    $error = $errorMessages[0]->getCode() . "  " .__('An error occurred during processing. Please try your order again or contact us.','wc-autoship-authorize-net-payments');
            }
            if (function_exists('wc_autoship_log_action')) {
                wc_autoship_log_action(get_current_user_id(), 'auth_net_delete_payment_profile_failed', $error, $_SERVER['REQUEST_URI'], $customer_id);
            }
            throw new Exception($error);
        }
    }

    public function process_admin_options() {
        parent::process_admin_options();
    }

    private function _get_environment() {
        return ( 'yes' == $this->_sandbox_mode ) ? \net\authorize\api\constants\ANetEnvironment::SANDBOX : \net\authorize\api\constants\ANetEnvironment::PRODUCTION;
    }

    private function _get_merchant_authentication() {
        $merchantAuthentication = new \net\authorize\api\contract\v1\MerchantAuthenticationType();
        $merchantAuthentication->setName($this->_api_login_id);
        $merchantAuthentication->setTransactionKey($this->_transaction_key);
        return $merchantAuthentication;
    }

    private function _clean_cc_number($cc_number) {
        return preg_replace('/\D/', '', $cc_number);
    }

    public function get_saved_payment_method_option_html($token) {
        $html = sprintf(
                '<li class="woocommerce-SavedPaymentMethods-token">
				<input id="wc-%1$s-payment-token-%2$s" type="radio" name="wc-%1$s-payment-token" value="%2$s" style="width:auto;" class="woocommerce-SavedPaymentMethods-tokenInput" %4$s />
				<label for="wc-%1$s-payment-token-%2$s">%3$s</label>
			</li>', esc_attr($this->id), esc_attr($token->get_id()), esc_html($this->get_display_name($token)), checked($token->is_default(), true, false)
        );

        return apply_filters('woocommerce_payment_gateway_get_saved_payment_method_option_html', $html, $token, $this);
    }

    public function get_display_name($token) {
        /* translators: 1: credit card type 2: last 4 digits 3: expiry month 4: expiry year */
        $display = sprintf(
                __('%1$s ending in %2$s (expires %3$s/%4$s)', 'woocommerce'), wc_get_credit_card_type_label($token->get_card_type()), $token->get_last4(), $token->get_expiry_month(), $token->get_expiry_year()
        );
        return $display;
    }

    public function payment_fields() {
        do_action('before_angelleye_pc_payment_fields', $this);
        if ($this->description) {
            echo '<p>' . wp_kses_post($this->description);
            if ('yes' == $this->_sandbox_mode) {
                echo '<p>';
                _e('NOTICE: SANDBOX (TEST) MODE ENABLED.', 'paypal-for-woocommerce');
                echo '<br />';
                _e('For testing purposes you can use the card number 4111111111111111 with any CVC and a valid expiration date.', 'paypal-for-woocommerce');
                echo '</p>';
            }
        }
        if (is_checkout()) {
            $this->tokenization_script();
            $this->saved_payment_methods();
            $this->form();

            $this->save_payment_method_checkbox();
        } else {
            $this->form();
        }
        do_action('payment_fields_saved_payment_methods', $this);
    }

    public function angelleye_wc_autoship_authorize_net_credit_card_form_fields($default_fields, $current_gateway_id) {
        if ($current_gateway_id == $this->id) {
            $fields = array(
                'card-number-field' => '<p class="form-row form-row-wide">
                        <label for="' . esc_attr($this->id) . '-card-number">' . apply_filters('cc_form_label_card_number', __('Card Number', 'paypal-for-woocommerce'), $this->id) . ' <span class="required">*</span></label>
                        <input id="' . esc_attr($this->id) . '-card-number" class="input-text wc-credit-card-form-card-number" inputmode="numeric" autocomplete="cc-number" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" placeholder="&bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull;" name="wc_autoship_authorize_net_number" />
                    </p>',
                'card-expiry-field' => '<p class="form-row form-row-first">
                        <label for="' . esc_attr($this->id) . '-card-expiry">' . apply_filters('cc_form_label_expiry', __('Expiration (MM / YYYY)', 'paypal-for-woocommerce'), $this->id) . ' <span class="required">*</span></label>
                        <input id="' . esc_attr($this->id) . '-card-expiry" class="input-text wc-credit-card-form-card-expiry" inputmode="numeric" autocomplete="cc-exp" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" placeholder="' . esc_attr__('MM / YYYY', 'paypal-for-woocommerce') . '" name="wc_autoship_authorize_net_exp" />
                    </p>',
                '<p class="form-row form-row-last">
                        <label for="' . esc_attr($this->id) . '-card-cvc">' . apply_filters('cc_form_label_card_code', __('CVV/CVC', 'paypal-for-woocommerce'), $this->id) . ' <span class="required">*</span></label>
                        <input id="' . esc_attr($this->id) . '-card-cvc" class="input-text wc-credit-card-form-card-cvc" inputmode="numeric" autocomplete="off" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" maxlength="4" placeholder="' . esc_attr__('CVC', 'paypal-for-woocommerce') . '" name="wc_autoship_authorize_net_card_code" style="width:100px" />
                    </p>'
            );
            return $fields;
        } else {
            return $default_fields;
        }
    }

    public function get_message($tresponse) {
       $message_array = array();
       $reasons = array(
    		'2' => array(
    			'2'   => 'card_declined',
    			'3'   => 'card_declined',
    			'4'   => 'card_declined',
    			'27'  => 'avs_mismatch',
    			'28'  => 'card_type_not_accepted',
    			'35'  => 'error',
    			'37'  => 'card_number_invalid',
    			'44'  => 'csc_mismatch',
    			'45'  => 'card_declined',
    			'65'  => 'csc_mismatch',
    			'127' => 'avs_mismatch',
    			'165' => 'csc_mismatch',
    			'250' => 'decline',
    			'251' => 'decline',
    			'254' => 'decline',
    			'315' => 'card_number_invalid',
    			'316' => 'card_expiry_invalid',
    			'317' => 'card_expired',
    		),
    		'3' => array(
    			'8'   => 'card_expired',
    			'9'   => 'bank_aba_invalid',
    			'10'  => 'bank_account_number_invalid',
    			'17'  => 'card_type_not_accepted',
    			'19'  => 'authorize_net_error_try_later',
    			'20'  => 'authorize_net_error_try_later',
    			'21'  => 'authorize_net_error_try_later',
    			'22'  => 'authorize_net_error_try_later',
    			'23'  => 'authorize_net_error_try_later',
    			'36'  => 'authorize_net_authorized_but_not_settled',
    			'52'  => 'authorize_net_authorized_but_not_settled',
    			'57'  => 'authorize_net_error_try_later',
    			'58'  => 'authorize_net_error_try_later',
    			'59'  => 'authorize_net_error_try_later',
    			'60'  => 'authorize_net_error_try_later',
    			'61'  => 'authorize_net_error_try_later',
    			'62'  => 'authorize_net_error_try_later',
    			'63'  => 'authorize_net_error_try_later',
    			'66'  => 'decline',
    			'101' => 'authorize_net_echeck_mismatch',
    			'128' => 'decline',
    		),
    		'4' => array(
    			'193' => 'held_for_review',
    			'252' => 'held_for_review',
    			'253' => 'held_for_review',
    		)
    	);
        $response_code = $tresponse->getResponseCode();
        $errorMessages = $tresponse->getErrors();
        if( !empty($errorMessages) ) {
            foreach ($errorMessages as $key => $errorMessage) {
                $response_reason_code = $errorMessage->getErrorCode();
                $message_id = isset($reasons[$response_code][$response_reason_code]) ? $reasons[$response_code][$response_reason_code] : null;
                $message_array[] = $this->get_user_message($message_id);
            }
        }
        return $message_array;
    }

    public function get_user_message($message_id) {
        $message = null;
        switch ($message_id) {
            case 'error': $message = esc_html__('An error occurred, please try again or try an alternate form of payment', 'woocommerce-plugin-framework');
                break;
            case 'decline': $message = esc_html__('We cannot process your order with the payment information that you provided. Please use a different payment account or an alternate payment method.', 'woocommerce-plugin-framework');
                break;
            case 'held_for_review': $message = esc_html__('This order is being placed on hold for review. Please contact us to complete the transaction.', 'woocommerce-plugin-framework');
                break;
            case 'held_for_incorrect_csc': $message = esc_html__('This order is being placed on hold for review due to an incorrect card verification number.  You may contact the store to complete the transaction.', 'woocommerce-plugin-framework');
                break;
            case 'csc_invalid': $message = esc_html__('The card verification number is invalid, please try again.', 'woocommerce-plugin-framework');
                break;
            case 'csc_missing': $message = esc_html__('Please enter your card verification number and try again.', 'woocommerce-plugin-framework');
                break;
            case 'card_type_not_accepted': $message = esc_html__('That card type is not accepted, please use an alternate card or other form of payment.', 'woocommerce-plugin-framework');
                break;
            case 'card_type_invalid': $message = esc_html__('The card type is invalid or does not correlate with the credit card number.  Please try again or use an alternate card or other form of payment.', 'woocommerce-plugin-framework');
                break;
            case 'card_type_missing': $message = esc_html__('Please select the card type and try again.', 'woocommerce-plugin-framework');
                break;
            case 'card_number_type_invalid': $message = esc_html__('The card type is invalid or does not correlate with the credit card number.  Please try again or use an alternate card or other form of payment.', 'woocommerce-plugin-framework');
                break;
            case 'card_number_invalid': $message = esc_html__('The card number is invalid, please re-enter and try again.', 'woocommerce-plugin-framework');
                break;
            case 'card_number_missing': $message = esc_html__('Please enter your card number and try again.', 'woocommerce-plugin-framework');
                break;
            case 'card_expiry_invalid': $message = esc_html__('The card expiration date is invalid, please re-enter and try again.', 'woocommerce-plugin-framework');
                break;
            case 'card_expiry_month_invalid': $message = esc_html__('The card expiration month is invalid, please re-enter and try again.', 'woocommerce-plugin-framework');
                break;
            case 'card_expiry_year_invalid': $message = esc_html__('The card expiration year is invalid, please re-enter and try again.', 'woocommerce-plugin-framework');
                break;
            case 'card_expiry_missing': $message = esc_html__('Please enter your card expiration date and try again.', 'woocommerce-plugin-framework');
                break;
            case 'bank_aba_invalid': $message_id = esc_html__('The bank routing number is invalid, please re-enter and try again.', 'woocommerce-plugin-framework');
                break;
            case 'bank_account_number_invalid': $message_id = esc_html__('The bank account number is invalid, please re-enter and try again.', 'woocommerce-plugin-framework');
                break;
            case 'card_expired': $message = esc_html__('The provided card is expired, please use an alternate card or other form of payment.', 'woocommerce-plugin-framework');
                break;
            case 'card_declined': $message = esc_html__('The provided card was declined, please use an alternate card or other form of payment.', 'woocommerce-plugin-framework');
                break;
            case 'insufficient_funds': $message = esc_html__('Insufficient funds in account, please use an alternate card or other form of payment.', 'woocommerce-plugin-framework');
                break;
            case 'card_inactive': $message = esc_html__('The card is inactivate or not authorized for card-not-present transactions, please use an alternate card or other form of payment.', 'woocommerce-plugin-framework');
                break;
            case 'credit_limit_reached': $message = esc_html__('The credit limit for the card has been reached, please use an alternate card or other form of payment.', 'woocommerce-plugin-framework');
                break;
            case 'csc_mismatch': $message = esc_html__('The card verification number does not match. Please re-enter and try again.', 'woocommerce-plugin-framework');
                break;
            case 'avs_mismatch': $message = esc_html__('The provided address does not match the billing address for cardholder. Please verify the address and try again.', 'woocommerce-plugin-framework');
                break;
        }
        return apply_filters('wc_payment_gateway_transaction_response_user_message', $message, $message_id, $this);
    }

}
