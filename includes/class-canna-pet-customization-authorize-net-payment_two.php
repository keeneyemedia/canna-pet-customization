<?php

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Canna_Pet_Customization
 * @subpackage Canna_Pet_Customization/includes
 * @author     Angell EYE <service@angelleye.com>
 */
class Canna_Pet_Customization_Authorize_Net_Payment_Two extends Canna_Pet_Customization_Authorize_Net_Payment {

	// COPIED from original Autoship Authorize.net class
	protected $_title;
	protected $_api_login_id;
	protected $_transaction_key;
	protected $_authorize_only;
	protected $_sandbox_mode;

	// COPIED from Canna_Pet_Customization_Authorize_Net_Payment, added "two"
	/**
	 * @since    1.0.0
	 */
	public $log_enabled = false;
	public $order;


	public function __construct() {
		// COPIED from original Autoship Authorize.net class
		// WooCommerce fields
		$this->id = 'wc_autoship_authorize_net_two';
		$this->icon = '';
		$this->has_fields = true;
		$this->method_title = __( "WC Autoship Authorize.net Payments - 2", 'wc-autoship' );
		$this->method_description = __(
			"Authorize.net payment gateway 2 for WooCommerce and WC Autoship",
			'wc-autoship'
		);
		$this->description = $this->method_description;
		// WooCommerce settings
		$this->init_form_fields();
		$this->init_settings();
		// Assign settings
		$this->title = $this->get_option( 'title' );
		$this->_api_login_id = $this->get_option( 'api_login_id' );
		$this->_transaction_key = $this->get_option( 'transaction_key' );
		$this->_authorize_only = $this->get_option( 'authorize_only' );
		$this->_require_cvv = $this->get_option( 'require_cvv' );
		$this->_sandbox_mode = $this->get_option( 'sandbox_mode' );
		// Supports
		$this->supports = array(
			'refunds',
			'tokenization'
		);
		// Payment gateway hooks
		add_action(
			'woocommerce_update_options_payment_gateways_' . $this->id,
			array( $this, 'process_admin_options' )
		);

		// COPIED from Canna_Pet_Customization_Authorize_Net_Payment, added "two"
		$this->debug = 'yes' === $this->get_option('debug', 'no');
		$this->log_enabled = $this->debug;
		if ($this->log_enabled) {
				if (!defined('AUTHORIZENET_LOG_FILE_TWO')) {
						define("AUTHORIZENET_LOG_FILE_TWO", version_compare(WC_VERSION, '3.0', '<') ? wc_get_log_file_path('wc_autoship_authorize_net_two') : WC_Log_Handler_File::get_log_file_path('wc_autoship_authorize_net_two'));
				}
		}
		add_filter('woocommerce_credit_card_form_fields', array($this, 'angelleye_wc_autoship_authorize_net_two_credit_card_form_fields'), 10, 2);

	}


	// COPIED from Canna_Pet_Customization_Authorize_Net_Payment, added "two"
	public function init_form_fields() {
			parent::init_form_fields();
			$this->form_fields['debug'] = array(
					'title' => __('Debug', 'cannapet-customization'),
					'type' => 'checkbox',
					'label' => sprintf(__('Enable logging<code>%s</code>', 'cannapet-customization'), version_compare(WC_VERSION, '3.0', '<') ? wc_get_log_file_path('wc_autoship_authorize_net_two') : WC_Log_Handler_File::get_log_file_path('wc_autoship_authorize_net_two')),
					'default' => 'no'
			);

			$this->form_fields['title'] = array(
				'title' => __( 'Title', 'wc-autoship' ),
				'type' => 'text',
				'description' => __(
					'This controls the title which the user sees during checkout.', 'wc-autoship'
				),
				'default' => __( 'Credit Card', 'wc-autoship' ),
				'desc_tip' => true,
			);

			//ADDED: set default to NOT enabled when this plugin is first activated
			$this->form_fields['enabled'] = array(
				'title' => __( 'Enable/Disable', 'wc-autoship' ),
				'type' => 'checkbox',
				'label' => __( 'Enable ' . $this->method_title, 'wc-autoship' ),
				'default' => 'no'
			);
	}


	// COPIED from Canna_Pet_Customization_Authorize_Net_Payment, added "two"
	public function get_posted_card() {
			$card_number = isset($_POST['wc_autoship_authorize_net_two_number']) ? wc_clean($_POST['wc_autoship_authorize_net_two_number']) : '';
			$card_cvc = isset($_POST['wc_autoship_authorize_net_two_card_code']) ? wc_clean($_POST['wc_autoship_authorize_net_two_card_code']) : '';
			$card_expiry = isset($_POST['wc_autoship_authorize_net_two_exp']) ? wc_clean($_POST['wc_autoship_authorize_net_two_exp']) : '';
			$card_number = str_replace(array(' ', '-'), '', $card_number);
			$card_expiry = array_map('trim', explode('/', $card_expiry));
			$card_exp_month = str_pad($card_expiry[0], 2, "0", STR_PAD_LEFT);
			$card_exp_year = isset($card_expiry[1]) ? $card_expiry[1] : '';
			if (strlen($card_exp_year) == 2) {
					$card_exp_year += 2000;
			}
			return (object) array(
									'number' => $card_number,
									'type' => '',
									'cvc' => $card_cvc,
									'exp_month' => $card_exp_month,
									'exp_year' => $card_exp_year,
			);
	}


	// COPIED from Canna_Pet_Customization_Authorize_Net_Payment, added "two"
	public function process_payment($order_id) {
			$card = $this->get_posted_card();
			$order = wc_get_order($order_id);
			$this->order = $order;
			$customer_id = $order->get_user_id();
			$token = null;
			$merchant_authentication = $this->_get_merchant_authentication();
			$refId = 'ref' . time();
			$order_type = new \net\authorize\api\contract\v1\OrderType();
			$order_type->setDescription('WC Order #' . $order->get_order_number());
			$order_type->setInvoiceNumber($order->get_order_number());
			$solution_type = new \net\authorize\api\contract\v1\SolutionType();
			$solution_type->setId(('yes' == $this->_sandbox_mode) ? 'AAA100302' : 'AAA172579');
			$transaction_request_type = new \net\authorize\api\contract\v1\TransactionRequestType();
			$transaction_type = ( 'yes' == $this->_authorize_only ) ? 'authOnlyTransaction' : 'authCaptureTransaction';
			$transaction_request_type->setTransactionType($transaction_type);
			$transaction_request_type->setAmount($order->get_total());
			$transaction_request_type->setSolution($solution_type);
			if (empty($customer_id)) {
					$payment_data = array(
							'number' => $card->number,
							'expiration_date' => $card->exp_year . '-' . $card->exp_month
					);
					$creditCard = new \net\authorize\api\contract\v1\CreditCardType();
					$creditCard->setCardNumber($payment_data["number"]);
					$creditCard->setExpirationDate($payment_data["expiration_date"]);
					if ('yes' == $this->_require_cvv) {
							$payment_data['card_code'] = $card->cvc;
							$creditCard->setCardCode($payment_data["card_code"]);
					}
					$paymentOne = new \net\authorize\api\contract\v1\PaymentType();
					$paymentOne->setCreditCard($creditCard);
					$transaction_request_type->setPayment($paymentOne);
					$bill_to = new \net\authorize\api\contract\v1\CustomerAddressType();
					$bill_to->setFirstName($_POST['billing_first_name']);
					$bill_to->setLastName($_POST['billing_last_name']);
					$bill_to->setCompany($_POST['billing_company']);
					$bill_to->setAddress($_POST['billing_address_1']);
					$bill_to->setCity($_POST['billing_city']);
					$bill_to->setState($_POST['billing_state']);
					$bill_to->setZip($_POST['billing_postcode']);
					$bill_to->setCountry($_POST['billing_country']);
					$transaction_request_type->setBillTo($bill_to);
			} else {
					if (empty($_POST['wc-wc_autoship_authorize_net_two-payment-token']) || $_POST['wc-wc_autoship_authorize_net_two-payment-token'] == 'new') {
							$payment_data = array(
									'number' => $card->number,
									'expiration_date' => $card->exp_year . '-' . $card->exp_month
							);
							if ('yes' == $this->_require_cvv) {
									$payment_data['card_code'] = $card->cvc;
							}
							try {
									$token = $this->add_customer_payment_method($customer_id, $payment_data);
							} catch (Exception $e) {
									throw new Exception(__($e->getMessage(), 'wc-autoship-authorize-net-payments'));
							}
					} else {
							$token_id = $_POST['wc-wc_autoship_authorize_net_two-payment-token'];
							$token = WC_Payment_Tokens::get($token_id);
					}
					try {
							$customer_profile = $this->get_or_create_customer($customer_id);
					} catch (Exception $ex) {

							$order->add_order_note($ex->getMessage());
					}

					$profile_to_charge = new \net\authorize\api\contract\v1\CustomerProfilePaymentType();
					$profile_to_charge->setCustomerProfileId($customer_profile->getCustomerProfileId());
					$payment_profile = new \net\authorize\api\contract\v1\PaymentProfileType();
					$payment_profile->setPaymentProfileId($token->get_token());
					$profile_to_charge->setPaymentProfile($payment_profile);
					$transaction_request_type->setProfile($profile_to_charge);
			}
			$transaction_request_type->setOrder($order_type);
			$request = new \net\authorize\api\contract\v1\CreateTransactionRequest();
			$request->setMerchantAuthentication($merchant_authentication);
			$request->setRefId($refId);
			$request->setTransactionRequest($transaction_request_type);
			$controller = new \net\authorize\api\controller\CreateTransactionController($request);
			$response = $controller->executeWithApiResponse($this->_get_environment());
			if ($response != null) {
					$tresponse = $response->getTransactionResponse();
					if (( $tresponse != null ) && ( $tresponse->getResponseCode() == 1 )) {
							if (!empty($token)) {
									$order->add_payment_token($token);
									add_post_meta($order_id, '_cc_last_four', $token->get_last4(), true);
							} elseif (!empty($_POST['wc_autoship_authorize_net_two_number'])) {
									$cc_last_four = substr($_POST['wc_autoship_authorize_net_two_number'], -4);
									add_post_meta($order_id, '_cc_last_four', $cc_last_four, true);
							}
							$order_not = __('Transaction ID: ' . $tresponse->getTransId()) . PHP_EOL;
							$order_not .= __('Transaction Response Code: ' . $tresponse->getResponseCode()) . PHP_EOL;
							$order_not .= __('Message Code: ' . $tresponse->getMessages()[0]->getCode()) . PHP_EOL;
							$order_not .= __('Auth Code: ' . $tresponse->getAuthCode()) . PHP_EOL;
							$order_not .= __('AVS result Code: ' . $tresponse->getAvsResultCode()) . PHP_EOL;
							$order_not .= __('CVV result Code: ' . $tresponse->getCvvResultCode()) . PHP_EOL;
							$order_not .= __('Description: ' . $tresponse->getMessages()[0]->getDescription()) . PHP_EOL;
							$order_not .= sprintf(__('Payment Status Completed via %1$s (%2$s)', 'woo-authorizenet'), $this->title, $this->id) . PHP_EOL;
							$order->add_order_note($order_not);
							$order->payment_complete($tresponse->getTransId());
							return array(
									'result' => 'success',
									'redirect' => $this->get_return_url($order)
							);
					} else {
							$errorMessages = $tresponse->getErrors();
							if( !empty($errorMessages) ) {
									$log_error = $errorMessages[0]->getErrorCode() . "  " . $errorMessages[0]->getErrorText();
									$ERRORCODE = "Error Code  : " . $errorMessages[0]->getErrorCode() . "<br>";
									$ERRORCODE .= "Error Message : " . $errorMessages[0]->getErrorText() . "</br>";
									$order->add_order_note($ERRORCODE);
									if (function_exists('wc_autoship_log_action')) {
											wc_autoship_log_action(get_current_user_id(), 'auth_net_guest_checkout_failed', $log_error, $_SERVER['REQUEST_URI'], $customer_id);
									}
									$user_error_mesage = $this->get_message($tresponse);
									if(count($user_error_mesage) > 0) {
											foreach ($user_error_mesage as $key => $error_mesage) {
													wc_add_notice($error_mesage, 'error');
											}
											return false;
									} else {
											$error = __('Error Message: ' . $errorMessages[0]->getErrorText(), 'wc-autoship-authorize-net-payments');
											throw new Exception($error, $errorMessages[0]->getErrorText());
									}
							}
					}
			}
			$error = 'No response from payment gateway';
			if (function_exists('wc_autoship_log_action')) {
					wc_autoship_log_action(get_current_user_id(), 'auth_net_get_no_response_from_server', $error, $_SERVER['REQUEST_URI'], $customer_id);
			}
			throw new Exception(__($error, 'wc-autoship-authorize-net-payments'));
	}


	// COPIED from Canna_Pet_Customization_Authorize_Net_Payment, added "two"
	public function validate_fields() {
			try {
					if (isset($_POST['wc-wc_autoship_authorize_net_two-payment-token']) && 'new' !== $_POST['wc-wc_autoship_authorize_net_two-payment-token']) {
							$token_id = wc_clean($_POST['wc-wc_autoship_authorize_net_two-payment-token']);
							$token = WC_Payment_Tokens::get($token_id);
							if ($token->get_user_id() !== get_current_user_id()) {
									throw new Exception(__('Error processing checkout. Please try again.', 'paypal-for-woocommerce'));
							} else {
									return true;
							}
					}
					$card = $this->get_posted_card();
					if (empty($card->exp_month) || empty($card->exp_year)) {
							throw new Exception(__('Card expiration date is invalid', 'woocommerce-gateway-paypal-pro'));
					}
					if (!ctype_digit($card->exp_month) || !ctype_digit($card->exp_year) || $card->exp_month > 12 || $card->exp_month < 1 || $card->exp_year < date('y')) {
							throw new Exception(__('Card expiration date is invalid', 'woocommerce-gateway-paypal-pro'));
					}
					if (empty($card->number) || !ctype_digit($card->number)) {
							throw new Exception(__('Card number is invalid', 'woocommerce-gateway-paypal-pro'));
					}
					if('yes' == $this->_require_cvv) {
							if (empty($card->cvc) || !ctype_digit($card->cvc)) {
									throw new Exception(__('CVV/CVC is invalid', 'woocommerce-gateway-paypal-pro'));
							}
					}

					return true;
			} catch (Exception $e) {
					wc_add_notice($e->getMessage(), 'error');
					return false;
			}
	}


	// COPIED from Canna_Pet_Customization_Authorize_Net_Payment, added "two"
	public function get_or_create_customer($customer_id) {
			$authorize_net_customer_id = get_user_meta($customer_id, 'wc_autoship_authorize_net_two_id', true);
			$merchant_authentication = $this->_get_merchant_authentication();
			if (!empty($authorize_net_customer_id)) {
					try {
							$request = new \net\authorize\api\contract\v1\GetCustomerProfileRequest();
							$request->setMerchantAuthentication($merchant_authentication);
							$request->setCustomerProfileId($authorize_net_customer_id);
							$controller = new \net\authorize\api\controller\GetCustomerProfileController($request);
							$response = $controller->executeWithApiResponse($this->_get_environment());
							if (( $response != null ) && ( $response->getMessages()->getResultCode() == "Ok" )) {
									return $response->getProfile();
							}
					} catch (Exception $e) {
							throw new Exception($e->getMessage());
					}
			}
			$user = new WP_User($customer_id);
			$refId = 'ref' . time();
			$request = new \net\authorize\api\contract\v1\CreateCustomerProfileRequest();
			$request->setMerchantAuthentication($merchant_authentication);
			$request->setRefId($refId);
			$customerprofile = new \net\authorize\api\contract\v1\CustomerProfileType();
			$customerprofile->setDescription(__('WC Autoship Customer ' . $customer_id, 'wc-autoship-authorize-net-payments'));
			$customerprofile->setMerchantCustomerId('wc-auto-' . $customer_id);
			$customerprofile->setEmail($user->user_email);
			$request->setProfile($customerprofile);
			$controller = new \net\authorize\api\controller\CreateCustomerProfileController($request);
			$response = $controller->executeWithApiResponse($this->_get_environment());
			if ($response != null) {
					if ($response->getMessages()->getResultCode() == "Ok") {
							$customer_profile_id = $response->getCustomerProfileId();
							update_user_meta($customer_id, 'wc_autoship_authorize_net_two_id', $customer_profile_id);
							$request = new \net\authorize\api\contract\v1\GetCustomerProfileRequest();
							$request->setMerchantAuthentication($merchant_authentication);
							$request->setCustomerProfileId($customer_profile_id);
							$controller = new \net\authorize\api\controller\GetCustomerProfileController($request);
							$response = $controller->executeWithApiResponse($this->_get_environment());
							if (( $response != null ) && ( $response->getMessages()->getResultCode() == "Ok" )) {
									return $response->getProfile();
							} else {
									$errorMessages = $response->getMessages()->getMessage();
									$error = $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText();
									if (function_exists('wc_autoship_log_action')) {
											wc_autoship_log_action(get_current_user_id(), 'auth_net_get_customer_profile_failed', $error, $_SERVER['REQUEST_URI'], $customer_id);
									}
									throw new Exception($error);
							}
					} else {
							$errorMessages = $response->getMessages()->getMessage();
							$error = $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText();
							if( $errorMessages[0]->getText() == 'An error occurred during processing. Call Merchant Service Provider.'){
									$error = $errorMessages[0]->getCode() . "  " .__('An error occurred during processing. Please try your order again or contact us.','wc-autoship-authorize-net-payments');
							}
							if (function_exists('wc_autoship_log_action')) {
									wc_autoship_log_action(get_current_user_id(), 'auth_net_create_customer_profile_failed', $error, $_SERVER['REQUEST_URI'], $customer_id);
							}
							throw new Exception($error);
					}
			} else {
					$error = 'Null response from Authorize.net server';
					if (function_exists('wc_autoship_log_action')) {
							wc_autoship_log_action(get_current_user_id(), 'auth_net_create_customer_profile_failed', $error, $_SERVER['REQUEST_URI'], $customer_id);
					}
					throw new Exception($error);
			}
	}


	// COPIED from Canna_Pet_Customization_Authorize_Net_Payment, added "two"
	public function delete_customer_payment_method($customer_id, $payment_method_id) {
			$authorize_net_customer_id = get_user_meta($customer_id, 'wc_autoship_authorize_net_two_id', true);
			$merchant_authentication = $this->_get_merchant_authentication();
			$request = new \net\authorize\api\contract\v1\DeleteCustomerPaymentProfileRequest();
			$request->setMerchantAuthentication($merchant_authentication);
			$request->setCustomerProfileId($authorize_net_customer_id);
			$request->setCustomerPaymentProfileId($payment_method_id);
			$controller = new \net\authorize\api\controller\DeleteCustomerPaymentProfileController($request);
			$response = $controller->executeWithApiResponse($this->_get_environment());
			if (null == $response) {
					$error = __('NULL response from Authorize.net server', 'wc-autoship-authorize-net');
					if (function_exists('wc_autoship_log_action')) {
							wc_autoship_log_action(get_current_user_id(), 'auth_net_delete_payment_profile_failed', $error, $_SERVER['REQUEST_URI'], $customer_id);
					}
					throw new Exception($error);
			} elseif ($response->getMessages()->getResultCode() != "Ok") {
					$errorMessages = $response->getMessages()->getMessage();
					$error = $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText();
					if( $errorMessages[0]->getText() == 'An error occurred during processing. Call Merchant Service Provider.'){
									$error = $errorMessages[0]->getCode() . "  " .__('An error occurred during processing. Please try your order again or contact us.','wc-autoship-authorize-net-payments');
					}
					if (function_exists('wc_autoship_log_action')) {
							wc_autoship_log_action(get_current_user_id(), 'auth_net_delete_payment_profile_failed', $error, $_SERVER['REQUEST_URI'], $customer_id);
					}
					throw new Exception($error);
			}
	}


	// 3 functions IDENTICAL from Canna_Pet_Customization_Authorize_Net_Payment - can't inherit "private" methods from parent
	private function _get_environment() {
		return ( 'yes' == $this->_sandbox_mode ) ? \net\authorize\api\constants\ANetEnvironment::SANDBOX : \net\authorize\api\constants\ANetEnvironment::PRODUCTION;
	}

	private function _get_merchant_authentication() {
		$merchantAuthentication = new \net\authorize\api\contract\v1\MerchantAuthenticationType();
		$merchantAuthentication->setName( $this->_api_login_id );
		$merchantAuthentication->setTransactionKey( $this->_transaction_key );
		return $merchantAuthentication;
	}

	private function _clean_cc_number( $cc_number ) {
		return preg_replace( '/\D/', '', $cc_number );
	}


	// COPIED from Canna_Pet_Customization_Authorize_Net_Payment, added "two" to each 'name'
	public function angelleye_wc_autoship_authorize_net_two_credit_card_form_fields($default_fields, $current_gateway_id) {
			if ($current_gateway_id == $this->id) {
					$fields = array(
							'card-number-field' => '<p class="form-row form-row-wide">
											<label for="' . esc_attr($this->id) . '-card-number">' . apply_filters('cc_form_label_card_number', __('Card Number', 'paypal-for-woocommerce'), $this->id) . ' <span class="required">*</span></label>
											<input id="' . esc_attr($this->id) . '-card-number" class="input-text wc-credit-card-form-card-number" inputmode="numeric" autocomplete="cc-number" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" placeholder="&bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull;" name="wc_autoship_authorize_net_two_number" />
									</p>',
							'card-expiry-field' => '<p class="form-row form-row-first">
											<label for="' . esc_attr($this->id) . '-card-expiry">' . apply_filters('cc_form_label_expiry', __('Expiration (MM / YYYY)', 'paypal-for-woocommerce'), $this->id) . ' <span class="required">*</span></label>
											<input id="' . esc_attr($this->id) . '-card-expiry" class="input-text wc-credit-card-form-card-expiry" inputmode="numeric" autocomplete="cc-exp" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" placeholder="' . esc_attr__('MM / YYYY', 'paypal-for-woocommerce') . '" name="wc_autoship_authorize_net_two_exp" />
									</p>',
							'<p class="form-row form-row-last">
											<label for="' . esc_attr($this->id) . '-card-cvc">' . apply_filters('cc_form_label_card_code', __('CVV/CVC', 'paypal-for-woocommerce'), $this->id) . ' <span class="required">*</span></label>
											<input id="' . esc_attr($this->id) . '-card-cvc" class="input-text wc-credit-card-form-card-cvc" inputmode="numeric" autocomplete="off" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" maxlength="4" placeholder="' . esc_attr__('CVC', 'paypal-for-woocommerce') . '" name="wc_autoship_authorize_net_two_card_code" style="width:100px" />
									</p>'
					);
					return $fields;
			} else {
					return $default_fields;
			}
	}


}
