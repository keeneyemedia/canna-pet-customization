<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
$http = is_ssl() ? 'https' : 'http';
?>
<?php do_action('woocommerce_email_header', $email_heading, $email); ?>

<?php
//ACF fields
if( class_exists('acf') ) {

	$create_page = get_field('custom_account_create_page','option');
	$create_page = esc_url(add_query_arg(array('email' => $email), $create_page ) );
	$prompt_title = get_field('guest_user_registration_prompt_title','option');
	$prompt_text = get_field('guest_user_registration_prompt_text','option');
	$prompt_button = get_field('guest_user_registration_prompt_button','option');

} else {

	$create_page = esc_url(add_query_arg(array('email' => $email), site_url('/create-your-account/', $http) ) );
	$prompt_title = sprintf(__('Save Time and Money on Every Order', 'canna-pet-customization'));
	$prompt_text = sprintf(__('Please register your Canna-Pet® account so you can track your orders, obtain personalized customer support, and receive notifications of future sales and promotions. It takes less than 20 seconds - just choose a password!', 'canna-pet-customization'));
	$prompt_button = __('Click here to setup your account', 'canna-pet-customization');
	
}
?>

<h3><?php echo $prompt_title; ?></h3>
<div>
	<p style="margin-bottom: 2em;"><?php echo $prompt_text; ?></p>
	<p style="text-align: center;"><?php _e('Click below to setup your account:', 'canna-pet-customization'); ?></p>
	<p style="text-align: center;"><a href="<?php echo $create_page; ?>" style="border: 2px solid #f17600; color: #f17600; border-radius: 5px; max-width: 400px; margin: 0.5em auto; padding: 10px 30px; text-decoration: none;"><?php echo $prompt_button; ?></a></p>
</div> 

<?php /*
<p><?php printf(__('Get access to order history and quick checkouts on future orders by completing your account setup today!', 'canna-pet-customization')); ?></p>
<p><?php _e('To setup your account, visit the following address:', 'canna-pet-customization'); ?></p>
<p>
    <a class="link" href="<?php echo esc_url(add_query_arg(array('email' => $email), site_url('/create-your-account/', $http))); ?>">
        <?php _e('Click here to setup your account', 'canna-pet-customization'); ?></a>
</p>
*/ ?>

<?php
do_action('woocommerce_email_footer', $email);
