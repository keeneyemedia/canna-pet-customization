<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Canna_Pet_Customization
 * @subpackage Canna_Pet_Customization/admin
 * @author     Angell EYE <service@angelleye.com>
 */
class Canna_Pet_Customization_Admin {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    public function add_guest_customer_new_account_woocommerce_email($email_classes) {
        $email_classes['WC_Email_Guest_Customer_New_Account'] = require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-canna-pet-customization-guest-checkout-email.php';
        return $email_classes;
    }

    public function send_guest_customer_new_account_woocommerce_email($user_id) {
        $emails = WC()->mailer()->get_emails();
        if (!empty($emails) && !empty($user_id)) {
            $emails['WC_Email_Guest_Customer_New_Account']->trigger($user_id);
        }
    }

    

    public function own_wc_autoship_admin_ajax_export_autoship_schedules() {
        if (!user_can(get_current_user_id(), 'manage_woocommerce')) {
            echo "Action not allowed.";
            die();
        }
        require_once(WC_AUTOSHIP_SRC_DIR . '/Admin/SchedulesListTable.php');
        $list_table = @new WC_Autoship_Admin_SchedulesListTable();
        $list_table->prepare_items();
        if (!empty($list_table->items)) {
            foreach ($list_table->items as $key => $item) {
                if (!empty($item->payment_token_id)) {
                    $token = WC_Payment_Tokens::get($item->payment_token_id);
                    if (!empty($token)) {
                        $item->gateway_id = $token->get_gateway_id();
                    } else {
                        $item->gateway_id = 'N/A';
                    }
                } else {
                    $item->gateway_id = 'Token empty';
                }
                $list_table->items[$key] = $item;
            }
        }
        wc_autoship_admin_export_items_file($list_table->items, 'autoship_schedules.csv');
    }
                
    public function add_your_gateway_class( $methods ) {
        if (class_exists('WC_Gateway_Petconscious_Checkout')) {
            $methods[] = 'WC_Gateway_Petconscious_Checkout';
        }        
        
        if (class_exists('WC_Gateway_Petconscious_Payflow_Pro')) {
            $methods[] = 'WC_Gateway_Petconscious_Payflow_Pro';
        }
        if (class_exists('WC_Gateway_Cannapets_MojoPay')) {
            $methods[] = 'WC_Gateway_Cannapets_MojoPay';
        }
        return $methods;
    }
}
