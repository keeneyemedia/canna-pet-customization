<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('WC_Email_Guest_Customer_New_Account')) :

    class WC_Email_Guest_Customer_New_Account extends WC_Email {

        public $user_login;
        public $user_email;
        public $reset_key;

        public function __construct() {
            $this->id = 'guest_customer_new_account';
            $this->customer_email = true;
            $this->title = __('New account', 'canna-pet-customization');
            $this->description = __('Guest Customer "new account" emails are sent to the customer when a customer place order.', 'canna-pet-customization');
            $this->template_html = 'admin/canna-pet-customization-guest-checkout-email.php';
            $this->subject = __('Your account on {site_title}', 'canna-pet-customization');
            $this->heading = __('Welcome to {site_title}', 'canna-pet-customization');
            parent::__construct();
        }

        public function trigger($user_id) {
            if ($user_id) {
                $this->object = new WP_User($user_id);
                $this->user_login = stripslashes($this->object->user_login);
                $this->user_email = stripslashes($this->object->user_email);
                $this->recipient = $this->user_email;
                $this->reset_key = get_password_reset_key($this->object);
            }
            if (!$this->get_recipient()) {
                return;
            }
            $this->send($this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments());
        }

        public function get_content_html() {
            return wc_get_template_html($this->template_html, array(
                'email_heading' => $this->get_heading(),
                'user_login' => $this->user_login,
                'blogname' => $this->get_blogname(),
                'sent_to_admin' => false,
                'plain_text' => false,
                'email' => $this->user_email,
                'reset_key' => $this->reset_key,
                    ), '', plugin_dir_path(dirname(__FILE__)));
        }

        public function get_content_plain() {
            return wc_get_template_html($this->template_plain, array(
                'email_heading' => $this->get_heading(),
                'user_login' => $this->user_login,
                'blogname' => $this->get_blogname(),
                'sent_to_admin' => false,
                'plain_text' => true,
                'email' => $this->user_email
            ));
        }

    }

    endif;
return new WC_Email_Guest_Customer_New_Account();